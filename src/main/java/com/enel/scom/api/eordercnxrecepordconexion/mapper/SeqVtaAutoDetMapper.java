package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SeqVtaAutoDetMapper implements RowMapper<Long>{

	public static final String SQLORACLE_SELECT_FOR_SEQVTAAUTODT = "SELECT"
			+ " SQVTAAUTODT.nextval as seqVtaAutoDt"
			+ " FROM dual";

	@Override
	public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
		Long seqVtaAutoDt = rs.getLong("seqVtaAutoDt");
		return seqVtaAutoDt;
	}
	
}
