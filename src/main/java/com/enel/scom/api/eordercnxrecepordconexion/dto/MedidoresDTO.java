package com.enel.scom.api.eordercnxrecepordconexion.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MedidoresDTO {

	private String numeroMedidor;
	private String marcaMedidor;
	private String modeloMedidor;
	private String numeroFabricaMedidor;
	private String tipoMedidor;
	private String tecnologiaMedidor;
	private String faseMedidor;
	private String factorMedidor;
	private String propiedadMedidor;
	private String accionMedidor;
	private String tipoMedicion;
	private String ubicacionMedidor;
	private String telemedido;
	private String elementoBorneroPrueba;
	private String anoFabricacion;
	private String fechaInstalacion;
}
