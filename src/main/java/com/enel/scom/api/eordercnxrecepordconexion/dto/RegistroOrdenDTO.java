package com.enel.scom.api.eordercnxrecepordconexion.dto;

import java.util.List;

import com.enel.scom.api.eordercnxrecepordconexion.autoactiva.GeneradorPlano;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RegistroOrdenDTO {

	private String numOrden;
	private String numCuenta;
	private String ptoEntrega; 
	private String propEmpalme;
	private String tipoConductor;
	private String capInterruptor;
	private String sisProteccion;
	private String cajaMedicion;
	private String cajaToma;
	private String acometida;
	private String sumDerecho;
	private String sumIzquierdo;
	private String numPcr;
	private String tensionPcr;
	private String sumPcrRef;
	// R.I. REQ MAXIMETROS INICIO
	//private List<MedidorInstallDTO> medidorInstalar;
	//private List<MedidorInstallDTO> medidorRetirar;
	// R.I. REQ MAXIMETROS FIN
	private String numMedidorInstall;
	private String codMarcaInstall;
	private String codModeloInstall;
	private String fecLecturaInstall;
	private String novedadInstall;
	private String lecturaInstall; 
	private String numMedidorRet;
	private String codMarcaRet;
	private String codModeloRet;
	private String fecLecturaRet;
	private String novedadRet;
	private String lecturaRet;
	private String set;
	private String alimentador;
	private String sed; 
	private String llave;
	private String sector;
	private String zona;
	private String correlativo;
	private String fechaEjecucion;
	private String latitudMedidor;
	private String longitudMedidor;
	private String latitudEmpalme;
	private String longitudEmpalme;	

	// R.I. REQ MAXIMETROS INICIO
	public String obtenerLinea() {
        StringBuilder lineaBuilder = new StringBuilder();
        appendField(lineaBuilder, numOrden);
        appendField(lineaBuilder, numCuenta);
        appendField(lineaBuilder, ptoEntrega);
        appendField(lineaBuilder, propEmpalme);
        appendField(lineaBuilder, tipoConductor);
        appendField(lineaBuilder, capInterruptor);
        appendField(lineaBuilder, sisProteccion);
        appendField(lineaBuilder, cajaMedicion);
        appendField(lineaBuilder, cajaToma);
        appendField(lineaBuilder, acometida);
        appendField(lineaBuilder, sumDerecho);
        appendField(lineaBuilder, sumIzquierdo);
        appendField(lineaBuilder, numPcr);
        appendField(lineaBuilder, tensionPcr);
        appendField(lineaBuilder, sumPcrRef);

        // Tratar la lista de medidores a instalar
        //appendMedidores(lineaBuilder, medidorInstalar);

        // Tratar la lista de medidores a retirar
        //appendMedidores(lineaBuilder, medidorRetirar);

		appendField(lineaBuilder, set);
        appendField(lineaBuilder, alimentador);
        appendField(lineaBuilder, sed);
        appendField(lineaBuilder, llave);
        appendField(lineaBuilder, sector);
        appendField(lineaBuilder, zona);
        appendField(lineaBuilder, correlativo);
        appendField(lineaBuilder, fechaEjecucion);
        appendField(lineaBuilder, latitudMedidor);
        appendField(lineaBuilder, longitudMedidor);
        appendField(lineaBuilder, latitudEmpalme);
        appendField(lineaBuilder, longitudEmpalme);

        return lineaBuilder.toString();
    }

    private void appendField(StringBuilder builder, String value) {
        if (value != null) {
            builder.append(value);
        }
        builder.append(GeneradorPlano.SEPARATOR);
    }

	private void appendMedidores(StringBuilder builder, List<MedidorInstallDTO> listaMedidores) {
        if (listaMedidores != null && !listaMedidores.isEmpty()) {
            for (MedidorInstallDTO medidor : listaMedidores) {
                builder.append(medidor.obtenerLinea());
            }
        }
    }
	// R.I. REQ MAXIMETROS FIN

}
