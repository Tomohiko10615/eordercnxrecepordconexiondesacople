package com.enel.scom.api.eordercnxrecepordconexion.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Constante {
	public static final String PROCESO = "EOrderCNXRecepOrdConexion";
	public static final int debug = 0;
	public static final int debug_sql=0;
	public String getRUTA_LOG() throws IOException {
		Properties propiedades = new Properties();	
		InputStream propertiesStream = this.getClass().getClassLoader().getResourceAsStream("application.properties");
		propiedades.load(propertiesStream);
		return propiedades.getProperty("log.output.RUTA_LOG");
	}
}
