package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.AnexosDTO;

public class AnexosMapper implements RowMapper<AnexosDTO> {

	public static final String SQLPOSTGRESQL_SELECT_FOR_ANEXOS = "select\r\n"
			+ "			 unnest(xpath('/item/Tipo_anexo/text()', d.REG_XML))::text AS tipoAnexo,\r\n"
			+ "				unnest(xpath('/item/Nombre_Anexo/text()', d.REG_XML))::text AS nombreAnexo,\r\n"
			+ "				unnest(xpath('/item/Ruta_Anexo/text()', d.REG_XML))::text AS rutaAnexo\r\n"
			+ "FROM (SELECT   unnest(xpath('/anexos/item', unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/anexos', d.REG_XML)))) AS REG_XML  \r\n"
			+ "                    FROM SCHSCOM.EOR_ORD_TRANSFER_DET d  \r\n"
			+ "                    WHERE d.id_ord_transfer =  ? \r\n"
			+ "                    	AND accion = 'RECEPCION'  \r\n"
			+ "                        AND nro_evento = ?)d";

	@Override
	public AnexosDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		String tipoAnexo = rs.getString("tipoAnexo");
		String nombreAnexo = rs.getString("nombreAnexo");
		String rutaAnexo = rs.getString("rutaAnexo");
		return new AnexosDTO(tipoAnexo, nombreAnexo, rutaAnexo);
	}
	
}
