package com.enel.scom.api.eordercnxrecepordconexion.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CompEstUbicacionDTO {

	private Long idComponente;
	private Long idEstComp;
	private Long idServicio;
	private String tipoUbic;
}
