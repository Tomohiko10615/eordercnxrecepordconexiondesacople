package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.CentroServicioSoliDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.OrdenVentaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.UbiRutaDTO;

public class CentroServicioSoliMapper implements RowMapper<CentroServicioSoliDTO> {

	public static final String SQLORACLE_SELECT_FOR_CENTROSERVICIOSOLI = "SELECT nvl(ofi.cod_oficina,-1) as cod_oficina, sol.id_solicitud\r\n"
			+ "	FROM vta_solicitud sol, vta_ord_vta oven , seg_oficina ofi, ord_orden ooven\r\n"
			+ "	WHERE\r\n"
			+ "	ooven.nro_orden = ?\r\n"
			+ "	AND sol.id_solicitud = oven.id_solicitud\r\n"
			+ "	AND sol.id_cen_servicio = ofi.id_oficina\r\n"
			+ "	AND oven.id_ord_venta =  ooven.id_orden\r\n"
			+ "	AND ooven.id_tipo_orden = 5\r\n"
			+ "	AND ooven.id_empresa = 3";

	@Override
	public CentroServicioSoliDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long codOficina = rs.getLong("cod_oficina");
		Long idSolicitud = rs.getLong("id_solicitud");
		return new CentroServicioSoliDTO(codOficina, idSolicitud);
	}
	
	
}
