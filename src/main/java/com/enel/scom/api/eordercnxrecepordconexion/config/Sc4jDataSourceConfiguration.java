package com.enel.scom.api.eordercnxrecepordconexion.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
/*
@Configuration
public class Sc4jDataSourceConfiguration {

	@Bean
	@ConfigurationProperties("orcl.datasource")
	public DataSourceProperties firstDataSourceProperties() {
	    return new DataSourceProperties();
	}
	
	@Bean(name = "firstDataSource")
	@ConfigurationProperties(prefix = "orcl.datasource")
	public DataSource firstDataSource() {
		return firstDataSourceProperties().initializeDataSourceBuilder().build();
	}

	@Bean
	@ConfigurationProperties("postgre.datasource")
	public DataSourceProperties secondDataSourceProperties() {
	    return new DataSourceProperties();
	}
	
	@Bean(name = "secondDataSource")
	@ConfigurationProperties(prefix="postgre.datasource")
	public DataSource secondDataSource() {
	    return secondDataSourceProperties().initializeDataSourceBuilder().build();
	}
	
	@Bean(name = "jdbcTemplate1")
	public JdbcTemplate jdbcTemplateOracle(@Qualifier("firstDataSource") DataSource ds) {
	    return new JdbcTemplate(ds);
	}

	@Bean(name = "jdbcTemplate2")
	public JdbcTemplate jdbcTemplatePostgres(@Qualifier("secondDataSource") DataSource ds ) {
	    return new JdbcTemplate(ds);
	}
	
}
*/