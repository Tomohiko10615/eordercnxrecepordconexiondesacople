package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class TarifaMapper implements RowMapper<String>{

	// R.I. 18/08/2023
	// R.I. CORRECCION 12/09/2023
	// Se cambia a nueva consulta para obtener tarifa
	public static final String SQLORACLE_SELECT_FOR_TARIFA = "SELECT DISTINCT SUBSTR(c.COD_TARIFA,1,3) as codTarifa\r\n"
			+ "	FROM VTA_SRV_VENTA SVEN, ORD_CONEX OCNX, VTA_SOL_SRV_ELE B, fac_tarifa c\r\n"
			+ "	WHERE\r\n"
			+ "		SVEN.ID_ORD_CONEX = OCNX.ID_ORD_CONEX\r\n"
			+ "		AND SVEN.ID_SOL_SRV_ELE = B.ID_SOL_SRV_ELE\r\n"
			+ "		AND OCNX.NRO_ORDEN = ?\r\n"
			+ "		AND b.ID_TARIFA =c.ID_TARIFA";

	@Override
	public String mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		String codTarifa = rs.getString("codTarifa");
		return codTarifa;
	}
	
}
