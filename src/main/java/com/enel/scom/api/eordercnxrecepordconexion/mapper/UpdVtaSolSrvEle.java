package com.enel.scom.api.eordercnxrecepordconexion.mapper;

public class UpdVtaSolSrvEle {

	public static final String SQLORACLE_UPDATE_FOR_VTASOLSRVELE = "UPDATE"
			+ " vta_sol_srv_ele"
			+ " SET fecha_puser = to_date(?,'DD/MM/YYYY HH24:MI:SS')"
//			+ " SET fecha_puser = to_date(substr(REPLACE('?','T',' '), 1,19 ),'YYYY-MM-DD HH24:MI:SS')"
			+ " WHERE id_sol_srv_ele = ?";
	
}
