package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.FwkAuditEventDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.MedHisComponenteDTO;

public class MedHisComponenteMapper implements RowMapper<MedHisComponenteDTO> {

	public static final String SQLORACLESQL_SELECT_FOR_MED_HIS_COMPONENTE_FEC_DESDE = "SELECT ID_HIS_COMPONENTE,\r\n"
			+ "ID_COMPONENTE,\r\n"
			+ "ID_EST_COMPONENTE,\r\n"
			+ "FEC_DESDE,\r\n"
			+ "FEC_HASTA,\r\n"
			+ "ID_UBICACION,\r\n"
			+ "TYPE_UBICACION,\r\n"
			+ "ID_ORDEN \r\n"
			+ "FROM med_his_componente\r\n"
			+ "WHERE ID_COMPONENTE = ?\r\n"
			+ "AND FEC_DESDE = TO_DATE(? , 'YYYY-MM-dd HH24:MI:SS')"
			;
	
	public static final String SQLORACLESQL_SELECT_FOR_MED_HIS_COMPONENTE_FEC_HASTA = "SELECT ID_HIS_COMPONENTE,\r\n"
			+ "ID_COMPONENTE,\r\n"
			+ "ID_EST_COMPONENTE,\r\n"
			+ "FEC_DESDE,\r\n"
			+ "FEC_HASTA,\r\n"
			+ "ID_UBICACION,\r\n"
			+ "TYPE_UBICACION,\r\n"
			+ "ID_ORDEN \r\n"
			+ "FROM med_his_componente\r\n"
			+ "WHERE ID_COMPONENTE = ?\r\n"
			+ "AND FEC_HASTA = TO_DATE(? , 'YYYY-MM-dd HH24:MI:SS')"
			;
	
	public static final String SQLPOSTGRESQL_SELECT_FOR_MED_HIS_COMPONENTE = "SELECT ID_HIS_COMPONENTE,\r\n"
			+ "ID_COMPONENTE,\r\n"
			+ "ID_EST_COMPONENTE,\r\n"
			+ "FEC_DESDE,\r\n"
			+ "FEC_HASTA,\r\n"
			+ "ID_UBICACION,\r\n"
			+ "TYPE_UBICACION,\r\n"
			+ "ID_ORDEN \r\n"
			+ "FROM med_his_componente\r\n"
			+ "WHERE ID_COMPONENTE = ?\r\n"
			+ "ORDER BY FEC_HASTA DESC"
			;
	
	public static final String SQLPOSTGREEQL_INSERT_FOR_MED_HIS_COMPONENTE = "INSERT INTO med_his_componente(id_his_componente,\r\n"
			+ "id_componente,\r\n"
			+ "id_est_componente,\r\n"
			+ "fec_desde,\r\n"
			+ "fec_hasta,\r\n"
			+ "id_ubicacion,\r\n"
			+ "type_ubicacion,\r\n"
			+ "id_orden)\r\n"
			+ "values(?, ?, ?, ?, ?, ?, ?, ?)"
			;
	
	public static final String SQLPOSTGREEQL_UPDATE_FOR_MED_HIS_COMPONENTE = "update med_his_componente\r\n"
			+ "set \r\n"
			+ "id_est_componente = ?,\r\n"
			+ "fec_desde = ?,\r\n"
			+ "fec_hasta = ?\r\n"
			+ "id_ubicacion = ?,\r\n"
			+ "type_ubicacion = ?,\r\n"
			+ "id_orden = ?\r\n"
			+ "where id_componente = ?\r\n"
			+ "and id_his_componente = ?";
	
	@Override
	public MedHisComponenteDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long ID_HIS_COMPONENTE = rs.getLong("ID_HIS_COMPONENTE");
		Long ID_COMPONENTE = rs.getLong("ID_COMPONENTE");
		Long ID_EST_COMPONENTE = rs.getLong("ID_EST_COMPONENTE");
		Date FEC_DESDE = rs.getDate("FEC_DESDE");
		Date FEC_HASTA = rs.getDate("FEC_HASTA");
		Long ID_UBICACION = rs.getLong("ID_UBICACION");
		String TYPE_UBICACION = rs.getString("TYPE_UBICACION");
		Long ID_ORDEN = rs.getLong("ID_ORDEN");
		return new MedHisComponenteDTO(ID_HIS_COMPONENTE, ID_COMPONENTE, ID_EST_COMPONENTE, 
				FEC_DESDE, FEC_HASTA, ID_UBICACION, TYPE_UBICACION, ID_ORDEN);
	}
	
}
