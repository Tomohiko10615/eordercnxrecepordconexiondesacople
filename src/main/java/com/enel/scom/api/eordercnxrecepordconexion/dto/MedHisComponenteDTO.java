package com.enel.scom.api.eordercnxrecepordconexion.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MedHisComponenteDTO {

	private Long id_his_componente;
	private Long id_componente;
	private Long id_est_componente;
	private Date fec_desde;
	private Date fec_hasta;
	private Long id_ubicacion;
	private String type_ubicacion;
	private Long id_orden;

}
