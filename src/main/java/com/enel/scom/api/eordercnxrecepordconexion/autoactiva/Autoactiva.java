package com.enel.scom.api.eordercnxrecepordconexion.autoactiva;

import java.util.List;

import org.springframework.stereotype.Component;

import com.enel.scom.api.eordercnxrecepordconexion.dto.ParametrosDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.RegistroOrdenDTO;

@Component
public class Autoactiva {

	public int iniciar(List<RegistroOrdenDTO> listaRegistroOrden, ParametrosDTO parametros) throws InterruptedException {
		String nombreArchivo = GeneradorPlano.generar(listaRegistroOrden, parametros);
		return EjecutorAutoactiva.ejecutar(nombreArchivo, parametros);
	}
}
