package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class IdSuministroMapper implements RowMapper<Long> {

	public static final String SQLORACLE_SELECT_FOR_IDSUMINISTRO = "SELECT"
			+ " b.id_servicio as idRegistro"
			+ "	FROM nuc_cuenta a, nuc_servicio b, srv_electrico c"
			+ "	WHERE a.id_cuenta = b.id_cuenta"
			+ "	AND b.id_servicio=c.id_servicio"
			+ "	AND b.tipo = 'ELECTRICO'"
			+ "	AND c.id_estado in(0, 5, 4 )"
			+ "	AND a.nro_cuenta = ?";

	@Override
	public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
	
		Long idRegistro = rs.getLong("idRegistro");
		return idRegistro;
	}
	
	
}
