package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.FwkAuditEventDTO;

public class FwkAuditEventMapper implements RowMapper<FwkAuditEventDTO> {

	public static final String SQLORACLESQL_SELECT_FOR_FWK_AUDITEVENT = "SELECT ID_AUDITEVENT,\r\n"
			+ " USECASE,\r\n"
			+ " OBJECTREF,\r\n"
			+ " ID_FK,\r\n"
			+ " FECHA_EJECUCION,\r\n"
			+ " SPECIFIC_AUDITEVENT,\r\n"
			+ " ID_USER\r\n"
			+ " FROM fwk_auditevent\r\n"
			+ " WHERE ID_FK = ?"
			;
	
	public static final String SQLPOSTGREEQL_INSERT_FOR_FWK_AUDITEVENT = "insert into "
			+ " fwk_auditevent (id, usecase, objectref,	id_fk, fecha_ejecucion,	specific_auditevent, id_user)"
			+ " values(?, ?, ?, ?, ?, ?, ?) "
			;
	
	@Override
	public FwkAuditEventDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long id_auditevent = rs.getLong("ID_AUDITEVENT");
		String usecase = rs.getString("USECASE");
		String objectref = rs.getString("OBJECTREF");
		Long id_fk = rs.getLong("ID_FK");
		Date fecha_ejecucion = rs.getDate("FECHA_EJECUCION");
		String specific_auditevent = rs.getString("SPECIFIC_AUDITEVENT");
		Long id_user = rs.getLong("ID_USER");
		return new FwkAuditEventDTO(id_auditevent, usecase, objectref, 
				id_fk, fecha_ejecucion, specific_auditevent, id_user);
	}
	
}
