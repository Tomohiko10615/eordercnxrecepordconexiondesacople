package com.enel.scom.api.eordercnxrecepordconexion.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.enel.scom.api.eordercnxrecepordconexion.dto.ActaConexionDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.AnexosDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.DatosComunesDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.DatosOrdenDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.DatosProcesosDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.LecturasDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.MedidoresDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.OrdenesTransferDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.ParametrosDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.SellosDTO;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.ActaConexionMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.AnexosMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.DatosComunesMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.DatosProcesosMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.LecturasMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.MedidoresMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.OrdenesTransferMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.SellosMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class OrdenesDAO {

	@Autowired
	@Qualifier("jdbcTemplate1")
	private JdbcTemplate jdbcTemplateOracle;
	
	@Autowired
	@Qualifier("jdbcTemplate2") 
	private JdbcTemplate jdbcTemplatePostgres;
	
	public List<DatosOrdenDTO> ObtieneOrden(ParametrosDTO parametros) {//ObtieneOrden
		 
		List<DatosOrdenDTO> listaDatosOrden = new ArrayList<>();
		String sqlOrdenesTransfer = OrdenesTransferMapper.SQLPOSTGRESQL_SELECT_FOR_ORDTRANSFER;
		OrdenesTransferMapper ordenesTransferMapper = new OrdenesTransferMapper();
		
		List<OrdenesTransferDTO> listaOrdenes = jdbcTemplatePostgres.query(sqlOrdenesTransfer, ordenesTransferMapper, parametros.getCodEstPrece());
		for(OrdenesTransferDTO ordenTransfer : listaOrdenes) {
			DatosOrdenDTO datosOrden = new DatosOrdenDTO();

			String sqlDatosProcesos = DatosProcesosMapper.SQLPOSTGRESQL_SELECT_FOR_DATOSPROCESOS;
			String sqlDatosComunes = DatosComunesMapper.SQLPOSTGRESQL_SELECT_FOR_DATOSCOMUNES;
			String sqlAnexos = AnexosMapper.SQLPOSTGRESQL_SELECT_FOR_ANEXOS;
			String sqlMedidores = MedidoresMapper.SQLPOSTGRESQL_SELECT_FOR_MEDIDORES;
			String sqlLecturas = LecturasMapper.SQLPOSTGRESQL_SELECT_FOR_LECTURA;
			//String sqlSellos = SellosMapper.SQLPOSTGRESQL_SELECT_FOR_SELLO;
			String sqlActaConexion = ActaConexionMapper.SQLPOSTGRESQL_SELECT_FOR_ACTACONEX;
			DatosProcesosMapper datosProcesosMapper = new DatosProcesosMapper();
			DatosComunesMapper datosComunesMapper = new DatosComunesMapper();
			AnexosMapper anexosMapper = new AnexosMapper();
			MedidoresMapper medidoresMapper = new MedidoresMapper();
			LecturasMapper lecturasMapper = new LecturasMapper();
			//SellosMapper sellosMapper = new SellosMapper();
			ActaConexionMapper actaConexionMapper = new ActaConexionMapper();
			
			DatosProcesosDTO datosProcesos = new DatosProcesosDTO();
			try {
				datosProcesos = jdbcTemplatePostgres.queryForObject(sqlDatosProcesos, datosProcesosMapper, ordenTransfer.getIdOrdTransfer(), ordenTransfer.getNroRecepciones());
			} catch (Exception e) {
				log.error("error sqlDatosProcesos -> " + e);
				continue;
			}
			
			DatosComunesDTO datosComunes = new DatosComunesDTO();
			try {
				datosComunes  = jdbcTemplatePostgres.queryForObject(sqlDatosComunes, datosComunesMapper, ordenTransfer.getIdOrdTransfer(), ordenTransfer.getNroRecepciones());
			} catch (Exception e) {
				log.error("error sqlDatosComunes -> " + e);
				continue;
			}

			log.info("datosProcesos : " + datosProcesos);
			log.info("datosComunes : " + datosComunes);
			
			//AnexosDTO anexos = jdbcTemplatePostgres.queryForObject(sqlAnexos, anexosMapper, ordenTransfer.getIdOrdTransfer(), ordenTransfer.getNroRecepciones());
			List<AnexosDTO> listaAnexos = new ArrayList<AnexosDTO>();
			try {
				listaAnexos = jdbcTemplatePostgres.query(sqlAnexos, anexosMapper, ordenTransfer.getIdOrdTransfer(), ordenTransfer.getNroRecepciones());
			} catch (Exception e) {
				log.error("error sqlAnexos -> " + e);
				
//				anexos.setNombreAnexo("no encontrado");
//				anexos.setRutaAnexo("no encontrado");
//				anexos.setTipoAnexo("no encontrado");
			}
			log.info("anexos : " + listaAnexos);
			
			List<MedidoresDTO> listaMedidores = new ArrayList<MedidoresDTO>();
			try {
				listaMedidores = jdbcTemplatePostgres.query(sqlMedidores, medidoresMapper, ordenTransfer.getIdOrdTransfer(), ordenTransfer.getNroRecepciones());
			} catch (Exception e) {
				log.error("error sqlMedidores -> " + e );
			}
			
			List<LecturasDTO> listaLecturas = new ArrayList<LecturasDTO>();
			try {
				listaLecturas = jdbcTemplatePostgres.query(sqlLecturas, lecturasMapper, ordenTransfer.getIdOrdTransfer(), ordenTransfer.getNroRecepciones());
			} catch (Exception e) {
				log.error("error sqlLecturas -> " + e );
			}
			//SellosDTO sellos = jdbcTemplatePostgres.queryForObject(sqlSellos, sellosMapper, ordenTransfer.getIdOrdTransfer(), ordenTransfer.getNroRecepciones());
			
			log.info("listaMedidores : " + listaMedidores);
			log.info("listaLecturas : " + listaLecturas);
			
			/*
			SellosDTO sellos = new SellosDTO();
			try {
				sellos = jdbcTemplatePostgres.queryForObject(sqlSellos, sellosMapper, ordenTransfer.getIdOrdTransfer(), ordenTransfer.getNroRecepciones());
			} catch (Exception e) {
				log.error("message", e);
			}
			*/
			
			ActaConexionDTO actaConexion = new ActaConexionDTO();
			try {
				actaConexion = jdbcTemplatePostgres.queryForObject(sqlActaConexion, actaConexionMapper, ordenTransfer.getIdOrdTransfer(), ordenTransfer.getNroRecepciones());
			} catch (Exception e) {
				log.error("error sqlActaConexion -> " + e );
			}
			log.info("actaConexion : " + actaConexion);
			
			datosOrden.setDatosProcesos(datosProcesos);
			datosOrden.setDatosComunes(datosComunes);
			datosOrden.setAnexos(listaAnexos);
			datosOrden.setListaMedidores(listaMedidores);
			datosOrden.setListaLecturas(listaLecturas);
			//datosOrden.setSellos(sellos);
			datosOrden.setActaConexion(actaConexion);
			datosOrden.setOrdenesTransfer(ordenTransfer);
			
			listaDatosOrden.add(datosOrden);
		}
		log.info("listaDatosOrden : " + listaDatosOrden);
		return listaDatosOrden; 
	}
}
