package com.enel.scom.api.eordercnxrecepordconexion.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CargaTagDTO {

	private String nomTag;
	private String nomColumna;
	private String edelnor;
	private String obligatorio;
	private String enArchivo;
	private Integer posicion;
	private Integer indQuery;
	private Integer posArchivo;
	private String tipo;

}
