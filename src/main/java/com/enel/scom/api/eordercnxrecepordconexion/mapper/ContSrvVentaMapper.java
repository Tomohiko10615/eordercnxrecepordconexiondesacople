package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class ContSrvVentaMapper implements RowMapper<Integer> {

	public static final String SQLORACLE_SELECT_FOR_CONTSRVVENTA = "SELECT"
			+ " count(1) as cont"
			+ "	FROM ord_orden oven, vta_srv_venta sven"
			+ "	WHERE oven.nro_orden = ?"
			+ "	AND oven.id_tipo_orden=5"
			+ "	and oven.id_empresa=3"
			+ "	AND oven.id_orden = sven.id_ord_venta"
			+ "	AND sven.estado = 'Anulado'"
			+ "	AND sven.fecha_habilitacion IS NOT NULL";

	@Override
	public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
		Integer cont = rs.getInt("cont");
		return cont;
	}
	
}
