package com.enel.scom.api.eordercnxrecepordconexion.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ServicioElectricoOrdenDTO {

	private Long idDato;
	private Long idSrvElectrico;
	private Long idAlim;
	private Long idSed;
	private Long idLlave;
	private Long idDir;
	private Long idRutaSumin;
	private Long idPcr;
	private Long idSolSrvEle;
	private Long idEstSrvElectrico;
	private Long idConfPres;
	private Long idOrdConex;
	private String nroOrdConex;
	private String codSucEjecutora;
	
}
