package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.BuscarCorrelativoDTO;

public class BuscarCorrelativoMapper implements RowMapper<BuscarCorrelativoDTO> {
    public static final String SQLPOSTGRESQL_SELECT_FOR_CORRELATIVO = "SELECT valor_num FROM com_parametros" 
    + "       WHERE sistema = 'EORDER' AND entidad = 'RUTAS'"
    + "       AND codigo = 'CORRELAT' AND activo = 'S';";

    @Override
    public BuscarCorrelativoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        int valorNum = rs.getInt("valor_num");
        return new BuscarCorrelativoDTO(valorNum);
    }

}
