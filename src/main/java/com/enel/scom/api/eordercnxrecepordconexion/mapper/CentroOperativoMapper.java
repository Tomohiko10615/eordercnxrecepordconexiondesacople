package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class CentroOperativoMapper implements RowMapper<String> {

	public static final String SQLORACLE_SELECT_FOR_CENTROOPERATIVO = "SELECT"
			+ " TRIM(TO_CHAR(COP.COD_OFICINA,'9999999999')) as centroOperativo"
			+ "	FROM VTA_SOLICITUD SOL, VTA_ORD_VTA OVEN, ORD_CONEX CNX, SEG_OFICINA COP, ORD_ORDEN OO"
			+ "	WHERE"
			+ "	SOL.ID_SOLICITUD = OVEN.ID_SOLICITUD"
			+ "	AND OVEN.ID_ORD_VENTA = CNX.ID_ORD_VENTA"
			+ "	AND SOL.ID_CEN_SERVICIO = COP.ID_OFICINA"
			+ "	AND OVEN.ID_ORD_VENTA = OO.ID_ORDEN"
			+ "	AND CNX.NRO_ORDEN = ?"
			+ "	AND OO.NRO_ORDEN = ?";
	
	public static final String SQLORACLE_SELECT_FOR_CENTROOPERATIVO_2 = "SELECT TRIM(TO_CHAR(COP.COD_OFICINA,'9999999999')) as centroOperativo \r\n"
			+ "	FROM VTA_SOLICITUD SOL, VTA_ORD_VTA OVEN, ORD_ORDEN OOVEN, SEG_OFICINA COP\r\n"
			+ "	WHERE\r\n"
			+ "	SOL.ID_SOLICITUD = OVEN.ID_SOLICITUD\r\n"
			+ "	AND SOL.ID_CEN_SERVICIO = COP.ID_OFICINA\r\n"
			+ "	AND OVEN.ID_ORD_VENTA = OOVEN.ID_ORDEN\r\n"
			+ "	AND OOVEN.ID_TIPO_ORDEN = 5\r\n"
			+ "	AND OOVEN.ID_EMPRESA = 3\r\n"
			+ "	AND OOVEN.NRO_ORDEN = ?";

	@Override
	public String mapRow(ResultSet rs, int rowNum) throws SQLException {
		String centroOperativo = rs.getString("centroOperativo");
		return centroOperativo;
	}
	
}
