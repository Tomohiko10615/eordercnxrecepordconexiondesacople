package com.enel.scom.api.eordercnxrecepordconexion.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ParametrosDTO {

	private Long idEmpresa;
	private String idUsuario;
	private String pathCnx;
	private Long codEstProce;
	private Long codEstPrece;
	private Long codEstRecep;
	private Long codEstRecer;
	private Long codEstEnvia;
	private String codErrSY000;
	private String desErrSY000;
	private String codErrSY002;
	private String desErrSY002;
	private String codErrSY013;
	private String desErrSY013;
	private String codErrSY024;
	private String desErrSY024;
	private String codErrSY045;
	private String desErrSY045;
	private String codErrSY099;
	private String desErrSY099;
	private String codErrSY200; // AGREGADO RSOSA INDEPE-5702 19/06/2023
	private String desErrSY200; // AGREGADO RSOSA INDEPE-5702 19/06/2023
	
	private Long nroOrden;
	private String codEmpresa;
	private String codUsuario;
}
