package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class DescripcionProductoMapper implements RowMapper<String> {

	public static final String SQLORACLE_SELECT_FOR_DESPRODUCTO = "SELECT"
			+ " PROD.DESCRIPTION as desProducto"
			+ "	FROM VTA_SRV_VENTA SVEN, ORD_CONEX OCNX, VTA_SOL_SRV_ELE SELE, PRD_PRODUCTO PROD, ORD_ORDEN OO"
			+ "	WHERE"
			+ "	SVEN.ID_ORD_CONEX = OCNX.ID_ORD_CONEX"
			+ "	AND SVEN.ID_SOL_SRV_ELE = SELE.ID_SOL_SRV_ELE"
			+ "	AND SVEN.ID_PRODUCTO = PROD.ID_PRODUCTO"
			+ "	AND OCNX.ID_ORD_VENTA = OO.ID_ORDEN"
			+ "	AND OCNX.NRO_ORDEN = ?"
			+ "	AND OO.NRO_ORDEN = ?"
			;

	@Override
	public String mapRow(ResultSet rs, int rowNum) throws SQLException {
		String desProducto = rs.getString("desProducto");
		return desProducto;
	}
	
	
}
