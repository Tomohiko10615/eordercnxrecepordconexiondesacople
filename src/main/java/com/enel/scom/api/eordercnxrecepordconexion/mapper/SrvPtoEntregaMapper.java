package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.SrvPtoEntregaDTO;

public class SrvPtoEntregaMapper implements RowMapper<SrvPtoEntregaDTO> {

	public static final String SQLORACLE_SELECT_FOR_IDPTOENTREGA = "SELECT"
			+ " id_pto_entrega as id_pto_entrega, '' codigo, '' descPtoEntrega"
			+ " FROM srv_pto_entrega"
			+ " WHERE descripcion = ?";

	public static final String SQLORACLE_SELECT_FOR_DESCRIPPTOENTREGA = "SELECT codigo,"
			+ " descripcion as descPtoEntrega, 0 id_pto_entrega"
			+ "	FROM srv_pto_entrega"
			+ "	WHERE codigo = ?";
	
	@Override
	public SrvPtoEntregaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		SrvPtoEntregaDTO oSrvPtoEntrega= new SrvPtoEntregaDTO();
		oSrvPtoEntrega.setIdPtoEntrega(rs.getLong("id_pto_entrega")+"");
		oSrvPtoEntrega.setCodigo( rs.getString("codigo"));
		oSrvPtoEntrega.setDescripcion(rs.getString("descPtoEntrega"));		
		return oSrvPtoEntrega;		
	}
	
}
