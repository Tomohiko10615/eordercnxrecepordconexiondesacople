package com.enel.scom.api.eordercnxrecepordconexion.mapper;

public class UpdOrdenVentaMapper {

	public static final String SQLPOSTGRE_UPDATE_FOR_ORDENVENTA = "UPDATE"
			+ " vta_ord_vta"
			+ "	SET paralizada = 'S'"
			+ " WHERE id_ord_venta = ?";
	
	public static final String SQLORACLE_UPDATE_FOR_ORDENVENTA2 = "UPDATE"
			+ " VTA_ORD_VTA"
			+ "	SET PARALIZADA = 'N'"
			+ "	WHERE ID_ORD_VENTA = ?";
}
