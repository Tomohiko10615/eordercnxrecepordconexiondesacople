package com.enel.scom.api.eordercnxrecepordconexion.dto;

import java.util.Date;
import com.enel.scom.api.eordercnxrecepordconexion.autoactiva.GeneradorPlano;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MedidorNumMarModDTO {

	private String accionmedidor;
	private String fechaconexion;
	private String nummedidor;
	private String modmedidor;
	private String marmedidor;

}
