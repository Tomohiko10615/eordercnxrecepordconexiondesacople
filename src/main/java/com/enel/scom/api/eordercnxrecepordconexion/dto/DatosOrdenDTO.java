package com.enel.scom.api.eordercnxrecepordconexion.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DatosOrdenDTO {

	private DatosProcesosDTO datosProcesos;
	private DatosComunesDTO datosComunes;
	private List<AnexosDTO> anexos; 
	private List<MedidoresDTO> listaMedidores;
	private List<LecturasDTO> listaLecturas; 
	private SellosDTO sellos; 
	private ActaConexionDTO actaConexion; 
	private OrdenesTransferDTO ordenesTransfer;
}
