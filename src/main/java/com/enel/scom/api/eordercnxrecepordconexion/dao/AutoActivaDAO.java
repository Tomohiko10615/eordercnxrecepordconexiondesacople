package com.enel.scom.api.eordercnxrecepordconexion.dao;

import java.sql.SQLException;
import java.sql.Types;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

//import org.hibernate.mapping.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.enel.scom.api.eordercnxrecepordconexion.autoactiva.GeneradorPlano;
import com.enel.scom.api.eordercnxrecepordconexion.dto.BuscarCorrelativoDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.CaeTmpClienteAlterDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.CaeTmpClienteDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.CentroServicioSoliDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.CompEstUbicacionDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.DatosCorrelativoMoverDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.DireccionOrdenDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.EorOrdTransferDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.FechaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.FwkAuditEventDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.MedComponenteDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.MedHisComponenteDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.MedLecUltimaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.MedidorNumMarModDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.OrdVentaWkfDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.ParametrosDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.PcrRefDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.RegAutActivaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.RegistroOrdenDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.RemediacionVtaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.RutaAutLecturaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.RutaLecturaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.ServicioElectricoOrdenDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.SrvPtoEntregaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.SrvVentaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.SuministroDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.UbiRutaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.ValidarSolicitudDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.VtaAutoActDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.VtaAutoDtDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.VtaAutoTablaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.BuscarCorrelativoMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.BuscarMedidasMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.CaeTmpClienteAlterMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.CaeTmpClienteMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.CentroOperativoMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.CentroServicioSoliMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.CompEstUbicacionMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.ContSrvVentaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.DatosCorrelativoMoverMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.DireccionOrdenMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.EorOrdTransferMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.ExtraDatosCorrelativoMoverMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.FechaHoraMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.FwkAuditEventMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.IdSuministroMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.InsAuditoriaVentaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.InsSrvElectricoMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.InsVtaAutoActMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.InsVtaAutoCompMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.InsVtaAutoCompMedMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.InsVtaAutoDtMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.MedComponenteMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.MedHisComponenteMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.MedLecUltimaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.MedidorNumMarModMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.OrdVentaWkfMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.OrdenConexMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.PcrRefMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.RemediacionVtaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.RutaAutLecturaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.RutaLecturaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.SeqVtaAutoActMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.SeqVtaAutoCompMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.SeqVtaAutoCompMedMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.SeqVtaAutoDetMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.ServicioElectricoOrdenMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.SrvAlimentadorMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.SrvElectricoMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.SrvLlaveMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.SrvPcrMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.SrvPtoEntregaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.SrvSedMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.SrvSetMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.SrvVentaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.SuministroMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.TarifaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.UbiRutaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.UpdEorOrdTransferDetMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.UpdEorOrdTransferMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.UpdOrdConexMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.UpdOrdenVentaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.UpdSrvElectricoMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.UpdSrvVentaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.UpdVtaAutoDtMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.UpdVtaSolSrvEle;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.UpdWkfWorkflowMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.ValidarSolicitudMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.VtaAutoActMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.VtaAutoTablaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.util.Log;

import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.dynamic.scaffold.subclass.ConstructorStrategy;

@Slf4j
@Component
public class AutoActivaDAO {

	@Autowired
	@Qualifier("jdbcTemplate1")
	private JdbcTemplate jdbcTemplateOracle;
	
	@Autowired
	@Qualifier("jdbcTemplate2") 
	private JdbcTemplate jdbcTemplatePostgres;
	
	/*
	 * @Autowired
	 * 
	 * @Qualifier("jdbcTemplateMOD") private JdbcTemplate jdbcTemplateOracleMOD;
	 */
	
	//variables para reporte
	Integer contOrdActivadas = 0;
	List<Long> listaOrdVtaActiv = new ArrayList<Long>();
	List<Long> listaOrdCnxActiv = new ArrayList<Long>();
	Integer cantTotalOrd = 0;
	
	String vtmpError;
	String codOperacion = "";
	String observacion = "";
	ParametrosDTO parametrosDto;
	
	String codTarifa;
	String centroOperativo;
	Long lCentroOperativo;
	Long idSrvElectricoXy;
	Long idSrvElectricoXy2;
	
	int cant_error=0;
	
	int esBT6 = 0;
	String origen="";
	String fecha_lect="";
	int pautActivar;
	String pcod="";
	Long srv;
	
	long regAutActiva_idMedida=0L;
	int regAutActiva_CantMedidor=0;
	
	SuministroDTO suministro = new SuministroDTO();
	
	RegAutActivaDTO paut = new RegAutActivaDTO();
	
	Long nroOrdConex;
	
	ArrayList<DatosCorrelativoMoverDTO>listDatosCorrMov= new ArrayList<>();
	
	public void procesoAutoActiva(List<RegistroOrdenDTO> listaRegistroOrden, ParametrosDTO parametros) {
		
		log.info("**********INICIO2 PROCESO procesoAutoActiva**********");
		Integer contador2 = 0;
		cantTotalOrd = listaRegistroOrden.size();
		//proceso()
		origen="EORDER";
		parametrosDto=parametros;
		
		//traza de Ordenes
		log.info("Cantidad de ordenes a recorrer , " + listaRegistroOrden.size());
		log.info("Lista de Ordenes : ");

		for (RegistroOrdenDTO registroOrdenAlter : listaRegistroOrden) {
			log.info("OrdenVenta : " + registroOrdenAlter.getNumOrden());
		}
		
		log.info("INICIO2 FOR registroOrden");
		for(RegistroOrdenDTO registroOrden : listaRegistroOrden) {
			contador2++;
			log.info("[CONTADOR2] Itereacion num : " + contador2);
			vtmpError="";
			log.info("se limpio la variable de Clase vtmpError ");
			/////////////////////////////////
			if(registroOrden.getNumOrden()==null)
			{
				registroOrden.setNumOrden("");
			}			
			if(registroOrden.getNumCuenta()==null)
			{
				registroOrden.setNumCuenta("");
			}
			if(registroOrden.getSumDerecho()==null)
			{
				registroOrden.setSumDerecho("0");
			}
			else
			{
				if(registroOrden.getSumDerecho()=="")
				{
					registroOrden.setSumDerecho("0");
				}	
			}
			if(registroOrden.getSumIzquierdo()==null)
			{
				registroOrden.setSumIzquierdo("0");
			}
			else
			{
				if(registroOrden.getSumIzquierdo()=="")
				{
					registroOrden.setSumIzquierdo("0");
				}	
			}
			if(registroOrden.getNumPcr()==null)
			{
				registroOrden.setNumPcr("0");
			}
			else
			{
				if(registroOrden.getNumPcr()=="")
				{
					registroOrden.setNumPcr("0");
				}	
			}
			if(registroOrden.getSumPcrRef()==null)
			{
				registroOrden.setSumPcrRef("0");
			}
			else
			{
				if(registroOrden.getSumPcrRef()=="")
				{
					registroOrden.setSumPcrRef("0");
				}	
			}
			
			if(registroOrden.getLecturaInstall()==null)
			{
				registroOrden.setLecturaInstall("0");
			}
			else
			{
				if(registroOrden.getLecturaInstall()=="")
				{
					registroOrden.setLecturaInstall("0");
				}	
			}
			/*
			if(registroOrden.getSet()==null)
			{
				registroOrden.setSet("");
			}
			if(registroOrden.getAlimentador()==null)
			{
				registroOrden.setAlimentador("");
			}
			if(registroOrden.getSed()==null)
			{
				registroOrden.setSed("");
			}
			if(registroOrden.getLlave()==null)
			{
				registroOrden.setLlave("");
			}*/
			
			Long numOrdenLong = Long.valueOf(registroOrden.getNumOrden());
			Long numCuentaLong = Long.valueOf(registroOrden.getNumCuenta());
			
			/////////////////////////////////
			RegAutActivaDTO regAutActiva = new RegAutActivaDTO();
			regAutActiva.setIdPcr(0L);
			regAutActiva.setIdPcrTension(0L);
			regAutActiva.setMedidorInstallIdMedidor(0L);
			
			log.info(String.format("\nProcesando -> Orden:[%d] - Cuenta:[%d]\n", numOrdenLong,numCuentaLong));
			Log.logError2(String.format("\n%d\t%d\t",numOrdenLong,numCuentaLong));
			
			//Long codRutaAuto = 0L;
			
			regAutActiva.setCrearRuta(0L);
			
			//String sqlSrvPtoEntrega = SrvPtoEntregaMapper.SQLORACLE_SELECT_FOR_IDPTOENTREGA;
			
			SrvElectricoMapper srvElectricoMapper = new SrvElectricoMapper();
			
			//SrvPtoEntregaMapper srvPtoEntregaMapper = new SrvPtoEntregaMapper();
			//VtaAutoTablaMapper vtaAutoTablaMapper = new VtaAutoTablaMapper();
			String sqlSrvElectrico = SrvElectricoMapper.SQLORACLE_SELECT_FOR_IDSRVELECTRICO;
			String sqlSrvElectrico2 = SrvElectricoMapper.SQLORACLE_SELECT_FOR_IDSRVELECTRICO2;
			String sqlInsSrvElectrico = InsSrvElectricoMapper.SQLORACLE_INSERT_FOR_SRVELECTRICO;
			String sqlUpdSrvElectrico = UpdSrvElectricoMapper.SQLORACLE_UPDATE_FOR_SRVELECTRICO;
			
			// TFRC 20210414 - tarifa - ini
			//GetTarifa
			if (GetTarifa(registroOrden, numCuentaLong) == false) // TFRC 20210405 - obtiene tarifa
			{
				log.error("Error en obtener tarifa\n");
				continue; //return FALSE;
			}
			
			if(codTarifa.compareTo("BT6")==0) {
				esBT6 = 1;
			}
			
			log.info("valor esBT6 es igual a " + esBT6);
			
			if(!GetCentroOperativo(registroOrden, numOrdenLong))
			  {
				guardarTransferenciaOrden(registroOrden,parametros,codOperacion,observacion,numOrdenLong, numCuentaLong);  /* MVC 20141029 */
				//limpiar_registros(&reg, &datos);
				continue;
			  }
			
			
			if(origen=="EORDER")
			{
				try {
					idSrvElectricoXy = jdbcTemplateOracle.queryForObject(sqlSrvElectrico, srvElectricoMapper, numOrdenLong, numCuentaLong);
				} catch(EmptyResultDataAccessException e) {
					idSrvElectricoXy = 0L;
					try {
						idSrvElectricoXy2 = jdbcTemplateOracle.queryForObject(sqlSrvElectrico2, srvElectricoMapper, numOrdenLong, numCuentaLong);
					} catch(EmptyResultDataAccessException er) {
						idSrvElectricoXy2 = 0L;
						log.error("Error: SELECT SELE.ID_SRV_ELECTRICO  INTO :id_srv_electrico_xy - id para insertar");
						Log.logError2("Error: SELECT SELE.ID_SRV_ELECTRICO  INTO :id_srv_electrico_xy - id para insertar");						
						continue;
					}
					try
					{
					Log.ifnMsgToLogFile("valores a insertar para query sqlInsSrvElectrico : " +
					" idSrvElectricoXy2 : " + idSrvElectricoXy2 +
					" registroOrden.getLatitudMedidor() " + registroOrden.getLatitudMedidor() +
					" registroOrden.getLongitudMedidor()" + registroOrden.getLongitudMedidor() + 
					" registroOrden.getLatitudEmpalme() " + registroOrden.getLatitudEmpalme() + 
					" registroOrden.getLongitudEmpalme() " + registroOrden.getLongitudEmpalme()
					);
					Double latitudMedidor = Double.valueOf(registroOrden.getLatitudMedidor());
					Double longitudMedidor = Double.valueOf(registroOrden.getLongitudMedidor());
					Double latitudEmpalme = Double.valueOf(registroOrden.getLatitudEmpalme());
					Double longitudEmpalme = Double.valueOf(registroOrden.getLongitudEmpalme());
					jdbcTemplateOracle.update(sqlInsSrvElectrico, idSrvElectricoXy2, latitudMedidor, longitudMedidor, 
							latitudEmpalme, longitudEmpalme);
					} catch(EmptyResultDataAccessException err) {
						log.error("EXEC SQL INSERT INTO EOR_SRV_ELEC_XY...");
						Log.logError2("EXEC SQL INSERT INTO EOR_SRV_ELEC_XY...");
						continue;
					} catch(Exception ex) {
						Log.logError2("EXEC SQL INSERT INTO EOR_SRV_ELEC_XY... ");
					}
				}
				
				if(idSrvElectricoXy > 0) {
					Double latitudmedidor = 0.0;
					Double longitudmedidor = 0.0;
					Double latitudempalme = 0.0;
					Double longitudempalme = 0.0;
					
					try {
						latitudmedidor = Double.parseDouble(registroOrden.getLatitudMedidor());
					} catch (Exception e) {
						log.error("error en latitudmedidor , Excepcion -> " + e);
					}
					try {
						longitudmedidor = Double.parseDouble(registroOrden.getLongitudMedidor());
					} catch (Exception e) {
						log.error("error en longitudmedidor , Excepcion -> " + e);
					}
					try {
						latitudempalme = Double.parseDouble(registroOrden.getLatitudEmpalme());
					} catch (Exception e){
						log.error("error en latitudempalme , Excepcion -> " + e);
					}
					try {
						longitudempalme = Double.parseDouble(registroOrden.getLongitudEmpalme());
					} catch (Exception e){
						log.error("error en longitudempalme , Excepcion -> " + e);
					}

					
					try
					{
					jdbcTemplateOracle.update(sqlUpdSrvElectrico, latitudmedidor, longitudmedidor, latitudempalme,
							longitudempalme, idSrvElectricoXy);
					}
					catch(EmptyResultDataAccessException e)
					{
						log.error("Error: UPDATE EOR_SRV_ELEC_XY ...");
						Log.logError2("Error: UPDATE EOR_SRV_ELEC_XY ...");
						continue;
					}
					catch(Exception ex)
					{
						log.error(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_act...",ex,ex.getMessage()));
						Log.logError2(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_act...",ex,ex.getMessage()));
						continue;
					}
				}
				
				////Fin Guardar datos de XY///
				//3124
				if ((lCentroOperativo==5400 || lCentroOperativo==5100 ) && esBT6 != 1) {
					
					codOperacion = parametros.getCodErrSY000();
					observacion = parametros.getDesErrSY000();
					
					guardarTransferenciaOrden(registroOrden, parametros, codOperacion, observacion, numOrdenLong, numCuentaLong);
					continue;
				}
			}
			/*** Fin Agrega EORDER ***/
			
			log.info("Validaciones:");
			if(validacion_datos( registroOrden, regAutActiva, numOrdenLong, numCuentaLong)==0)
			{
				Log.logError2("00\tNo se pudieron validar datos");
				guardarTransferenciaOrden(registroOrden, parametros, codOperacion, observacion, numOrdenLong, numCuentaLong);
				continue;
			}
			regAutActiva.setMedidorInstallIdMedida(regAutActiva_idMedida);
			regAutActiva.setCantMedidor(regAutActiva_CantMedidor);
						
			log.info("\nValidaciones Ok.\nCalculo de Datos Faltantes:");
			
			log.info("limpiar variable listDatosCorrMov");
			listDatosCorrMov.clear();
			log.info("tamaño de listDatosCorrMov : " + listDatosCorrMov.size());
			if(calcularDatosFaltantes(regAutActiva, registroOrden, numOrdenLong)==0)
			{
				Log.logError2("00\tNo se pudo calcular datos faltantes");
				log.error("00\tNo se pudo calcular datos faltantes");
				guardarTransferenciaOrden(registroOrden, parametros, codOperacion, observacion, numOrdenLong, numCuentaLong);
				continue;
			}
			ArrayList<DatosCorrelativoMoverDTO> datosCorrelativoMoverDTOs=listDatosCorrMov;
			
			log.info("Calculo de Datos Faltantes Ok.\nRegistro de Autoactivacion[ID_Auto_Act]:\n");
			Log.logError2("Validaciones y Calculos OK\t");
			log.info("Validaciones y Calculos OK\t");
			Long idAutoAct=registrarOrden(registroOrden,regAutActiva,datosCorrelativoMoverDTOs, numOrdenLong);
			//Long idAutoAct = registrarOrden(registroOrden, regAutActiva);
			if(idAutoAct <=0) {
				codOperacion = parametros.getCodErrSY002();
				observacion = parametros.getDesErrSY002();
				guardarTransferenciaOrden(registroOrden, parametros, codOperacion, observacion, numOrdenLong, numCuentaLong);
				continue;
				
			}
			else
			{
				log.info("grabar datos para la activacion automatica , valor vtmpErr : " + vtmpError);
				/*grabar datos para la activacion automatica*/
				if(origen.equals("EORDER"))
				{
					codOperacion = parametros.getCodErrSY000();
					observacion = parametros.getDesErrSY000();
					guardarTransferenciaOrden(registroOrden, parametros, codOperacion, observacion, numOrdenLong, numCuentaLong);
				}
				log.info(String.format("Activacion Automatica: %d \n",idAutoAct));
				Log.logError2(String.format("Activacion Automatica: %d \n",idAutoAct));
				
				/*intentar activar solo si la orden esta completa*/
				log.info("Activar orden numero : "+ numOrdenLong);
				log.info("numero orden conexion  : "+ nroOrdConex);
				if(pautActivar==1){
					activar_orden(numOrdenLong, nroOrdConex);//idAutoAct
				}
				else{
					log.error("Pendiente de Activar. Aun quedan Servicios por Ejecutar\n");
					Log.logError2("Pendiente de Activar. Aun quedan Servicios por Ejecutar\n");
				}
			}
			log.info("[CONTADOR2] Fin iteracion numero:" + contador2);
		} // fin for
		//reporte de ordenes activadas
		log.info("Cantidad de ordenes activadas : " + contOrdActivadas );
		log.info("Las Ordenes activadas son :");
		for (Long ordenVentaReporte : listaOrdVtaActiv) {
			log.info("OrdenVenta : " + ordenVentaReporte);
		}
		for (Long ordenConexReporte : listaOrdCnxActiv) {
			log.info("OrdenConexion : " + ordenConexReporte);
		}
		log.info("Cantidad de ordenes NO activadas : " + (cantTotalOrd - contOrdActivadas));
	}
	
	@Transactional
	public void guardarTransferenciaOrden(RegistroOrdenDTO registroOrden, ParametrosDTO parametros, String in_codOperacion, String in_observacion, Long numOrdenLong, Long numCuentaLong) {
		
		int iErrTmp=0;
		nroOrdConex=null;
		EorOrdTransferDTO ordTransfer = new EorOrdTransferDTO();
		
		String sqlOrdenConex = OrdenConexMapper.SQLORACLE_SELECT_FOR_NROORDENCONEX;
		String sqlEorOrdTransfer = EorOrdTransferMapper.SQLPOSTGRESQL_SELECT_FOR_ORDTRANSFER;
		String sqlUpdEorOrdTransfer = UpdEorOrdTransferMapper.SQLPOSTGRE_UPDATE_FOR_EORORDTRANSFER;
		String sqlUpdEorOrdTransferDet = UpdEorOrdTransferDetMapper.SQLPOSTGRE_UPDATE_FOR_ERRORORORDTRANSFERDET;
		OrdenConexMapper ordenConexMapper = new OrdenConexMapper();
		EorOrdTransferMapper eorOrdTransferMapper = new EorOrdTransferMapper();
		
		if(vtmpError==null)
		{
			vtmpError="";
		}
		
		if(vtmpError.length()>0)
		{
			SetErrGen(vtmpError);
			in_observacion=observacion;
			in_codOperacion=codOperacion;
		}
		else
		{
			in_codOperacion=parametros.getCodErrSY000();
			in_observacion="";
		}

		
		try {
			nroOrdConex = jdbcTemplateOracle.queryForObject(sqlOrdenConex, ordenConexMapper, numOrdenLong, numCuentaLong);
		} catch(EmptyResultDataAccessException e) {
			nroOrdConex = 0L;
			Log.logError("ERROR SELECT OCNX.NRO_ORDEN ");
			iErrTmp=1;
		}
		
		String nroOrdConexString = String.valueOf(nroOrdConex);
		
		try {
			ordTransfer = jdbcTemplatePostgres.queryForObject(sqlEorOrdTransfer, eorOrdTransferMapper, parametros.getCodEstPrece(), nroOrdConexString);
		} catch(EmptyResultDataAccessException e) {
			//ordTransfer.setCodTipoTDC("");
			//ordTransfer.setIdOrdTransfer(0L);
			//ordTransfer.setNroRecepciones(0L);
			Log.logError("ERROR SELECT ID_ORD_TRANSFER ");
			iErrTmp=1;
		}
		
		try
		{
			jdbcTemplatePostgres.update(sqlUpdEorOrdTransfer, in_codOperacion, in_observacion, in_codOperacion, parametros.getCodErrSY000(), 
				parametros.getCodEstRecep(), parametros.getCodEstRecer(), ordTransfer.getIdOrdTransfer());
		}
		catch(Exception ex)
		{
			iErrTmp=1;
		}
		try
		{
			jdbcTemplatePostgres.update(sqlUpdEorOrdTransferDet, in_codOperacion, ordTransfer.getIdOrdTransfer(), ordTransfer.getNroRecepciones());
		}
		catch(Exception ex)
		{
			iErrTmp=1;
		}
		if(iErrTmp>0)
		{
			throw new NullPointerException("iErrTmp >0");
		}
		else
		{
			log.info("Commit...");
		}
	}

	//registrar_orden
	public Long registrarOrden(RegistroOrdenDTO registroOrden, RegAutActivaDTO regAutActiva,
			ArrayList<DatosCorrelativoMoverDTO> datosCorrelativoMoverDTOs, Long numOrdenLong) {
	
		log.info("ingreso a registrarOrden");
		String sqlSeqVtaAutoAct = SeqVtaAutoActMapper.SQLORACLE_SELECT_FOR_SEQVTAAUTOACT;
		String sqlInsVtaAutoAct = InsVtaAutoActMapper.SQLORACLE_INSERT_FOR_VTAAUTOACT;
		String sqlSeqVtaAutoDt = SeqVtaAutoDetMapper.SQLORACLE_SELECT_FOR_SEQVTAAUTODT;
		String sqlInsVtaAutoDt = InsVtaAutoDtMapper.SQLORACLE_INSERT_FOR_VTAAUTODT;
		String sqlUpdVtaAutoDt = UpdVtaAutoDtMapper.SQLORACLE_UPDATE_FOR_VTAAUTODT;
		String sqlUpdVtaAutoDt2 = UpdVtaAutoDtMapper.SQLORACLE_UPDATE_FOR_VTAAUTODT2;
		String sqlUpdVtaAutoDt3 = UpdVtaAutoDtMapper.SQLORACLE_UPDATE_FOR_VTAAUTODT3;
		String sqlSeqVtaAutoComp = SeqVtaAutoCompMapper.SQLORACLE_SELECT_FOR_SEQVTAAUTOCOMP;
		String sqlInsVtaAutoComp = InsVtaAutoCompMapper.SQLORACLE_INSERT_FOR_VENTAAUTOCOMP;
		String sqlSeqVtaAutoCompMed = SeqVtaAutoCompMedMapper.SQLORACLE_SELECT_FOR_SEQVTAAUTOCOMPMED;
		String sqlInsVtaAutoCompMed = InsVtaAutoCompMedMapper.SQLORACLE_INSERT_FOR_VTAAUTOCOMPMED;
		
		SeqVtaAutoActMapper seqVtaAutoActMapper = new SeqVtaAutoActMapper();
		SeqVtaAutoDetMapper seqVtaAutoDtMapper = new SeqVtaAutoDetMapper();
		SeqVtaAutoCompMapper seqVtaAutoCompMapper = new SeqVtaAutoCompMapper();
		SeqVtaAutoCompMedMapper seqVtaAutoCompMedMapper = new SeqVtaAutoCompMedMapper();
		
		Double acometidaLong = 0.0;
		try {
			acometidaLong = Double.parseDouble(registroOrden.getAcometida());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		Long seqVtaAutoAct = jdbcTemplateOracle.queryForObject(sqlSeqVtaAutoAct, seqVtaAutoActMapper);
		Long seqVtaAutoDt = jdbcTemplateOracle.queryForObject(sqlSeqVtaAutoDt, seqVtaAutoDtMapper);

		try {
			jdbcTemplateOracle.update(sqlInsVtaAutoAct, seqVtaAutoAct, numOrdenLong);
		}
		catch(EmptyResultDataAccessException e)
		{
			log.error(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_act...",e,e.getMessage()));
			Log.logError2(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_act...",e,e.getMessage()));
			return 0L;
		}
		catch (Exception e) {
			log.error("Error al insertar sqlInsVtaAutoAct");
			log.error(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_act...",e,e.getMessage()));
			Log.logError2(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_act...",e,e.getMessage()));
			return 0L;
		}
		
		try {
			/*
			jdbcTemplateOracle.update(sqlInsVtaAutoDt, seqVtaAutoDt, seqVtaAutoAct, regAutActiva.getIdSrvVenta(), regAutActiva.getIdPtoEntrega(), regAutActiva.getProp_empalme(),
					regAutActiva.getCapInterruptor(), regAutActiva.getSisProteccion(), regAutActiva.getCajaMedicion(), acometidaLong, regAutActiva.getIdSet(),
					regAutActiva.getIdAlim(), regAutActiva.getIdSed(), regAutActiva.getIdLlave(), regAutActiva.getTipoConductor());
					*/
			jdbcTemplateOracle.update(sqlInsVtaAutoDt, seqVtaAutoDt, seqVtaAutoAct, paut.getIdSrvVenta(), paut.getIdPtoEntrega(), paut.getProp_empalme(),
					paut.getCapInterruptor(), paut.getSisProteccion(), paut.getCajaMedicion(), acometidaLong, paut.getIdSet(),
					paut.getIdAlim(), paut.getIdSed(), paut.getIdLlave(), paut.getTipoConductor());
		}
		catch(EmptyResultDataAccessException e)
		{
			log.error(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_dt...",e,e.getMessage()));
			Log.logError2(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_dt...",e,e.getMessage()));
			return 0L;
		}
		catch (Exception e) {
			log.error("Datos enviado , sqlInsVtaAutoDt: " + seqVtaAutoDt+ " ,  " + seqVtaAutoAct+ " ,  " + regAutActiva.getIdSrvVenta()+ " ,  " + regAutActiva.getIdPtoEntrega()+ " ,  " + regAutActiva.getProp_empalme()+ " ,  " +
					regAutActiva.getCapInterruptor()+ " ,  " + regAutActiva.getSisProteccion()+ " ,  " + regAutActiva.getCajaMedicion()+ " ,  " + acometidaLong+ " ,  " + regAutActiva.getIdSet()+ " ,  " +
					regAutActiva.getIdAlim()+ " ,  " + regAutActiva.getIdSed()+ " ,  " + regAutActiva.getIdLlave()+ " ,  " + regAutActiva.getTipoConductor());
			log.error("Error al insertar sqlInsVtaAutoDt ,/n excepcion: " + e);
			log.error(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_dt...",e,e.getMessage()));
			Log.logError2(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_dt...",e,e.getMessage()));
			return 0L;
		}
		
		if(regAutActiva.getCajaToma()==null)
		{
			regAutActiva.setCajaToma("");
		}
		
		if(regAutActiva.getCajaToma().length() > 0) {
			jdbcTemplateOracle.update(sqlUpdVtaAutoDt, regAutActiva.getCajaToma(), seqVtaAutoDt);
		}
		
		if(regAutActiva.getIdSumDer() == null)
			regAutActiva.setIdSumDer(0L);
		
		if(regAutActiva.getIdSumDer()>0) {
			jdbcTemplateOracle.update(sqlUpdVtaAutoDt2, regAutActiva.getIdSumDer(), seqVtaAutoDt);
		}
		
		if(regAutActiva.getIdSumIzq() == null)
			regAutActiva.setIdSumIzq(0L);
		
		if(regAutActiva.getIdSumIzq()>0) {
			jdbcTemplateOracle.update(sqlUpdVtaAutoDt3, regAutActiva.getIdSumIzq(), seqVtaAutoDt);
		}
		
		if(regAutActiva.getCantMedidor() == null)
			regAutActiva.setCantMedidor(0);
		
		
		///////////////////////////////////////////////////////////
		/*
		String fechaLecturaInstalacion = registroOrden.getFecLecturaInstall();
		SimpleDateFormat formato2 = new SimpleDateFormat("yyyy-MM-dd"); 
		Date fechaLecInstalDate;
		try {
			fechaLecInstalDate= formato2.parse(fechaLecturaInstalacion);
		}catch (ParseException e) {
			fechaLecInstalDate = null;
			e.printStackTrace();
		}
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY");  
		String strDate = dateFormat.format(fechaLecInstalDate);  
		System.out.println("strDate es : " + strDate);
		registroOrden.setFecLecturaInstall(strDate);
		*/
        ///////////////////////////////////////////////////////////
		
		for(int i=0; i<regAutActiva.getCantMedidor(); i++) {
			
			if(i==0) {
				if(regAutActiva.getMedidorInstallIdMedidor()>0) {
					Long seqVtaAutoComp = jdbcTemplateOracle.queryForObject(sqlSeqVtaAutoComp, seqVtaAutoCompMapper);
					try
					{
					jdbcTemplateOracle.update(sqlInsVtaAutoComp, seqVtaAutoComp, seqVtaAutoAct, regAutActiva.getIdSrvVenta(), registroOrden.getFecLecturaInstall(),
							paut.getProp_empalme(), regAutActiva.getMedidorInstallIdMedidor(), registroOrden.getNovedadInstall());
					}
					catch(EmptyResultDataAccessException e)
					{
						log.error(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_comp...",e,e.getMessage()));
						Log.logError2(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_comp...",e,e.getMessage()));
						return 0L;
					}
					catch(Exception e)
					{
						log.error(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_comp...",e,e.getMessage()));
						Log.logError2(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_comp...",e,e.getMessage()));
						return 0L;
					}
					Long seqVtaAutoCompMed = jdbcTemplateOracle.queryForObject(sqlSeqVtaAutoCompMed, seqVtaAutoCompMedMapper);
						Double lectInstal = 0.0;
						try {
							lectInstal=Double.parseDouble(registroOrden.getLecturaInstall()); 
						} catch (Exception e) {
							log.error("error con lectInstal , Exception -> " + e);
						}
						
						try
						{
						jdbcTemplateOracle.update(sqlInsVtaAutoCompMed, seqVtaAutoCompMed,seqVtaAutoComp, regAutActiva.getMedidorInstallIdMedida(),lectInstal);// registroOrden.getLecturaInstall());
						}
						catch(EmptyResultDataAccessException e)
						{
							log.error(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_comp_med...",e,e.getMessage()));
							Log.logError2(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_comp_med...",e,e.getMessage()));
							return 0L;
						}
						catch(Exception e)
						{
							log.error(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_comp_med...",e,e.getMessage()));
							Log.logError2(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_comp_med...",e,e.getMessage()));
							return 0L;
						}					
				}
			}
			if(i==1) {
				if(regAutActiva.getMedidorRetIdMedidor()>0) {
					Long seqVtaAutoComp = jdbcTemplateOracle.queryForObject(sqlSeqVtaAutoComp, seqVtaAutoCompMapper);
					try
					{
					jdbcTemplateOracle.update(sqlInsVtaAutoComp, seqVtaAutoComp, seqVtaAutoAct, regAutActiva.getIdSrvVenta(), registroOrden.getFecLecturaRet(),
							paut.getProp_empalme(), regAutActiva.getMedidorRetIdMedidor(), registroOrden.getNovedadRet());
					}
					catch(EmptyResultDataAccessException e)
					{
						log.error(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_comp...",e,e.getMessage()));
						Log.logError2(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_comp...",e,e.getMessage()));
						return 0L;
					}
					catch(Exception e)
					{
						log.error(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_comp...",e,e.getMessage()));
						Log.logError2(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_comp...",e,e.getMessage()));
						return 0L;
					}
					Long seqVtaAutoCompMed = jdbcTemplateOracle.queryForObject(sqlSeqVtaAutoCompMed, seqVtaAutoCompMedMapper);
					
					try
					{
						jdbcTemplateOracle.update(sqlInsVtaAutoCompMed, seqVtaAutoCompMed, regAutActiva.getMedidorRetIdMedida(), registroOrden.getLecturaRet());
					}
					catch(EmptyResultDataAccessException e)
					{
						log.error(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_comp_med...",e,e.getMessage()));
						Log.logError2(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_comp_med...",e,e.getMessage()));
						return 0L;
					}
					catch(Exception e)
					{
						log.error(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_comp_med...",e,e.getMessage()));
						Log.logError2(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_comp_med...",e,e.getMessage()));
						return 0L;
					}
				}
			}
			
		}
		
		//MARCA ED 2
		
		/* Insertar datos de VTA_AUTO_RUTA, si se le creo alguna 1-Nueva Ruta calculada 2-Nueva Ruta arch Entrada*/
		//	if(vaut.crear_ruta==1 || vaut.crear_ruta==2){
		if(regAutActiva.getCrearRuta() == 1 || regAutActiva.getCrearRuta() == 2) {
			log.info("ingreso condicion regAutActiva.getCrearRuta() == 1 || regAutActiva.getCrearRuta() == 2");
			String sqlSeqVtaAutoRuta = "select SQVTAAUTORUTA.nextval from dual";
			Long id_registro = jdbcTemplateOracle.queryForObject(sqlSeqVtaAutoAct, Long.class);
			
			String sqlInsVtaAutoRuta = "INSERT INTO vta_auto_ruta(id_auto_ruta, id_auto_act, id_srv_venta, id_sector_lec, id_zona_lec,\r\n"
					+ "			correlativo_lec,  id_sector_rep, id_zona_rep, correlativo_rep,  id_sector_fac,\r\n"
					+ "			id_zona_fac, correlativo_fac,  id_sector_cor, id_zona_cor, correlativo_cor, id_empresa)\r\n"
					+ "		VALUES(?, ?, ?,\r\n"
					+ "			?, ?, ?,\r\n"
					+ "			?, ?, ?,\r\n"
					+ "			?, ?, ?,\r\n"
					+ "			?, ?, ?, 3)";
			
			try
			{
			jdbcTemplateOracle.update(sqlInsVtaAutoRuta, id_registro, seqVtaAutoAct, regAutActiva.getIdSrvVenta(),
					 regAutActiva.getIdSector(), regAutActiva.getIdZona(), regAutActiva.getCorrelativo(), 
					 regAutActiva.getIdSector(), regAutActiva.getIdZona(), regAutActiva.getCorrelativo(), 
					 regAutActiva.getIdSector(), regAutActiva.getIdZona(), regAutActiva.getCorrelativo(), 
					 regAutActiva.getIdSector(), regAutActiva.getIdZona(), regAutActiva.getCorrelativo()
					 );
			}
			catch(EmptyResultDataAccessException e)
			{
				log.error(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_ruta...",e,e.getMessage()));
				Log.logError2(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_ruta...",e,e.getMessage()));
				return 0L;
			}
			catch(Exception e)
			{
				log.error(String.format("Error: [%s] [%s [%s]\n", "Error: INSERT INTO vta_auto_ruta...",e,e.getMessage()));
				Log.logError2(String.format("Error: [%s] [%s] [%s]\n", "Error: INSERT INTO vta_auto_ruta...",e,e.getMessage()));
				return 0L;
			}
			
			if(regAutActiva.getCrearRuta()==1) {
				log.info("ingreso a condicion regAutActiva.getCrearRuta()==1");
				final int datCorrLength=datosCorrelativoMoverDTOs.size();
				
				String sqlSqAuditoria = "select mod_red.s_sqauditoria.nextval from dual";
				Integer sqAuditoria = jdbcTemplateOracle.queryForObject(sqlSqAuditoria, Integer.class);
				
				String sqlDatosCorrelativoMoverUpdate="UPDATE VTA_AUTO_RUTA SET" +
						"     ID_SECTOR_LEC=?,ID_ZONA_LEC=?,CORRELATIVO_LEC=?," +
						"     ID_SECTOR_REP=?,ID_ZONA_REP=?,CORRELATIVO_REP=?," +
						"     ID_SECTOR_FAC=?,ID_ZONA_FAC=?,CORRELATIVO_FAC=?," +
						"     ID_SECTOR_COR=?,ID_ZONA_COR=?,CORRELATIVO_COR=?" +
						"     WHERE ID_SRV_VENTA=?";
				String registerAuditQuery="INSERT INTO synergia.com_modificacion" +
						"     (ID_MODIFICACION,ID_EMPRESA,ID_USUARIO,APLICACION,FEC_MODIFICACION,ID_DATO,"
						+
						"     TYPE_DATO,DES_MODIFICACION,DATO_ANTERIOR,DATO_ACTUAL,OBSERVACION) VALUES" +
						"     (?,3,9998,'vta_auto_ruta',sysdate,1,'cod_ruta'," +
						"     'Cambio de rutas',?,?,'vta_auto_ruta')";

				//tabla com_modificacion de Schom creada de forma temporal hasta que nos acceso a INSERTAR EN synergia.com_modificacion@EDSCPROD.ENEL
				String registerAuditQuery2="INSERT INTO schscom.com_modificacion" +
						"     (ID_MODIFICACION,ID_EMPRESA,ID_USUARIO,APLICACION,FEC_MODIFICACION,ID_DATO,"
						+
						"     TYPE_DATO,DES_MODIFICACION,DATO_ANTERIOR,DATO_ACTUAL,OBSERVACION) VALUES" +
						"     (?,3,9998,'vta_auto_ruta',now(),1,'cod_ruta'," +
						"     'Cambio de rutas',?,?,'vta_auto_ruta')";
				
				
				log.info("inicia recorrido datCorrLength, valor de datCorrLength es " + datCorrLength);
				for (int j=0; j < datCorrLength; j++) {
					int auxIndex=j + 1;

					if(j==datCorrLength - 1) {
						auxIndex=0;
					}
					DatosCorrelativoMoverDTO corrMov=datosCorrelativoMoverDTOs.get(j);
					DatosCorrelativoMoverDTO corrMovAux=datosCorrelativoMoverDTOs.get(auxIndex);
					corrMov.setIdSector(corrMovAux.getIdSector());
					corrMov.setCorrel(corrMovAux.getCorrel());
					corrMov.setIdZona(corrMovAux.getIdZona());
					Integer numFilasAfectadas = -1;
					try {
						numFilasAfectadas = jdbcTemplateOracle.update(
								sqlDatosCorrelativoMoverUpdate,
								corrMov.getIdSector(),corrMov.getIdZona(),corrMov.getCorrel(),
								corrMov.getIdSector(),corrMov.getIdZona(),corrMov.getCorrel(),
								corrMov.getIdSector(),corrMov.getIdZona(),corrMov.getCorrel(),
								corrMov.getIdSector(),corrMov.getIdZona(),corrMov.getCorrel(),
								corrMov.getIdServVenta());
					} catch(Exception e) {
						log.error("Error al actualizar CorrMov VTA_AUTO_RUTA SET... ,/n excepcion: " + e);
						numFilasAfectadas = -1;
					}
					
					log.info("numero de filas afectadas en update sqlDatosCorrelativoMoverUpdate es "+ numFilasAfectadas);
					
					//validacion , si el update afecto a mas de 1 fila inserta registro en tabla auditoria com_modificacion
					if(numFilasAfectadas >= 1) {
						try {
							//DESCOMENTAR LINEA DE ABAJO cuando tengamos acceso a INSERTAR en synergia.com_modificacion@EDSCPROD.ENEL
//							jdbcTemplateOracleMOD.update(registerAuditQuery, sqAuditoria, corrMov.getCodRuta(),corrMovAux.getCodRuta());
							//INSERCION TEMPORAL EN SCHOM a com_modificacion
							jdbcTemplatePostgres.update(registerAuditQuery2, sqAuditoria, corrMov.getCodRuta(),corrMovAux.getCodRuta());
						} catch(Exception e) {
							log.error(
									"Error al insertar auditoria CorrMov INSERT INTO COM_MODIFICACION... ,/n excepcion: "
											+ e);
						}
					}

					datosCorrelativoMoverDTOs.add(j,corrMov);
				}
			}
			
		}
		
		return seqVtaAutoAct;
	}
	
	public void activar_orden(Long numOrden, Long numOrdConex) {
		
		VtaAutoActDTO vtaAutoAct = new VtaAutoActDTO();
		OrdVentaWkfDTO ordVentaWkf = new OrdVentaWkfDTO();
		OrdVentaWkfDTO ordVentaWkf2 = new OrdVentaWkfDTO();
		
		String sqlVtaAutoAct = VtaAutoActMapper.SQLORACLE_SELECT_FOR_VTAAUTOACT;
		String sqlContSrvVenta = ContSrvVentaMapper.SQLORACLE_SELECT_FOR_CONTSRVVENTA;
		
		String sqlOrdVentaWkf = OrdVentaWkfMapper.SQLORACLE_SELECT_FOR_ORDENVENTAWKF;
		String sqlUpdWkfWorkflow = UpdWkfWorkflowMapper.SQLORACLE_UPDATE_FOR_WKFWORKFLOW;
		String sqlOrdVentaWkf2 = OrdVentaWkfMapper.SQLORACLE_SELECT_FOR_ORDENVENTAWKF2;
		
		VtaAutoActMapper vtaAutoActMapper = new VtaAutoActMapper();
		ContSrvVentaMapper contSrvVentaMapper = new ContSrvVentaMapper();
		OrdVentaWkfMapper ordVentaWkfMapper = new OrdVentaWkfMapper();
		
		int cant=0;
//		
//		try {
//			vtaAutoAct = jdbcTemplateOracle.queryForObject(sqlVtaAutoAct, vtaAutoActMapper, numOrden);
//		} catch(EmptyResultDataAccessException e) {
//			vtaAutoAct.setIdAutoAct(0L);
//			vtaAutoAct.setNroCuenta(0L);
//		}
//		
		List<VtaAutoActDTO> listavtaAutoAct = null;
		try {
			listavtaAutoAct = jdbcTemplateOracle.query(sqlVtaAutoAct, vtaAutoActMapper, numOrden);
		} catch(EmptyResultDataAccessException e) {
			vtaAutoAct.setIdAutoAct(0L);
			vtaAutoAct.setNroCuenta(0L);
			listavtaAutoAct.add(vtaAutoAct);
		}
		
		for (VtaAutoActDTO vtaAutoActDTO : listavtaAutoAct) {
			if(cant>0)
			 {
				Log.logError2(String.format("\n%d\t%d\tActivacion Masiva\t%d\t ",numOrden, vtaAutoAct.getNroCuenta(),vtaAutoAct.getIdAutoAct()));
				log.info(String.format("\n%d\t%d\tActivacion Masiva\t%d\t ",numOrden, vtaAutoAct.getNroCuenta(),vtaAutoAct.getIdAutoAct()));
			 }
			
			String sqlFuncionServiceActivarOrden = "SELECT vta_activar_orden(?) as vta_activar_orden"
					+ " FROM dual";
			
			String resultadoFuncion = jdbcTemplateOracle.queryForObject(sqlFuncionServiceActivarOrden, String.class, vtaAutoActDTO.getIdAutoAct());
			
			Log.logError2(resultadoFuncion);
			log.info("Resultado de funcion vta_activar_orden : " + resultadoFuncion);
			
			//homologacion con tablas scom
			if(resultadoFuncion.substring(0,2).equals("OK")) {
				contOrdActivadas++;
				listaOrdVtaActiv.add(numOrden);
				listaOrdCnxActiv.add(numOrdConex);
				if(numOrden !=null) {
					Boolean homologacion = homologacionDe4JaScom(numOrden);
					log.info("la homologacion fue " + homologacion);
				} else {
					log.info("no se pudo realizar la homologacion ya que numOrden es " + numOrden);
				}
			}
			
			String resultado = resultadoFuncion.substring(0,5);
			log.info("variable resultado -> " + resultado);
			Log.logError2("Cadena Electrica: SET: "+ paut.getIdSet() +", ALIMENTADOR: "+ paut.getIdAlim()+", SED: "+ paut.getIdSed() +", LLAVE:"+ paut.getIdLlave());
			
			// NUEVO REQUERIMIENTO CAE_TMP_CLIENTE
				String estadoProceso = "";
				String estado_activacion = "KO";
				if (!resultado.equals("ERROR")) {
					estadoProceso = "PendientePcr";
					estado_activacion = "OK";
				}
				
				// metodo para registro caetmpcliente y activacion
				registrarCaeTpmCliente(estadoProceso, estado_activacion, numOrdConex, vtaAutoActDTO.getNroCuenta(),
						vtaAutoActDTO.getIdAutoAct(), numOrden,resultadoFuncion);
						
			
			if(resultado.equals("ERROR")) {
				log.error("No se pudo Activar la Orden " + resultadoFuncion);
			} else {
				log.info("Activacion OK : ->" + resultadoFuncion);
			}
			
		}
		
		Integer cont = jdbcTemplateOracle.queryForObject(sqlContSrvVenta, contSrvVentaMapper, numOrden);
		String sqlUpdSrvVenta = UpdSrvVentaMapper.SQLORACLE_UPDATE_FOR_SRVVENTA4;
		
		if(cont>0) {
			jdbcTemplateOracle.update(sqlUpdSrvVenta, numOrden);
			try {
				ordVentaWkf = jdbcTemplateOracle.queryForObject(sqlOrdVentaWkf, ordVentaWkfMapper, numOrden);
			} catch(EmptyResultDataAccessException e) {
				ordVentaWkf.setIdStateOven("");
				ordVentaWkf.setIdWkfOrdVenta(0L);
			}
			
			if(ordVentaWkf.getIdStateOven().compareTo("Finalizada")==0) {
				jdbcTemplateOracle.update(sqlUpdWkfWorkflow, "RecEjecucion", ordVentaWkf.getIdWkfOrdVenta());
			}
			
			try {
				ordVentaWkf2 = jdbcTemplateOracle.queryForObject(sqlOrdVentaWkf2, ordVentaWkfMapper, numOrden);
			} catch(EmptyResultDataAccessException e) {
				ordVentaWkf2.setIdStateOven("");
				ordVentaWkf2.setIdWkfOrdVenta(0L);
			}
			
			if(ordVentaWkf2.getIdStateOven().compareTo("Finalizada")==0) {
				jdbcTemplateOracle.update(sqlUpdWkfWorkflow, "Venta", ordVentaWkf.getIdWkfOrdVenta());
			}
		}
		
	}
	//calcular_datos_faltantes
	public int calcularDatosFaltantes(RegAutActivaDTO regautoActiva, RegistroOrdenDTO registroOrden, Long numOrdenLong) {
		log.info("Ingresa a calcularDatosFaltantes ");
		Long idDato = 0L;
		Long idRegistro = 0L;
		
		Long idnodo = 0L;
		int correlativo = 0;
		int corr_ini ;
		int corr_fin ;
		int corr_prom = 0;
		int cant = 0;
		String cod_ruta = "";
		String cod_zona = "";
		
		Long cod_oficina = 0L;
		Long id_solicitud = 0L;
				
		idDato	= regautoActiva.getIdAlim();
		
		String sqlquery1 = "SELECT nvl(id_set,0)\r\n"
				+ "            FROM srv_alimentador\r\n"
				+ "           WHERE id_alimentador = ?";
		paut.setIdAlim(idDato);
		try {
			idRegistro = jdbcTemplateOracle.queryForObject(sqlquery1, Long.class, idDato);	
		} catch (EmptyResultDataAccessException e) {
			log.error("El Alimentador[Id = "+idDato+"] no existe. ");
			return 0;
		} catch(Exception e) {
			log.error("Error [SELECT nvl(id_set,0)....] , Excepcion " + e);
			return 0;
		}
		
		regautoActiva.setIdSet(idRegistro);
		paut.setIdSet(idRegistro);
		
		 /*Calcular rutas lectura, facturacion, corte, reparto , si se indico crearla
		 1- Si hay 2 aledaños diferentes, la ruta es el promedio de ellas, siempre que este libre, caso contrario se reporta como error.
		 2- Si hay 1 aledaño, la ruta se calcula buscando en las 10 rutas superiores del aledaño, si no hay espacio, se busca en las 10 rutas inferiores del aledaño, si no hay espacio se reporta como error.
		 3- Si no hay aledaños, se busca la del servicio electrico de la venta, si no tiene, se reporta como error.
		 */

		if(regautoActiva.getCrearRuta() == 1) {
			log.info("ingreso a CrearRuta() == 1");
			idDato = regautoActiva.getIdRutaIzq();
			idRegistro = regautoActiva.getIdRutaDer();
			
			String sql2 = UbiRutaMapper.SQLORACLE_SELECT_FOR_UBIRUTA;
			
			UbiRutaDTO ubirutadto = new UbiRutaDTO();
			
			try {
				ubirutadto = jdbcTemplateOracle.queryForObject(sql2, new UbiRutaMapper() , idDato, idRegistro);
			} catch (EmptyResultDataAccessException e) {
				log.error("No existen datos para calcular la nueva ruta.");
				return 0;
			} catch(Exception e) {
				log.error("Error [SELECT id_nodo, min(to_number(substr(cod_ruta,7,4)))...]  , Excepcion " + e);
				return 0;
			}
			BuscarCorrelativoDTO correlativoDto = new BuscarCorrelativoDTO();

			String sqlCorrelativo = BuscarCorrelativoMapper.SQLPOSTGRESQL_SELECT_FOR_CORRELATIVO;
			try {
				correlativoDto = jdbcTemplatePostgres.queryForObject(sqlCorrelativo, new BuscarCorrelativoMapper());
			} catch (EmptyResultDataAccessException e) {
				log.error("Parametro de Correlativo no registrado.");
			} catch(Exception e) {
				log.error("Error [SELECT valor_num...]  , Excepcion " + e);
			}

			correlativo = correlativoDto.getValorNum();
			
			idnodo = ubirutadto.getIdNodo();
			corr_ini = ubirutadto.getCorrIni();
			corr_fin = ubirutadto.getCorrFin();					
			
		     if( corr_ini == corr_fin){
		         corr_prom = corr_ini;
		         corr_ini =	corr_ini-correlativo;//corr_ini =	corr_ini-10;
		         corr_fin =	corr_fin+correlativo;//corr_fin =	corr_fin+10;
		         if(corr_ini < 1 )
		        	 corr_ini = 1;
		         if(corr_fin > 9999 )
		        	 corr_fin = 9999;
		    } else {
		          corr_prom = (int)((corr_ini + corr_fin)/2) ;
		          corr_ini=corr_ini-5;  /* ANL_20210617 */
		          corr_fin=corr_fin+5;
		          if(corr_ini <1 )   corr_ini=1;
		          if(corr_fin >9999 )corr_fin=9999;
		     }
		     
		     ArrayList<DatosCorrelativoMoverDTO> listDerDatosCorrMov = new ArrayList<>();
		 	 ArrayList<DatosCorrelativoMoverDTO> listIzqDatosCorrMov = new ArrayList<>();
		 	
			 /*Buscar en circulo la ruta libre de lectura mas cercana*/
		     cant=1;
		     String ubica = "";
		 	 String selectCodeQuery = "SELECT id_ruta, cod_ruta FROM ubi_ruta\r\n" +
		 	 "      WHERE cod_ruta=?\r\n" +
		 	 "      AND id_tipo_ruta=1";
		 	 int found = 0;
		     for(int i=corr_prom; i<=corr_fin; i++){

		         cod_ruta = String.format("9%05d%04d", idnodo, i);
		         
		         DatosCorrelativoMoverDTO datosCorrMoverDto = new DatosCorrelativoMoverDTO();
		         
		         try {
		        		datosCorrMoverDto = jdbcTemplateOracle.queryForObject(
									        		selectCodeQuery,
									        		new DatosCorrelativoMoverMapper(),cod_ruta);

		        	} catch (EmptyResultDataAccessException e) {
		        		datosCorrMoverDto = new DatosCorrelativoMoverDTO();
		        		// si no existe creamos un nuevo objeto disponible
		        		datosCorrMoverDto.setIdRuta(0);
		        		datosCorrMoverDto.setCodRuta(cod_ruta);
		        		datosCorrMoverDto.setIdDisponible(1);
		        		ubica = "D";
		        		found = 1;
		        	} catch(Exception e) {
		        		log.error("Error [SELECT id_ruta, cod_ruta...]  , Excepcion " + e);
		        	}
		        	if (datosCorrMoverDto != null) {
		        		datosCorrMoverDto.setIdDisponible(0);
		        	}
		        	listDerDatosCorrMov.add(datosCorrMoverDto);
		        	if (found == 1) break;
		        	
		        	//sprintf(cod_ruta,"9%05d%04d",id_nodo,i);
		        	
		         
//		         String sql3 = "SELECT count(*)\r\n"
//		         		+ "		                    FROM ubi_ruta\r\n"
//		         		+ "		                   WHERE cod_ruta = ?\r\n"
//		         		+ "		                     AND id_tipo_ruta=1";
//		         
//		         cant = jdbcTemplateOracle.queryForObject(sql3, Integer.class , cod_ruta);
//		         
//				 System.out.println("Buscando ruta libre 1....");
//
//		         if(cant==0 )
//		        	 break;

		         corr_ini = (corr_prom*2) - i - 1;

		         if(corr_ini>0){
		        	// sprintf(cod_ruta,"9%05d%04d",id_nodo,corr_ini);
		        	cod_ruta = String.format("9%05d%04d", idnodo, corr_ini);

//		            String sql4 = "SELECT count(*)\r\n"
//		            		+ "		                       FROM ubi_ruta\r\n"
//		            		+ "		                      WHERE cod_ruta = ?\r\n"
//		            		+ "		                        AND id_tipo_ruta=1 ";
//
//			         cant = jdbcTemplateOracle.queryForObject(sql4, Integer.class , cod_ruta);
//		
//			        System.out.println("Buscando ruta libre 2....");
//		            if(cant==0 )
//		            	break;
		        			        	
		        	try {
		        		datosCorrMoverDto = jdbcTemplateOracle.queryForObject(
											        			selectCodeQuery,
												        		new DatosCorrelativoMoverMapper(), cod_ruta);

        			} catch (EmptyResultDataAccessException e) {
		        		datosCorrMoverDto = new DatosCorrelativoMoverDTO();
		        		// si no existe creamos un nuevo objeto disponible
		        		datosCorrMoverDto.setIdRuta(0);
		        		datosCorrMoverDto.setCodRuta(cod_ruta);
		        		datosCorrMoverDto.setIdDisponible(1);
		        		ubica = "I";
		        		found = 1;
        			} catch(Exception e) {
    					log.error("Error [SELECT id_ruta, cod_ruta...]  , Excepcion " + e);
        			}
        			if (datosCorrMoverDto != null) {
        				datosCorrMoverDto.setIdDisponible(0);
        			}
        			listIzqDatosCorrMov.add(datosCorrMoverDto);
        			if (found == 1) break;
		        	
		        	
		         }
		      }
		     
		     //if(cant>0){ /*Si no se encontro alguna ruta libre*/
		     if(ubica=="") {
		    	 log.error("No existe una ruta libre para el suministro.\t");
//		         return 0;
		      }
		     else{
		    	 log.info("ingreso a else , valor de ubica es " + ubica);
		         String sql5 = "SELECT id_nodo_padre\r\n"
		         		+ "	 FROM ubi_nodo_ruta\r\n"
		         		+ "	 WHERE id_nodo = ?";
		         
				 try {
					 idRegistro = jdbcTemplateOracle.queryForObject(sql5, Long.class, idnodo);
				 } catch (EmptyResultDataAccessException e) {
					log.error("No existen datos para SELECT id_nodo_padre...");
				 } catch(Exception e) {
					log.error("Error: SELECT id_nodo_padre...\n , Excepcion " + e);
					 return 0;
				 }
		         
		         regautoActiva.setIdSector(idRegistro);
		         regautoActiva.setIdZona(idnodo);
		         // strcpy(paut->correlativo, &cod_ruta[6]);     
		         String cod_rutaCorrelativo = cod_ruta.substring(6);
		         regautoActiva.setCorrelativo(cod_rutaCorrelativo);
		         String sql6 = "SELECT c.id_srv_venta idSrvVenta, va.id_sector_lec idSectorLec," +
		        		 "      va.id_zona_lec idZonaLec, va.correlativo_lec corrLec" +
		        		 "      FROM nuc_cuenta a, nuc_servicio b," +
		        		 "      vta_srv_venta c, vta_sol_srv_ele d," +
		        		 "      srv_electrico e, vta_auto_ruta va" +
		        		 "      WHERE a.id_cuenta=b.id_cuenta AND" +
		        		 "      b.id_servicio=d.id_srv_electrico AND" +
		        		 "      d.id_sol_srv_ele=c.id_sol_srv_ele AND" +
		        		 "      b.id_servicio=e.id_servicio AND" +
		        		 "      b.tipo='ELECTRICO' AND" +
		        		 "      e.id_ruta_lectura = ? AND" +
		        		 "      e.id_estado in (0,3,5) AND" +
		        		 "      va.id_srv_venta = c.id_srv_venta";
		         
		        		 if (ubica == "D") {
		        			log.info("ingreso en condicicion ubica D");
		        		 	int derLength = listDerDatosCorrMov.size();

		        		 	for (int i = 0; i < derLength; i++) {
		        		 		 int controlinterno1 = 0;
				        		 DatosCorrelativoMoverDTO datosCorrMoverDto = new DatosCorrelativoMoverDTO();
				        		 try {		
				        		 	Long idRuta = listDerDatosCorrMov.get(i).getIdRuta();
		
				        		 	String codRuta = listDerDatosCorrMov.get(i).getCodRuta();
				        		 	int idDisponible = listDerDatosCorrMov.get(i).getIdDisponible();
		
				        		 	datosCorrMoverDto = jdbcTemplateOracle.queryForObject(sql6,
				        		 										new ExtraDatosCorrelativoMoverMapper(), idRuta);
		
				        		 	datosCorrMoverDto.setIdRuta(idRuta);
				        		 	datosCorrMoverDto.setCodRuta(codRuta);
				        		 	datosCorrMoverDto.setIdDisponible(idDisponible);
		
				        		 } catch(Exception e) {
				        		 	log.error("SELECT c.id_srv_venta, va.id_sector_lec,...  , Excepcion " + e);
				        		 	controlinterno1 = 1;
				        		 }
				        		 
				        		 if(controlinterno1 == 0) {
				        			 listDatosCorrMov.add(datosCorrMoverDto);
				        			 log.info("[CASO1]se añadieron datos para listDatosCorrMov , objeto añadido " + datosCorrMoverDto);
				        		 }
		        		 	}
		        		 } else {
		        			 log.info("ingreso en else ubica , valor de ubica es " + ubica);
		        		 	int izqLength = listIzqDatosCorrMov.size();
		        		 	for (int i = 0; i < izqLength; i++) {
		        		 		 int controlinterno2 = 0;
				        		 DatosCorrelativoMoverDTO datosCorrMoverDto = new DatosCorrelativoMoverDTO();
		
				        		 try {
		
				        		 	Long idRuta = listIzqDatosCorrMov.get(i).getIdRuta();
		
				        		 	String codRuta = listIzqDatosCorrMov.get(i).getCodRuta();
				        		 	int idDisponible = listIzqDatosCorrMov.get(i).getIdDisponible();
		
				        		 	datosCorrMoverDto = jdbcTemplateOracle.queryForObject(sql6,
				        		 									new ExtraDatosCorrelativoMoverMapper(), idRuta);
		
				        		 	datosCorrMoverDto.setIdRuta(idRuta);
				        		 	datosCorrMoverDto.setCodRuta(codRuta);
				        		 	datosCorrMoverDto.setIdDisponible(idDisponible);
		
				        		 } catch(Exception e) {
				        		 	log.error("SELECT c.id_srv_venta, va.id_sector_lec,...  , Excepcion " + e);
				        		 	controlinterno2 = 1;
				        		 }
				        		 
				        		 if(controlinterno2 == 0) {
				        			 listDatosCorrMov.add(datosCorrMoverDto);
				        			 log.info("[CASO2]se añadieron datos para listDatosCorrMov , objeto añadido " + datosCorrMoverDto);
				        		 }
		        		 	}
		        		 }
		       }

		}
		
		/*MVC Datos de ruta fueron ingresado en archivo de entrada*/
		if(regautoActiva.getCrearRuta() == 2) {
//			sprintf(cod_zona, "%03d", preg->zona);
			cod_zona = String.format("%03d", registroOrden.getZona());
//			sector = preg->sector;
			int sector = Integer.parseInt(registroOrden.getSector());
			
			String sql1 = "SELECT id_nodo\r\n"
					+ "			 FROM ubi_nodo_ruta\r\n"
					+ "			 WHERE id_nivel =2\r\n"
					+ "			 and cod_nodo = ?\r\n"
					+ "			 and id_nodo_padre = ?";
			
			try {
				idnodo = jdbcTemplateOracle.queryForObject(sql1, Long.class, cod_zona, sector);
			} catch (EmptyResultDataAccessException e) {
				log.error("No existen datos para SELECT id_nodo...");
				return 0;
			} catch(Exception e) {
				log.error("Error [SELECT id_nodo...] , Excepcion " + e);
				return 0;
			}
			
			Long sectorLong = (long) sector;
			
			regautoActiva.setIdSector(sectorLong);
			regautoActiva.setIdZona(idnodo);
//			sprintf(paut->correlativo, "%04d" ,preg->correlativo);
			regautoActiva.setCorrelativo(String.format("%04d", registroOrden.getCorrelativo()));
		}
		
		/*	2372	Obtener centro de servicio de la solicitud*/
		Long num_registro = numOrdenLong;
		
		String sqlCentroServSoli = CentroServicioSoliMapper.SQLORACLE_SELECT_FOR_CENTROSERVICIOSOLI;

		CentroServicioSoliDTO centroServicioSoli = new CentroServicioSoliDTO();
		
		try {
			centroServicioSoli = jdbcTemplateOracle.queryForObject(sqlCentroServSoli, new CentroServicioSoliMapper() , num_registro);			
		} catch (EmptyResultDataAccessException e) {
			log.error("La solicitud no existe.");
			return 0;
		} catch(Exception e) {
			log.error("Error [SELECT nvl(ofi.cod_oficina,-1), sol.id_solicitud...] , Excepcion " + e);
			return 0;
		}
		
		/*Si es centro de servicio expansion cambiar automoticamente los pptos a estado Pagado*/
		
		cod_oficina = centroServicioSoli.getCodOficina();
		id_solicitud = centroServicioSoli.getIdSolicitud();
		
		if(cod_oficina == 5300) {
			String sqlUpdStateWkfWorkflow = "UPDATE wkf_workflow SET id_old_state= id_state, id_state = 'Pagado'\r\n"
					+ "		WHERE id_workflow in\r\n"
					+ "		(SELECT id_workflow FROM vta_conf_pres_vta WHERE id_solicitud = ? )\r\n"
					+ "		AND id_state in('Cotizado','Emitido')";
			
			try {
				jdbcTemplateOracle.update(sqlUpdStateWkfWorkflow, id_solicitud);			
			} catch(Exception e) {
				log.error("Error [UPDATE wkf_workflow...] , Excepcion " + e);
				return 0;
			}
		}
		
		regautoActiva.setCodCentroOperativo(cod_oficina);
		
		return 1;
		
	}
	
	public int verificarRutaLibre(String pruta) {
		
		String ruta = pruta;
		Long lCount = 0L;
		
		/* Primer Bloque */
		String sql1 = "SELECT count(*) \r\n"
				+ "	 FROM vta_sol_srv_ele sele, srv_electrico se, ubi_ruta ur\r\n"
				+ "	 WHERE\r\n"
				+ "	 ur.cod_ruta = ?\r\n"
				+ "	 and sele.id_srv_electrico = se.id_servicio\r\n"
				+ "	 and ? = ur.id_ruta\r\n"
				+ "	 and se.ID_ESTADO not in(1,6)";
		
		try {
			lCount = jdbcTemplateOracle.queryForObject(sql1, Long.class, ruta, "sele.ID_RUTA_CORTE");
		} catch (Exception e) {
			log.error("Error VerificarRutaLibre() , Excepcion " + e);
		}
		
		if(lCount>0)
			return 0;

		try {
			lCount = jdbcTemplateOracle.queryForObject(sql1, Long.class, ruta, "sele.ID_RUTA_lectura");
		} catch (Exception e) {
			log.error("Error VerificarRutaLibre() , Excepcion " + e);
		}
		
		if(lCount>0)
			return 0;

		try {
			lCount = jdbcTemplateOracle.queryForObject(sql1, Long.class, ruta, "sele.ID_RUTA_FACTURACION");
		} catch (Exception e) {
			log.error("Error VerificarRutaLibre() , Excepcion " + e);
		}
		
		if(lCount>0)
			return 0;
		
		try {
			lCount = jdbcTemplateOracle.queryForObject(sql1, Long.class, ruta, "sele.ID_RUTA_reparto");
		} catch (Exception e) {
			log.error("Error VerificarRutaLibre() , Excepcion " + e);
		}
		
		if(lCount>0)
			return 0;
		
		/* Segundo bloque */
		
		String sql2 = "Select count(*) \r\n"
				+ "	 From srv_electrico se, ubi_ruta ur\r\n"
				+ "	 Where\r\n"
				+ "	 ur.cod_ruta = ?\r\n"
				+ "	 and ? = ur.id_ruta\r\n"
				+ "	 and se.ID_ESTADO not in(1,6)";
		
		try {
			lCount = jdbcTemplateOracle.queryForObject(sql2, Long.class, ruta, "se.id_ruta_lectura");
		} catch (Exception e) {
			log.error("Error VerificarRutaLibre() , Excepcion " + e);
		}
		
		if(lCount>0)
			return 0;
		
		try {
			lCount = jdbcTemplateOracle.queryForObject(sql2, Long.class, ruta, "se.id_ruta_corte");
		} catch (Exception e) {
			log.error("Error VerificarRutaLibre() , Excepcion " + e);
		}

		if(lCount>0)
			return 0;
		
		/* Tercer bloque */
		
		String sql3 = "SQL Select count(*) \r\n"
				+ "	 From nuc_servicio ns, srv_electrico se, rep_config_rep rep, ubi_ruta ur\r\n"
				+ "	 Where\r\n"
				+ "	 se.id_servicio = ns.id_servicio\r\n"
				+ "	 and ns.tipo = 'ELECTRICO'\r\n"
				+ "	 and ns.id_servicio = rep.id_servicio\r\n"
				+ "	 and rep.id_ruta = ur.id_ruta\r\n"
				+ "	 and ur.cod_ruta = ?\r\n"
				+ "	 and se.ID_ESTADO not in(1,6)";

		try {
			lCount = jdbcTemplateOracle.queryForObject(sql3, Long.class, ruta);
		} catch (Exception e) {
			log.error("Error VerificarRutaLibre() , Excepcion " + e);
		}
		
		if(lCount>0)
			return 0;
		
		String sql4 = "Select count(*) \r\n"
				+ "	 From nuc_servicio ns, srv_electrico se, fac_config_doc cd, ubi_ruta ur\r\n"
				+ "	 Where\r\n"
				+ "	 ur.cod_ruta = ?\r\n"
				+ "	 and se.id_servicio = ns.id_servicio\r\n"
				+ "	 and ns.tipo = 'ELECTRICO'\r\n"
				+ "	 and ns.id_cuenta = cd.id_cuenta\r\n"
				+ "	 and ur.id_ruta = cd.ID_RUTA_FACT\r\n"
				+ "	 and se.ID_ESTADO not in(1,6)";
		
		try {
			lCount = jdbcTemplateOracle.queryForObject(sql4, Long.class, ruta);
		} catch (Exception e) {
			log.error("Error VerificarRutaLibre() , Excepcion " + e);
		}
		
		if(lCount>0)
			return 0;
		
		return 1;
	}
	private void SetErrGen(String pErrorNCat)
	{
		codOperacion =parametrosDto.getCodErrSY099();// vCodErrASY099;
		observacion =parametrosDto.getDesErrSY099() + " - "+ pErrorNCat;
	}
	
	private boolean GetTarifa(RegistroOrdenDTO registroOrden, Long numCuentaLong)
	{
		String sqlTarifa = TarifaMapper.SQLORACLE_SELECT_FOR_TARIFA;
		TarifaMapper tarifaMapper = new TarifaMapper();
		
		
		try {
			codTarifa = jdbcTemplateOracle.queryForObject(sqlTarifa, tarifaMapper, numCuentaLong);
		} catch(EmptyResultDataAccessException e) {
			log.error("Error: SELECT ftb.cod_interno INTO :cod_tarifa\n");
			return false;
		}
		
		log.info(String.format("cod_tarifa: %s\n", codTarifa));
		return true;
	}
	
	private boolean GetCentroOperativo(RegistroOrdenDTO registroOrden, Long numOrdenLong)
	{
		String sqlCentroOperativo = CentroOperativoMapper.SQLORACLE_SELECT_FOR_CENTROOPERATIVO_2;
		CentroOperativoMapper centroOperativoMapper = new CentroOperativoMapper();
		
		try {//GetCentroOperativo
			centroOperativo = jdbcTemplateOracle.queryForObject(sqlCentroOperativo, centroOperativoMapper, numOrdenLong+"");
			lCentroOperativo = Long.parseLong(centroOperativo);
			
		} catch(EmptyResultDataAccessException e) {
			log.error("No se pudo obtener Codigo de Centro Operativo. ");
			return false;
		}
		return true;
	}
	
	private int validacion_datos(RegistroOrdenDTO registroOrden,RegAutActivaDTO regAutActiva, Long numOrdenLong, Long numCuentaLong)
	{
		RemediacionVtaDTO remediacionVta = new RemediacionVtaDTO();
		
		ValidarSolicitudMapper validarSolicitudMapper = new ValidarSolicitudMapper();
		UpdVtaAutoDtMapper updVtaAutoDtMapper = new UpdVtaAutoDtMapper();
		RemediacionVtaMapper remediacionVtaMapper = new RemediacionVtaMapper();
		FechaHoraMapper fechaHoraMapper = new FechaHoraMapper();		
		SuministroMapper suministroMapper = new SuministroMapper(); 
		
		ServicioElectricoOrdenMapper servicioElectricoOrdenMapper = new ServicioElectricoOrdenMapper();
		
		SrvPcrMapper srvPcrMapper = new SrvPcrMapper();
		
		ValidarSolicitudDTO validarSolicitud = new ValidarSolicitudDTO(); 
		ValidarSolicitudDTO validarSolicitud2 = new ValidarSolicitudDTO();
		
		
		String sqlValidarSolicitud = ValidarSolicitudMapper.SQLORACLE_SELECT_FOR_VALIDARSOLICITUD;
		String sqlValidarSolicitud2 = ValidarSolicitudMapper.SQLORACLE_SELECT_FOR_VALIDARSOLICITUD2;
		String sqlUpdOrdenVenta = UpdOrdenVentaMapper.SQLORACLE_UPDATE_FOR_ORDENVENTA2;		
		String sqlInsAuditoriaVenta = InsAuditoriaVentaMapper.SQLORACLE_INSERT_FOR_AUDITORIAVENTA2;
		String sqlServicioElectricoOrden = ServicioElectricoOrdenMapper.SQLORACLE_SELECT_FOR_SRVELECTRICOORDEN;
		String sqlCountVtaAutoDt =UpdVtaAutoDtMapper.SQLORACLE_SELECT_COUNT_VTAAUTODT;
		String sqlRemediacionVta = RemediacionVtaMapper.SQLORACLE_SELECT_FOR_REMEDIACIONVTA;
		String sqlSrvElectrico = SrvElectricoMapper.SQLORACLE_SELECT_FOR_IDSRVELECTRICO;
		String sqlSrvElectrico2 = SrvElectricoMapper.SQLORACLE_SELECT_FOR_IDSRVELECTRICO2;
		String sqlSrvPcrMapper = SrvPcrMapper.SQLORACLE_SELECT_FOR_IDTENSION;
		String sqlSelectFechaDif=FechaHoraMapper.SQLORACLE_SELECT_FOR_FECHA_DIF;
		String sqlSelectIdPtoEntrega = SrvPtoEntregaMapper.SQLORACLE_SELECT_FOR_IDPTOENTREGA;
		String sqlSuministro = SuministroMapper.SQLORACLE_SELECT_FOR_SUMINISTRO;
		String sqlCountUbiRuta = UbiRutaMapper.SQLORACLE_SELECT_FOR_COUNT_UBIRUTA;
		String sqlUpdSrvVenta2 = UpdSrvVentaMapper.SQLORACLE_UPDATE_FOR_SRVVENTA2;
		
		VtaAutoDtDTO vtaAutoDtDTO = new VtaAutoDtDTO();
		ServicioElectricoOrdenDTO servicioElectricoOrden = new ServicioElectricoOrdenDTO();	
		SrvVentaDTO srvVenta = new SrvVentaDTO();
		SuministroDTO suministroD = new SuministroDTO();
		
		Long idRegistro=0L;
		String codigo="";
		Long idTension;
		
		cant_error=0;
		
		if(numOrdenLong<=0)
		{
			Log.logError2("Registro sin numero de Orden de Venta. ");
			vtmpError="Registro sin numero de Orden de Venta. ";
			cant_error ++;
		}
		if(numCuentaLong<=0)
		{
			Log.logError2("Registro sin numero Cuenta de Facturacion. ");
			vtmpError="Registro sin numero Cuenta de Facturacion. ";
			cant_error ++;
		}
		
		if(esBT6 != 1) {
			
			Integer cantMedidor = 0;
			Integer cont=0;
			if(registroOrden.getNumMedidorInstall() == null)
				registroOrden.setNumMedidorInstall("0");
			if(registroOrden.getNumMedidorRet() == null)
				registroOrden.setNumMedidorRet("0");
			Long numMedidorInstall = Long.parseLong(registroOrden.getNumMedidorInstall());
			Long numMedidorRet = Long.parseLong(registroOrden.getNumMedidorRet());
			
			
			if(numMedidorInstall > 0) {
				cantMedidor = 1;
			}
			if(numMedidorRet > 0) {
				cantMedidor = 2;
			}
			
			if(Integer.parseInt(registroOrden.getNumMedidorInstall())>0 && registroOrden.getNovedadInstall() != "I")
			{
				cant_error ++;
				Log.logError2("El primer medidor debe tener novedad [I]nstalado. ");
				vtmpError="El primer medidor debe tener novedad [I]nstalado. ";
			}
			
			if(registroOrden.getFecLecturaInstall()==null)
			{
				registroOrden.setFecLecturaInstall("");
			}
			
			if(Integer.parseInt(registroOrden.getNumMedidorInstall())>0 && registroOrden.getFecLecturaInstall().length() != 10)
			{
				cant_error ++;
				Log.logError2("La fecha de lectura del Medidor Instalado no es correcta. ");
				vtmpError="La fecha de lectura del Medidor Instalado no es correcta. ";
			}
			
			if(cantMedidor==2)
			{
				if(Integer.parseInt(registroOrden.getNumMedidorInstall())==0 && Integer.parseInt(registroOrden.getNumMedidorRet())>0)
				{
					cant_error ++;
					Log.logError2("Falta el medidor nuevo[I]nstalado. ");
					vtmpError="Falta el medidor nuevo[I]nstalado. ";
				}
				if(Integer.parseInt(registroOrden.getNumMedidorRet())>0 && registroOrden.getNovedadRet() != "R")
				{
					cant_error ++;
					Log.logError2("El segundo medidor debe tener novedad [R]etirado. ");
					vtmpError="El segundo medidor debe tener novedad [R]etirado. ";
				}				
			
				if(registroOrden.getFecLecturaRet()==null)
				{
					registroOrden.setFecLecturaRet("");
				}
				
				if(Integer.parseInt(registroOrden.getNumMedidorRet())>0 && registroOrden.getFecLecturaRet().length() != 10)
				{
					cant_error ++;
					Log.logError2("La fecha de lectura del Medidor Retirado no es correcta. ");
					vtmpError="La fecha de lectura del Medidor Retirado no es correcta. ";
				}
			}
		}
		
		if(registroOrden.getTensionPcr()==null)
		{
			registroOrden.setTensionPcr("");
		}
		
		if(registroOrden.getNumPcr()=="0" && registroOrden.getTensionPcr().length()==0 && registroOrden.getSumPcrRef()=="0")
		{
			cant_error ++;
			Log.logError2("No ha especificado datos para calcular el PCR. ");
			vtmpError="No ha especificado datos para calcular el PCR. ";
		}
			
		if(registroOrden.getNumPcr().compareTo("0")==0 && registroOrden.getSumPcrRef().compareTo("0") == 0) {//Int
//				 paut->id_pcr=0;
//			     paut->id_pcr_tension=0;
		     regAutActiva.setIdPcr(0L);
		     regAutActiva.setIdPcrTension(0L);
		     paut.setIdPcr(0L);
		     paut.setIdPcrTension(0L);
		     
			switch(registroOrden.getTensionPcr().substring(0,1)) {
				case "B": regAutActiva.setIdPcrTension(1129L);paut.setIdPcrTension(1129L); break;
				case "M": regAutActiva.setIdPcrTension(2229L);paut.setIdPcrTension(2229L); break;
				case "A": regAutActiva.setIdPcrTension(1029L);paut.setIdPcrTension(1029L); break;
			} 
			if(regAutActiva.getIdPcrTension()==0)
			{
				Log.logError2("La tension del PCR no es valida. ");
				vtmpError="La tension del PCR no es valida. ";
				cant_error++;
			}
		}
			
		if( cant_error>0)  
		{
			return 0;				
		}
			
			/*validar existencia de datos del archivo de entrada*/
			
			cant_error=0;
			
			/*Validar Solicitud, Orden y Cuenta Facturacion*/
			
			try {
				validarSolicitud = jdbcTemplateOracle.queryForObject(sqlValidarSolicitud, validarSolicitudMapper, numOrdenLong);
				codigo=validarSolicitud.getCodigo();
			} catch(EmptyResultDataAccessException e) {
				validarSolicitud.setCodigo("");
				validarSolicitud.setFechaIngreso("");
				validarSolicitud.setIdRegistro(0L);
								
				try {
					validarSolicitud = jdbcTemplateOracle.queryForObject(sqlValidarSolicitud2, validarSolicitudMapper, numOrdenLong);					
				} catch(EmptyResultDataAccessException er) {
					validarSolicitud.setCodigo("");
					validarSolicitud.setFechaIngreso("");
					validarSolicitud.setIdRegistro(0L);
					
					Log.logError2("La orden no existe o esta Activa. ");
					vtmpError="La orden no existe o esta Activa. ";
					
					log.error("Error: Buscando Orden Paralizada...\n");
					return 0;
				}
				codigo=validarSolicitud.getCodigo();
				try
				{
				jdbcTemplateOracle.update(sqlUpdOrdenVenta, validarSolicitud.getIdRegistro());
				}
				catch(Exception ex)
				{
					log.error("Error: UPDATE VTA_ORD_VTA SET  PARALIZADA = 'N'...\n");
					Log.logError2("Error: UPDATE VTA_ORD_VTA SET  PARALIZADA = 'N'...\n");
					return 0;
				}
				
				try
				{
					jdbcTemplateOracle.update(sqlInsAuditoriaVenta, validarSolicitud.getIdRegistro(), validarSolicitud.getIdRegistro());
				}catch(Exception ex)
				{
					log.error("Error: INSERT INTO VTA_AUD_ORD...");
					Log.logError2("Error: INSERT INTO VTA_AUD_ORD...");
					return 0;
				}
			}
			
			/* 
		
			
			if(validarSolicitud.getIdRegistro() != 0) {
				idRegistro = validarSolicitud.getIdRegistro();
				propEmpalme = validarSolicitud.getCodigo();
			} else {
				idRegistro = validarSolicitud2.getIdRegistro();
				propEmpalme = validarSolicitud2.getCodigo();
			}
			*/
			validarSolicitud.setCodigo(validarSolicitud.getCodigo().trim());
			long num_registro=numCuentaLong;
			/*Validar Solicitud, Orden y Cuenta Facturacion*/
			idRegistro=validarSolicitud.getIdRegistro();
			try {
				log.info("QUERY PARA servicioElectricoOrden: "+sqlServicioElectricoOrden + " DATOS " + idRegistro + " , "+ numCuentaLong);
				servicioElectricoOrden = jdbcTemplateOracle.queryForObject(sqlServicioElectricoOrden, servicioElectricoOrdenMapper, 
					idRegistro, num_registro);
				log.info("servicioElectricoOrden EN TRY: " + servicioElectricoOrden);
			} catch(EmptyResultDataAccessException ess) {
				servicioElectricoOrden.setCodSucEjecutora("");
				servicioElectricoOrden.setIdAlim(0L);
				servicioElectricoOrden.setIdConfPres(0L);
				servicioElectricoOrden.setIdDato(0L);
				servicioElectricoOrden.setIdDir(0L);
				servicioElectricoOrden.setIdEstSrvElectrico(0L);
				servicioElectricoOrden.setIdLlave(0L);
				servicioElectricoOrden.setIdOrdConex(0L);
				servicioElectricoOrden.setIdPcr(0L);
				servicioElectricoOrden.setIdRutaSumin(0L);
				servicioElectricoOrden.setIdSed(0L);
				servicioElectricoOrden.setIdSolSrvEle(0L);
				servicioElectricoOrden.setIdSrvElectrico(0L);
				servicioElectricoOrden.setNroOrdConex("");
				
				log.error("servicioElectricoOrden EN CATCH: " + servicioElectricoOrden +" EXCEPCION" + ess);
				log.error("Error: Buscando Servicio Electrico de Orden...\n");
				log.error(String.format("La Cuenta [%d] no pertenece a la Orden [%d] o no tiene Suministros por activar (revisar estados). ",numCuentaLong,numOrdenLong));
				Log.logError2(String.format("La Cuenta [%d] no pertenece a la Orden [%d] o no tiene Suministros por activar (revisar estados). ", numCuentaLong,numOrdenLong));				
				vtmpError=String.format("La Cuenta [%d] no pertenece a la Orden [%d] o no tiene Suministros por activar (revisar estados). ", numCuentaLong,numOrdenLong);
				return 0;
			}
			//paut->regAutActiva
			regAutActiva.setIdSrvElectrico(servicioElectricoOrden.getIdSrvElectrico());
			regAutActiva.setIdDirSumin(servicioElectricoOrden.getIdDir());
			regAutActiva.setIdSrvVenta(servicioElectricoOrden.getIdDato());
			regAutActiva.setIdSolSrvEle(servicioElectricoOrden.getIdSolSrvEle());
			regAutActiva.setIdAlim(servicioElectricoOrden.getIdAlim());
			regAutActiva.setIdSed(servicioElectricoOrden.getIdSed());
			regAutActiva.setIdLlave(servicioElectricoOrden.getIdLlave());
			regAutActiva.setIdOrdConex(servicioElectricoOrden.getIdOrdConex());
			regAutActiva.setNroOrdConex(servicioElectricoOrden.getNroOrdConex());
			regAutActiva.setCodSucEjecutora(servicioElectricoOrden.getCodSucEjecutora());
			long id_est_srv_electrico=servicioElectricoOrden.getIdSrvElectrico();
			paut.setIdSrvElectrico(servicioElectricoOrden.getIdSrvElectrico());
			paut.setIdDirSumin(servicioElectricoOrden.getIdDir());
			paut.setIdSrvVenta(servicioElectricoOrden.getIdDato());
			paut.setIdSolSrvEle(servicioElectricoOrden.getIdSolSrvEle());
			paut.setIdAlim(servicioElectricoOrden.getIdAlim());
			paut.setIdSed(servicioElectricoOrden.getIdSed());
			paut.setIdLlave(servicioElectricoOrden.getIdLlave());
			paut.setIdOrdConex(servicioElectricoOrden.getIdOrdConex());
			paut.setNroOrdConex(servicioElectricoOrden.getNroOrdConex());
			paut.setCodSucEjecutora(servicioElectricoOrden.getCodSucEjecutora());
			
			
			long vtiene_ruta = servicioElectricoOrden.getIdRutaSumin();
			
			if(servicioElectricoOrden.getIdRutaSumin()==0){ /*si el suministro no tiene ruta crearla*/
				regAutActiva.setCrearRuta(1L);
				paut.setCrearRuta(1L);
				// printf("\nMarca suministro Nro. ( [%d] ) para generar ruta de lectura.", preg->num_cuenta);   /* MA_11122019 */
			 }
			
			try {
				vtaAutoDtDTO = jdbcTemplateOracle.queryForObject(sqlCountVtaAutoDt, updVtaAutoDtMapper, regAutActiva.getIdSrvVenta());
				if(vtaAutoDtDTO.getCont()>0)
				{					
					vtmpError="Este suministro ya se proceso en otro archivo. ";
					return 0;
				}
			} catch(EmptyResultDataAccessException ess) {
				log.error("vtaAutoDt EN CATCH: " + vtaAutoDtDTO +" EXCEPCION" + ess);
				return 0;
			}
			
			/*Long tieneRuta = servicioElectricoOrden.getIdRutaSumin(); 

			if(servicioElectricoOrden.getIdRutaSumin()==0) {
				regAutActiva.setCrearRuta(1L);
			}*/
			
			try {
				remediacionVta = jdbcTemplateOracle.queryForObject(sqlRemediacionVta, remediacionVtaMapper, idRegistro, servicioElectricoOrden.getIdDato(),
					idRegistro, servicioElectricoOrden.getIdDato(), idRegistro, servicioElectricoOrden.getIdDato(), idRegistro);
			} catch(EmptyResultDataAccessException e) {
				remediacionVta.setTieneContrato("");
				remediacionVta.setTieneDirRepetidas("");
				remediacionVta.setTieneDirRepetidasIn("");
				
				log.error("Error: PREPARE remediacionVta...\n");
				Log.logError2("Error: PREPARE remediacionVta...\n");
				return 0;
			}
			
			if(remediacionVta.getTieneContrato()=="NO")
			{
				Log.logError2(String.format( "Error: Falta emitir contrato” para el servicio de la cuenta  [NRO_CUENTA: %d]",numCuentaLong));
				vtmpError=String.format( "Error: Falta emitir contrato” para el servicio de la cuenta  [NRO_CUENTA: %d]",numCuentaLong);
				//System.out.println(String.format( "Error: Falta emitir contrato” para el servicio de la cuenta  [NRO_CUENTA: %d]",numCuentaLong));
				return 0;
			}
			
			if(remediacionVta.getTieneDirRepetidas()=="SI")
			{
				Log.logError2(String.format( "Error: Dirección del servicio de la cuenta [NRO_CUENTA: %d] ya está asignada a otro servicio",numCuentaLong));
				vtmpError=String.format( "Error: Dirección del servicio de la cuenta [NRO_CUENTA: %d] ya está asignada a otro servicio",numCuentaLong);
				//System.out.println(String.format( "Error: Dirección del servicio de la cuenta [NRO_CUENTA: %d] ya está asignada a otro servicio",numCuentaLong));
				return 0;
			}
			
			if(remediacionVta.getTieneDirRepetidasIn()=="SI")
			{
				Log.logError2(String.format( "Error: Dirección del servicio de la cuenta [NRO_CUENTA: %d] ya está asignada a otro servicio",numCuentaLong));
				vtmpError=String.format( "Error: Dirección del servicio de la cuenta [NRO_CUENTA: %d] ya está asignada a otro servicio",numCuentaLong);
				//System.out.println(String.format( "Error: Dirección del servicio de la cuenta [NRO_CUENTA: %d] ya está asignada a otro servicio",numCuentaLong));
				return 0;
			}
			
			  // AGF 20170221 RemediacionVta >>>>>>>>>>>>>>

			 /*Verificar los estados de la orden*/
			/* MBP se incluyo ||  strcmp(codigo,"Ejecucion" )==0 */

			//Transicionar estado de orden de SinEjecucion a Por ejecutar - Aplica solo para origen EORDER
			
			if(origen.equals("EORDER") && codigo.compareTo("SinEjecucion")==0)
			{
				String resul="";
				//EXEC SQL CALL Preco.PrecoActualizaEstadoVenta( :num_orden_vta, 'PorEjecutar', 'CoordGCyC',1,:resul);
				try
				{
				jdbcTemplateOracle.update("call PRECO.PrecoActualizaEstadoVenta (?, ?, ?, ?, ?)", numOrdenLong, "PorEjecutar", "CoordGCyC", 1, resul);
				}
				catch(Exception ex)
				{
					Log.logError2("No se pudo transicionar la orden de Sin Ejecucion a Por ejecutar.");
					vtmpError="No se pudo transicionar la orden de Sin Ejecucion a Por ejecutar. ";
					return 0;
				}
				
				resul=resul.trim();
				
				if(!resul.equals("OK"))
				{
					Log.logError2("No se pudo transicionar la orden de Sin Ejecucion a Por ejecutar.");
					vtmpError="No se pudo transicionar la orden de Sin Ejecucion a Por ejecutar. ";
					return 0;
				}
				else
					codigo="PorEjecutar" ;
			}
			
			if(codigo.compareTo("PorEjecutar")==0 || codigo.compareTo("Ejecucion")==0) {
				if(registroOrden.getFecLecturaInstall()==null)
				{
					registroOrden.setFecLecturaInstall("");
				}
				if(registroOrden.getFechaEjecucion()==null)
				{
					registroOrden.setFechaEjecucion("");
				}
				if(registroOrden.getFecLecturaInstall().length() != 10 && registroOrden.getFechaEjecucion().length() != 10)
				{
					Log.logError2("No hay fecha PUSER para el 'Informe de Ejecucion'. ");					
					vtmpError="No hay fecha PUSER para el Informe de Ejecucion. ";
					return 0;
				}
				
				fecha_lect=registroOrden.getFecLecturaInstall();
				
				if(registroOrden.getFecLecturaInstall()==null)
				{
					registroOrden.setFecLecturaInstall("");
				}
				
				if (origen=="EORDER" && registroOrden.getFecLecturaInstall().length() != 10 )
				{
					//Para casos en que no hay cambio de medidor
					fecha_lect=registroOrden.getFechaEjecucion();
				}
				FechaDTO oFecha= new FechaDTO();
				oFecha=jdbcTemplateOracle.queryForObject(sqlSelectFechaDif, fechaHoraMapper,fecha_lect,validarSolicitud.getFechaIngreso(),fecha_lect);
				
				if(oFecha.getDif1()==null)
				{
					oFecha.setDif1("0");
				}
				if(Integer.parseInt(oFecha.getDif1())>0)
				{
					Log.logError2("La fecha de Lectura debe ser menor o igual a la fecha actual. ");					
					vtmpError="La fecha de Lectura debe ser menor o igual a la fecha actual. ";
					return 0;
				}
				
				if(Integer.parseInt(oFecha.getDif2())>0)
				{
					Log.logError2("La fecha de Lectura debe ser mayor o igual a la fecha de ingreso de la solicitud. ");					
					vtmpError="La fecha de Lectura debe ser mayor o igual a la fecha de ingreso de la solicitud. ";
					return 0;
				}
				pautActivar=informar_ejecucion( registroOrden, regAutActiva,id_est_srv_electrico, numOrdenLong);
				
				switch(pautActivar)
				{
				case -1:
					Log.logError("No se pudo Cambiar de Estado a la orden. ");
					break;
				case 0:
					/*Se informo ejecucion, pero sin cambio de estado:No se puede activar*/
				case 1:
					codigo="RecEjecucion";
					break;/*Se informo y cambio de estado : Se puede Activar*/
				default:
					pautActivar=-1;						
				}
				if(pautActivar==-1)
				{
					return 0;
				}
			}
			else
			{
				if(codigo.compareTo("RecEjecucion")==0)  /* || strcmp(codigo,"Ejecucion" )!=0)*/
				{
					Log.logError2(String.format("La orden de venta [%d] esta en estado '%s'. ", numOrdenLong, codigo));
					vtmpError=String.format("La orden de venta [%d] esta en estado '%s'. ", numOrdenLong, codigo);
					//strcpy(vtmpError,"Este suministro ya se proceso en otro archivo. ");
					return 0;
				}
				else
				{
					pautActivar=1;
					/*----------------------------MBP  2----------------------------------*/
					try
					{
						jdbcTemplateOracle.update(sqlUpdSrvVenta2, idRegistro, servicioElectricoOrden.getIdConfPres());
					}
					catch(Exception ex)
					{
						log.error("Error: UPDATE vta_srv_venta ...\n");
						Log.logError2("Error: UPDATE vta_srv_venta ...\n");
						return 0;
					}
					/*-----------------------------------------------------------*/
				}
			}
			/*si ya tiene PCR completar los dato*/
			
			if(servicioElectricoOrden.getIdPcr() > 0) {
				try {
					idTension = jdbcTemplateOracle.queryForObject(sqlSrvPcrMapper, srvPcrMapper, servicioElectricoOrden.getIdPcr());
					//do_error("Buscando Tension de PCR existente....");  // TFRC 20210422 - no usar do_error
				} catch(EmptyResultDataAccessException e) {
					idTension = 0L;
				}
				
				regAutActiva.setIdPcr(servicioElectricoOrden.getIdPcr());
				regAutActiva.setIdPcrTension(idTension);
				paut.setIdPcr(servicioElectricoOrden.getIdPcr());
				paut.setIdPcrTension(idTension);
			}
			/*validar Datos Tecnicos */
			try
			{
			SrvPtoEntregaDTO oSrvPtoEntrega = new SrvPtoEntregaDTO();
			SrvPtoEntregaMapper srvPtoEntregaMapper= new SrvPtoEntregaMapper();
			oSrvPtoEntrega=jdbcTemplateOracle.queryForObject(sqlSelectIdPtoEntrega,srvPtoEntregaMapper,registroOrden.getPtoEntrega());
			paut.setIdPtoEntrega(Long.parseLong(oSrvPtoEntrega.getIdPtoEntrega()));
			}
			catch(Exception ex)
			{
				Log.logError2("El [Punto de Entrega] no es valido. ");
				vtmpError="El [Punto de Entrega] no es valido. ";
				cant_error++;
			}
			
			 if(valida_DTC(registroOrden.getPropEmpalme(),"PROP_EMPALME")==0)
			 {
				 Log.logError2("la [Propiedad del Empalme] no es valida. ");
				 vtmpError="la [Propiedad del Empalme] no es valida. ";
				 cant_error++;
			 }
			 paut.setProp_empalme(pcod);
			 registroOrden.setPropEmpalme(pcod);
			
			 if(valida_DTC(registroOrden.getTipoConductor(),"TIPO_CONDUCTOR")==0)
			 {
				 Log.logError2("El [Tipo de Conductor] no es valido. ");
				 vtmpError="El [Tipo de Conductor] no es valido. ";
				 cant_error++;
			 }
			 registroOrden.setTipoConductor(pcod);
			 paut.setTipoConductor(pcod);
			 
			 if(valida_DTC(registroOrden.getCapInterruptor(),"CAP_INTERRUP")==0)
			 {
				 Log.logError2("La [Cap. de Interruptor] no es valida. ");
				 vtmpError="La [Cap. de Interruptor] no es valida. ";
				 cant_error++;
			 }
			 registroOrden.setCapInterruptor(pcod);
			 paut.setCapInterruptor(pcod);
			 
			 if(valida_DTC(registroOrden.getSisProteccion(),"SIS_PROTECCION")==0)
			 {
				 Log.logError2("El [Sistema de Proteccion] no es valido. ");
				 vtmpError="El [Sistema de Proteccion] no es valido. ";
				 cant_error++;
			 }
			 registroOrden.setSisProteccion(pcod);
			 paut.setSisProteccion(pcod);
			 
			 if(valida_DTC(registroOrden.getCajaMedicion(),"CAJA_MEDICION")==0)
			 {
				 Log.logError2("La [Caja Medicion] no es valida. ");
				 vtmpError="La [Caja Medicion] no es valida. ";
				 cant_error++;
			 }
			 registroOrden.setCajaMedicion(pcod);
			 paut.setCajaMedicion(pcod);
			 
			 if(registroOrden.getCajaToma() != " ")
			 {
				 if(valida_DTC(registroOrden.getCajaToma(),"CAJA_TOMA")==0)
				 {
					 Log.logError2("La [Caja Toma] no es valida. ");
					 vtmpError="La [Caja Toma] no es valida. ";
					 cant_error++;
				 }
			 }
			 registroOrden.setCajaToma(pcod);
			 paut.setCajaToma(pcod);
			 
			 /*Validar Suiministros Aledaños,rutas, PCR y Sum_PCR_Ref*/ /*MVC Se validara aquí rutas y cadena electrica, sólo si en
			  arch entrada falta algun dato de lo contrario se tomaran los
			  datos ingresados*/
			 
			 if(registroOrden.getSet().compareTo("")==0 || registroOrden.getAlimentador().compareTo("")==0 || registroOrden.getSed().compareTo("")==0 
						|| registroOrden.getSector().compareTo("")==0 || registroOrden.getZona().compareTo("")==0 || registroOrden.getCorrelativo().compareTo("")==0)
			 {
				
				 log.info("\nSuiministros Aledaños,rutas, PCR y Sum_PCR_Ref ...");
					Long numRegistro = Long.parseLong(registroOrden.getSumIzquierdo());

					//incializacion de atributos para evitar caida en en null
					//regAutActiva.setIdRutaIzq(0L);
					//regAutActiva.setIdRutaDer(0L);
					
					Long id_ruta_sumin=0L;
					if(numRegistro >0){
						try {
							suministro  = jdbcTemplateOracle.queryForObject(sqlSuministro, suministroMapper, numRegistro);
						} catch(EmptyResultDataAccessException e) {
							suministro.setIdAlim(0L);
							suministro.setIdLlave(0L);
							suministro.setIdRegistro(0L);
							suministro.setIdRutaSumin(0L);
							suministro.setIdSed(0L);
							Log.logError2(String.format("El Sumin. Izq. [%s] no existe o su estado no es valido('Habilitado'/'Modificacion'/'EnProcesoRetiro'). ",registroOrden.getSumIzquierdo()));
							vtmpError=String.format("El Sumin. Izq. [%s] no existe o su estado no es valido('Habilitado'/'Modificacion'/'EnProcesoRetiro'). ",registroOrden.getSumIzquierdo());
							cant_error ++;
						}
						
						regAutActiva.setIdSumIzq(suministro.getIdRegistro());
						regAutActiva.setIdAlim(suministro.getIdAlim());
						regAutActiva.setIdSed(suministro.getIdSed());
						regAutActiva.setIdLlave(suministro.getIdLlave());
						regAutActiva.setIdRutaIzq(suministro.getIdRutaSumin());
						
						paut.setIdSumIzq(suministro.getIdRegistro());
						paut.setIdAlim(suministro.getIdAlim());
						paut.setIdSed(suministro.getIdSed());
						paut.setIdLlave(suministro.getIdLlave());
						paut.setIdRutaIzq(suministro.getIdRutaSumin());
						
						id_ruta_sumin=suministro.getIdRutaSumin();
					}
					
					Long numRegistroD = Long.parseLong(registroOrden.getSumDerecho());
					if(numRegistroD >0){
						try {
							suministroD  = jdbcTemplateOracle.queryForObject(sqlSuministro, suministroMapper, numRegistroD);
						} catch(EmptyResultDataAccessException e) {
							suministroD.setIdAlim(0L);
							suministroD.setIdLlave(0L);
							suministroD.setIdRegistro(0L);
							suministroD.setIdRutaSumin(0L);
							suministroD.setIdSed(0L);
							
							paut.setIdAlim(0L);
							paut.setIdLlave(0L);
							paut.setIdSumDer(0L);
							paut.setIdRutaDer(0L);
							paut.setIdSed(0L);
							
							Log.logError2(String.format("El Sumin. Der. [%s] no existe o su estado no es valido('Habilitado'/'Modificacion'/'EnProcesoRetiro'). ",registroOrden.getSumDerecho()));
							vtmpError=String.format("El Sumin. Der. [%s] no existe o su estado no es valido('Habilitado'/'Modificacion'/'EnProcesoRetiro'). ",registroOrden.getSumDerecho());
							cant_error ++;
						}
						
						regAutActiva.setIdSumDer(suministroD.getIdRegistro());
						regAutActiva.setIdAlim(suministroD.getIdAlim());
						regAutActiva.setIdSed(suministroD.getIdSed());
						regAutActiva.setIdLlave(suministroD.getIdLlave());
						regAutActiva.setIdRutaDer(suministroD.getIdRutaSumin());
						
						paut.setIdSumDer(suministroD.getIdRegistro());
						paut.setIdAlim(suministroD.getIdAlim());
						paut.setIdSed(suministroD.getIdSed());
						paut.setIdLlave(suministroD.getIdLlave());
						paut.setIdRutaDer(suministroD.getIdRutaSumin());
						
						id_ruta_sumin=suministroD.getIdRutaSumin();
					}
					
					if(regAutActiva.getIdAlim()==0L || regAutActiva.getIdSed()==0 || regAutActiva.getIdLlave()==0)
					{
						Log.logError2("No hay datos para calcular la Cadena Electrica. ");
						vtmpError="No hay datos para calcular la Cadena Electrica. ";
						cant_error ++;
					}
					if(id_ruta_sumin==null)
					{
						id_ruta_sumin=0L;
					}
						
					if(id_ruta_sumin==0L) /*no hay datos de aledaños ni suministro existente*/
					{
						Log.logError2("No hay datos para calcular la ruta del suministro. ");
						vtmpError="No hay datos para calcular la ruta del suministro. ";
						cant_error ++;
					}
					else/*completar y verificar rutas aledañas*/
					{
						if(regAutActiva.getIdRutaDer()==null) {
							regAutActiva.setIdRutaDer(0L);
						}
						if(regAutActiva.getIdRutaIzq()==null) {
							regAutActiva.setIdRutaIzq(0L);
						}
						
						if(regAutActiva.getIdRutaDer()==0) { 
//							regAutActiva.setIdRutaDer(numRegistroD);
							regAutActiva.setIdRutaDer(suministroD.getIdRutaSumin());
						}
						if(regAutActiva.getIdRutaIzq()==0) {
//							regAutActiva.setIdRutaIzq(numRegistro);
							regAutActiva.setIdRutaIzq(suministro.getIdRutaSumin());
						}
						
						UbiRutaDTO oUbiRuta= new UbiRutaDTO();
						UbiRutaMapper ubiRutaMapper = new UbiRutaMapper();
						
						if(regAutActiva.getIdRutaDer()==null)
						{
							regAutActiva.setIdRutaDer(0L);
						}
						if(regAutActiva.getIdRutaIzq()==null)
						{
							regAutActiva.setIdRutaIzq(0L);
						}
						
						oUbiRuta = jdbcTemplateOracle.queryForObject(sqlCountUbiRuta,ubiRutaMapper,regAutActiva.getIdRutaDer(),regAutActiva.getIdRutaIzq());
						
						if(oUbiRuta.getCount()>1)
						{
							Log.logError2(String.format("Los Aledaños izq. [%s], der. [%s] son de distinto [Sector-Zona]. ",registroOrden.getSumIzquierdo(),registroOrden.getSumDerecho()));
							vtmpError=String.format("Los Aledaños izq. [%s], der. [%s] son de distinto [Sector-Zona]. ",registroOrden.getSumIzquierdo(),registroOrden.getSumDerecho());
							cant_error ++;
						}
					}
					
					/* MA_11122019 - Inicio: Buscar ruta de lectura generada de forma automática */
					
					
					
					
					String sqlRutaAutLectura = RutaAutLecturaMapper.SQLPOSTGRESQL_SELECT_FOR_RUTAUTLECTURA;
					String sqlCodRutaAuto = RutaAutLecturaMapper.SQLPOSTGRESQL_SELECT_FOR_CODRUTAAUTO;
					String sqlCodRutaAuto2 = RutaAutLecturaMapper.SQLPOSTGRESQL_SELECT_FOR_CODRUTAAUTO2;
					
					RutaAutLecturaMapper rutaAutLecturaMapper = new RutaAutLecturaMapper();
					
					RutaAutLecturaDTO rutaAutLectura = new RutaAutLecturaDTO();
					
					try {
						// printf("\nBuscando INTO_vsolicitud [%d]", preg->num_orden);
						rutaAutLectura = jdbcTemplatePostgres.queryForObject(sqlRutaAutLectura, rutaAutLecturaMapper, numOrdenLong);
					} catch(EmptyResultDataAccessException e) {
						rutaAutLectura.setSolicitud(0L);
					}
					catch(Exception e) {
						rutaAutLectura.setTipoSol("");
						rutaAutLectura.setSolicitud(0L);
						
						log.error("Error: SELECT TIPO, NRO_SOLICITUD...\n");
						Log.logError2("Error: SELECT TIPO, NRO_SOLICITUD...\n");
						return 0;
					}
					
					
					if(rutaAutLectura.getSolicitud()>0){
						
						if(rutaAutLectura.getTipoSol().compareTo("E")==0) {
							try {
								rutaAutLectura= new RutaAutLecturaDTO();
								log.info(String.format("\nObteniendo ruta automatica Expansion [%d]",numOrdenLong));
								rutaAutLectura = jdbcTemplatePostgres.queryForObject(sqlCodRutaAuto, rutaAutLecturaMapper, numOrdenLong);
							} catch(EmptyResultDataAccessException e) {
								rutaAutLectura.setCodRutaAuto(0L);
								log.error("Error: Buscando INTO_vcod_ruta_auto_expansion...\n");
								Log.logError2("Error: Buscando INTO_vcod_ruta_auto_expansion...\n");
								return 0;
							}
						}
						
						if(rutaAutLectura.getTipoSol().compareTo("C")==0) { //Constructores
							try {
								rutaAutLectura= new RutaAutLecturaDTO();
								rutaAutLectura = jdbcTemplatePostgres.queryForObject(sqlCodRutaAuto2, rutaAutLecturaMapper, numOrdenLong, numCuentaLong);
								//do_error("Buscando INTO_vcod_ruta_auto_constructor...");  // TFRC 20210422 - no usar do_error
							} catch(EmptyResultDataAccessException e) {
								rutaAutLectura.setCodRutaAuto(0L);
								log.error("Error: Buscando INTO_vcod_ruta_auto_constructor...\n");
								Log.logError2("Error: Buscando INTO_vcod_ruta_auto_constructor...\n");
								return 0;
							}
						}
					} else { //MARCA ED 
						log.info("Obteniendo ruta automatica por direccion "+ numOrdenLong);
						//Búsqueda por dirección
						
						/*
						String sql1 = "SELECT FN_OBTIENE_RUTALECT(?)\r\n"
								+ "        FROM DUAL ";
						*/						
						
						Long codRutaAuto;
						//do_error("Buscando INTO_FN_OBTIENE_RUTALECT...");  // TFRC 20210422 - no usar do_error
						try {
							
							codRutaAuto=FN_OBTIENE_RUTALECT(numOrdenLong+"");
							rutaAutLectura.setCodRutaAuto(codRutaAuto);
							
						} catch (Exception e) {
							rutaAutLectura.setCodRutaAuto(0L);
							log.error("Error: Buscando INTO_FN_OBTIENE_RUTALECT...\\n, Excepcion -> " + e);
							Log.logError2("Error: Buscando INTO_FN_OBTIENE_RUTALECT...\\n, Excepcion -> " + e);
							return 0;
						}
					}
					
					//MARCA ED 1
					String vcod_ruta_auto = String.valueOf(rutaAutLectura.getCodRutaAuto());
					// if(strlen(vcod_ruta_auto)>0){  /* && vtiene_ruta ==0 */
					
					if(vcod_ruta_auto==null)
					{
						vcod_ruta_auto="";
					}
					
					if(vcod_ruta_auto.length() == 10 ) {/* && vtiene_ruta ==0 */   /* ANL_20201107 - incidente activaciones */
					//valida que la ruta creada no este asignada
						if(verificarRutaLibre(vcod_ruta_auto) == 0)
						{
							log.error("Ruta [%s] ya esta asignada a otro servicio :"+ vcod_ruta_auto);
							Log.logError2("Ruta [%s] ya esta asignada a otro servicio :"+ vcod_ruta_auto);
							vtmpError="Ruta [%s] ya esta asignada a otro servicio :"+ vcod_ruta_auto;
							cant_error ++;
						} else {

							RutaLecturaDTO rutalectura = new RutaLecturaDTO();
								
							String sqlrutaLectura = RutaLecturaMapper.SQLORACLE_SELECT_FOR_RUTALECTURA;
							try {
								rutalectura = jdbcTemplateOracle.queryForObject(sqlrutaLectura, new RutaLecturaMapper(), vcod_ruta_auto, vcod_ruta_auto, vcod_ruta_auto);
							} catch (Exception e) {
								log.error("Error: Buscando datos de la ruta de lectura...\\n, Excepcion -> " + e);
							}
							
							String vsector = String.valueOf(rutalectura.getVsector());
							String vzona = String.valueOf(rutalectura.getVzona());
							String vcorrelativo = String.valueOf(rutalectura.getVcorrelativo());
							
							registroOrden.setSector(vsector);
							registroOrden.setZona(vzona);
							registroOrden.setCorrelativo(vcorrelativo);
							
							regAutActiva.setCrearRuta(2L);
							paut.setCrearRuta(2L);
							
							String sqlUpdUbiRuta = "UPDATE UBI_RUTA\r\n"
									+ "			SET COD_RUTA = '000000000'\r\n"
									+ "			WHERE COD_RUTA = ?";
							
							try {
								jdbcTemplateOracle.update(sqlUpdUbiRuta, vcod_ruta_auto);
							} catch (Exception e) {
								log.error("Error: EXEC SQL UPDATE UBI_RUTA_SET_COD_RUTA = ...\\n , Excepcion ->"+e);
								Log.logError2("Error: EXEC SQL UPDATE UBI_RUTA_SET_COD_RUTA = ...\\n , Excepcion ->"+e);
								return 0;
							}
							
							log.info(String.format("Se asigno valores de ruta generada y se actualizo tabla de rutas [%s]",vcod_ruta_auto));
						}
				}
				else
				{
					log.info(String.format("\nNo se encontro ruta automatica para orden [%d], se autogenerara", numOrdenLong));
					// sprintf(vtmpError," No se encontro ruta automatica para orden [%d]", preg->num_orden);  /* ANL_20201107 - incidente activaciones */
				}
					/* MA_11122019 - Fin: Buscar ruta de lectura generada de forma automática */
					
					//regAutActiva.setIdSet(idSet);
					
				} else {
					
					String sqlSrvSet = SrvSetMapper.SQLORACLE_SELECT_FOR_IDSET;
					String sqlSrvAlimentador = SrvAlimentadorMapper.SQLORACLE_SELECT_FOR_SRVALIMENTADOR;
					String sqlSrvSed = SrvSedMapper.SQLORACLE_SELECT_FOR_SRVSED;
					String sqlSrvLlave = SrvLlaveMapper.SQLORACLE_SELECT_FOR_IDLLAVE;
					
					SrvSetMapper srvSetMapper = new SrvSetMapper();
					SrvAlimentadorMapper srvAlimentadorMapper = new SrvAlimentadorMapper(); 
					SrvSedMapper srvSedMapper = new SrvSedMapper();
					SrvLlaveMapper srvLlaveMapper = new SrvLlaveMapper();
					
					Long idSet = 0L;
					Long idAlimentador;
					Long idSed;
					Long idLlave;
					
					try {
						idSet = jdbcTemplateOracle.queryForObject(sqlSrvSet, srvSetMapper, registroOrden.getSet());
					} catch(EmptyResultDataAccessException e) {
						idSet = 0L;
						log.error("[Codigo set] No es valido ");
						Log.logError2("[Codigo set] No es valido ");
						vtmpError="[Codigo set] No es valido ";
						cant_error++;
					}
					
					try {
						idAlimentador = jdbcTemplateOracle.queryForObject(sqlSrvAlimentador, srvAlimentadorMapper, registroOrden.getAlimentador(), idSet);
					} catch(EmptyResultDataAccessException e) {
						idAlimentador = 0L;
						log.error("[Codigo alimentador] No es valido ");
						Log.logError2("[Codigo alimentador] No es valido ");
						vtmpError="[Codigo alimentador] No es valido ";
						cant_error++;
					}
					
					try {
						idSed = jdbcTemplateOracle.queryForObject(sqlSrvSed, srvSedMapper, registroOrden.getSed(), idAlimentador);
					} catch(EmptyResultDataAccessException e) {
						idSed = 0L;
						log.error("[Codigo Sed] No es valido ");
						Log.logError2("[Codigo Sed] No es valido ");
						vtmpError="[Codigo Sed] No es valido ";
						cant_error++;
					}
					
					try {
						idLlave = jdbcTemplateOracle.queryForObject(sqlSrvLlave, srvLlaveMapper, registroOrden.getLlave(), idSed);
					} catch(EmptyResultDataAccessException e) {
						idLlave = 0L;
						log.error("[Descripcion Llave] No es valido ");
						Log.logError2("[Descripcion Llave] No es valido ");
						vtmpError="[Descripcion Llave] No es valido ";
						cant_error++;
					}
					
					regAutActiva.setIdSet(idSet);
					regAutActiva.setIdAlim(idAlimentador);
					regAutActiva.setIdSed(idSed);
					regAutActiva.setIdLlave(idLlave);
					
					paut.setIdSet(idSet);
					paut.setIdAlim(idAlimentador);
					paut.setIdSed(idSed);
					paut.setIdLlave(idLlave);
					
					/*Validar ruta ingresada en arch entrada*/					
					String cod_ruta = "";
					try {
						cod_ruta=String.format("9%02d%03d%04d",Integer.parseInt(registroOrden.getSector()), Integer.parseInt(registroOrden.getZona()), Integer.parseInt(registroOrden.getCorrelativo()) );
					} catch (Exception e) {
						log.error("error al concatenar en cod_ruta , Excepcion -> " + e);
					}
					log.info("cod_ruta generado es " + cod_ruta);
					if(verificarRutaLibre(cod_ruta)==0)
					{
						Log.logError2("[Ruta] ya esta asignada a otro servicio");
						vtmpError="[Ruta] ya esta asignada a otro servicio";
					 	cant_error ++;
					}
					
					Long sumIzquierdo = Long.parseLong(registroOrden.getSumIzquierdo());
					String sqlIdSuministro = IdSuministroMapper.SQLORACLE_SELECT_FOR_IDSUMINISTRO;
					IdSuministroMapper idSuministroMapper = new IdSuministroMapper();
					Long idSuministro;
					
					if(sumIzquierdo > 0) {
						
						try {
							idSuministro = jdbcTemplateOracle.queryForObject(sqlIdSuministro, idSuministroMapper, sumIzquierdo);
						} catch(EmptyResultDataAccessException e) {
							idSuministro = 0L;
							
							Log.logError2(String.format("El Sumin. Izq. [%s] no existe o su estado no es valido('Habilitado'/'Modificacion'/'EnProcesoRetiro'). ",registroOrden.getSumIzquierdo()));
							vtmpError=String.format("El Sumin. Izq. [%s] no existe o su estado no es valido('Habilitado'/'Modificacion'/'EnProcesoRetiro'). ",registroOrden.getSumIzquierdo());
							cant_error ++;
						}
						
						regAutActiva.setIdSumIzq(idSuministro);
						paut.setIdSumIzq(idSuministro);
					}
					
					Long sumDerecho = Long.parseLong(registroOrden.getSumDerecho());
					if(sumDerecho > 0) {
						
						try {
							idSuministro = jdbcTemplateOracle.queryForObject(sqlIdSuministro, idSuministroMapper, sumDerecho);
						} catch(EmptyResultDataAccessException e) {
							idSuministro = 0L;
							Log.logError2(String.format("El Sumin. Der. [%s] no existe o su estado no es valido('Habilitado'/'Modificacion'/'EnProcesoRetiro'). ",registroOrden.getSumDerecho()));
							vtmpError=String.format("El Sumin. Der. [%s] no existe o su estado no es valido('Habilitado'/'Modificacion'/'EnProcesoRetiro'). ",registroOrden.getSumDerecho());
							cant_error ++;
						}
						
						regAutActiva.setIdSumDer(idSuministro);
						paut.setIdSumDer(idSuministro);
					}
					
					regAutActiva.setCrearRuta(2L);
					paut.setCrearRuta(2L);
				}
			 
			 PcrRefDTO pcrRef = new PcrRefDTO();
			 PcrRefDTO numPcrRef = new PcrRefDTO();
			 
			 PcrRefMapper pcrRefMapper = new PcrRefMapper();
			 
			 String sqlPcrRef = PcrRefMapper.SQLORACLE_SELECT_FOR_PCRREF;
			 String sqlPcrRef2 = PcrRefMapper.SQLORACLE_SELECT_FOR_PCRREF2;
			 String sqlPcrRef3 = PcrRefMapper.SQLORACLE_SELECT_FOR_PCRREF3;
				
			 Long sumPcrRef = Long.parseLong(registroOrden.getSumPcrRef());
				
				if(sumPcrRef > 0) {
					try {
						pcrRef = jdbcTemplateOracle.queryForObject(sqlPcrRef, pcrRefMapper, sumPcrRef);
					} catch(EmptyResultDataAccessException e) {
						
						try {
							pcrRef = jdbcTemplateOracle.queryForObject(sqlPcrRef2, pcrRefMapper, sumPcrRef);
						} catch(EmptyResultDataAccessException er) {
							pcrRef.setIdPcr(0L);
							pcrRef.setIdTension(0L);
							paut.setIdPcr(0L);
							paut.setIdPcrTension(0L);
							
							Log.logError2("El [Sum_PCR_Ref] no existe o no es valido. ");
							vtmpError="El [Sum_PCR_Ref] no existe o no es valido. ";
							cant_error ++;
						}
					}
					
					regAutActiva.setIdPcr(pcrRef.getIdPcr());
					regAutActiva.setIdPcrTension(pcrRef.getIdTension());
					
					paut.setIdPcr(pcrRef.getIdPcr());
					paut.setIdPcrTension(pcrRef.getIdTension());
				}
				
				Long numPcr = Long.parseLong(registroOrden.getNumPcr());
				if(numPcr > 0) {
					try {
						numPcrRef = jdbcTemplateOracle.queryForObject(sqlPcrRef3, pcrRefMapper, numPcr);
					} catch(EmptyResultDataAccessException er) {
						numPcrRef.setIdPcr(0L);
						numPcrRef.setIdTension(0L);
						Log.logError2(String.format("El Numero de PCR [%s] no existe. ",registroOrden.getNumPcr()));
						vtmpError=String.format("El Numero de PCR [%s] no existe. ",registroOrden.getNumPcr());
						cant_error ++;
					}
					
					regAutActiva.setIdPcr(numPcrRef.getIdPcr());
					regAutActiva.setIdPcrTension(numPcrRef.getIdTension());
					
					paut.setIdPcr(numPcrRef.getIdPcr());
					paut.setIdPcrTension(numPcrRef.getIdTension());
				}
			 
			/*
				
				String fechaEje = registroOrden.getFechaEjecucion();
				if(registroOrden.getNumMedidorInstall().length() > 0) {
					fechaEje = registroOrden.getFechaEjecucion();
				}
				
				//fechaEje = fechaEje.concat(" 00:00:00");
				
				try {
					srvVenta = jdbcTemplateOracle.queryForObject(sqlSrvVenta, srvVentaMapper, servicioElectricoOrden.getIdDato());
				} catch(EmptyResultDataAccessException e) {
					srvVenta.setIdConfPres(0L);
					srvVenta.setIdOrden(0L);
				}
				
//				try {
//					String sqlfechaEjeConvertido = "select to_date(substr(REPLACE('?','T',' '), 1,19 ),'YYYY-MM-DD HH24:MI:SS') from dual";
//					String fechaEjeConvertido = jdbcTemplateOracle.queryForObject(sqlfechaEjeConvertido, String.class, fechaEje);
//				} catch (Exception e) {
//					String fechaEjeConvertido = "sin fecha";
//					System.out.println(e);
//				}
				
				String fechaEjesinT = fechaEje.replace('T', ' ');
				SimpleDateFormat formato2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"); 
				Date fechaEjeDate;
				try {
					fechaEjeDate= formato2.parse(fechaEjesinT);
				} catch (ParseException e) {
					fechaEjeDate = null;
					e.printStackTrace();
				}
				
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");  
				String strDate = dateFormat.format(fechaEjeDate);  
				System.out.println("strDate es : " + strDate);
				
				jdbcTemplateOracle.update(sqlUpdSrvVenta, strDate, regAutActiva.getIdSrvVenta());
				jdbcTemplateOracle.update(sqlUpdOrdConex, strDate, regAutActiva.getIdOrdConex());
				jdbcTemplateOracle.update(sqlUpdVtaSolSrvEle, strDate, regAutActiva.getIdSolSrvEle());
				jdbcTemplateOracle.update(sqlUpdSrvVenta3, strDate, srvVenta.getIdOrden(), srvVenta.getIdConfPres());
				
				
			} else {
				if(codigo.compareTo("RecEjecucion")==0) {
					jdbcTemplateOracle.update(sqlUpdSrvVenta2, idRegistro, servicioElectricoOrden.getIdConfPres());
				}
			}
		}
		if(validarSolicitud.getCodigo().compareTo("") != 0) {
			codigo = validarSolicitud.getCodigo();
		} else {
			codigo = validarSolicitud2.getCodigo();
		}
		*/
		
		
		
		/*Validar Medidores Instalados y Retirados*/
		if(esBT6 != 1) {// TFRC 20210414 - no validar medidores para cuentas en tarifa BT6
			Integer cantMedidor = 0;
			paut.setCantMedidor(0);
			regAutActiva_CantMedidor=0;
			Long numMedidorInstall = Long.parseLong(registroOrden.getNumMedidorInstall());
			Long numMedidorRet = Long.parseLong(registroOrden.getNumMedidorRet());

			if(numMedidorInstall > 0) {
				cantMedidor = 1;
			}
			if(numMedidorRet > 0) {
				cantMedidor = 2;
			}
			for(int i=0; i<cantMedidor; i++){
				String marca = "";
				String modelo = "";	
				String nroComponente = "";
				if(i==0) {
					marca = registroOrden.getCodMarcaInstall();
					modelo = registroOrden.getCodModeloInstall();
					nroComponente = registroOrden.getNumMedidorInstall();
					regAutActiva.setMedidorInstallIdMedidor(buscar_medidor(registroOrden));
					paut.setMedidorInstallIdMedidor(regAutActiva.getMedidorInstallIdMedidor());
				}
				if(i==1) {
					marca = registroOrden.getCodMarcaRet();
					modelo = registroOrden.getCodModeloRet();
					nroComponente = registroOrden.getNumMedidorRet();
					regAutActiva.setMedidorRetIdMedidor(buscar_medidor(registroOrden));
					paut.setMedidorRetIdMedidor(regAutActiva.getMedidorRetIdMedidor());
				}
				
//					regAutActiva.setMedidorInstallIdMedidor(buscar_medidor(registroOrden));
//					paut.setMedidorInstallIdMedidor(regAutActiva.getMedidorInstallIdMedidor());
				
					if(regAutActiva.getMedidorInstallIdMedidor()==0 && srv==0L)
					{
						Log.logError2(String.format("El Medidor [%s], marca [%s], modelo[%s] no existe. ", registroOrden.getNumMedidorInstall(),registroOrden.getCodMarcaInstall(),registroOrden.getCodModeloInstall() ));
						vtmpError=String.format("El Medidor [%s], marca [%s], modelo[%s] no existe. ", registroOrden.getNumMedidorInstall(),registroOrden.getCodMarcaInstall(),registroOrden.getCodModeloInstall());
						cant_error ++;
					}
					else
					{
						if(registroOrden.getNovedadInstall()=="I")/*Si se desea instalar, verificar que este disponible*/
						{
							if(regAutActiva.getMedidorInstallIdMedidor()==0 || (srv>0 && regAutActiva.getIdSrvElectrico()!=srv))
							{/* FVR_20210709 */
								Log.logError2(String.format("El Medidor a instalar [%s], marca [%s], modelo[%s] no esta Disponible. ",registroOrden.getNumMedidorInstall(), registroOrden.getCodMarcaInstall(),registroOrden.getCodModeloInstall()));
								vtmpError=String.format("El Medidor a instalar [%s], marca [%s], modelo[%s] no esta Disponible. ",registroOrden.getNumMedidorInstall(), registroOrden.getCodMarcaInstall(),registroOrden.getCodModeloInstall());
								cant_error ++;
							}
							else
							{
								if(regAutActiva.getIdSrvElectrico()==srv) 
									{
									regAutActiva.setMedidorInstallIdMedidor(0L);
									}
								/* FVR_20210709: si el medidor esta asignado al suministro, limpiar id */
							}
						}
						else
						{
							if(regAutActiva.getMedidorInstallIdMedidor()==0 || regAutActiva.getIdSrvElectrico()!=srv)
							{
								Log.logError2(String.format("El Medidor a Retirar [%s], marca [%s], modelo[%s] no pertenece al suministro. ",registroOrden.getNumMedidorInstall(), registroOrden.getCodMarcaInstall(),registroOrden.getCodModeloInstall()));
								vtmpError=String.format("El Medidor a Retirar [%s], marca [%s], modelo[%s] no pertenece al suministro. ", registroOrden.getNumMedidorInstall(), registroOrden.getCodMarcaInstall(),registroOrden.getCodModeloInstall());
								cant_error ++;
							}
						}
					}
					
					/*buscar medidas*/
					
					Long idMedida;
					if(regAutActiva.getMedidorInstallIdMedidor()>0) {
						try {
							String sqlBuscarMedidas = BuscarMedidasMapper.SQLPOSTGRE_SELECT_FOR_IDMEDIDA;
							BuscarMedidasMapper buscarMedidasMapper = new BuscarMedidasMapper();
							
							idMedida = jdbcTemplatePostgres.queryForObject(sqlBuscarMedidas, buscarMedidasMapper, regAutActiva.getMedidorInstallIdMedidor());
							regAutActiva.setMedidorInstallIdMedida(idMedida);
							if(regAutActiva.getCantMedidor()==null)
							{
								regAutActiva.setCantMedidor(0);
							}
							regAutActiva.setCantMedidor(regAutActiva.getCantMedidor()+1);
							regAutActiva_idMedida=idMedida;
							regAutActiva_CantMedidor=regAutActiva.getCantMedidor();
						} catch(EmptyResultDataAccessException e) {
							idMedida = 0L;
							Log.logError2(String.format("El Medidor [%s] no tiene medidas. ", registroOrden.getNumMedidorInstall()));
							vtmpError=String.format("El Medidor [%s] no tiene medidas. ", registroOrden.getNumMedidorInstall());
							cant_error ++;
							regAutActiva.setMedidorInstallIdMedida(idMedida);
							regAutActiva_idMedida=idMedida;
						}
					}
				}
						/*Integer valor=0; 
						try
						{
						valor=regAutActiva.getCantMedidor();
						if(valor==null)
						{
							valor=0;
						}
						}
						catch(Exception ex)
						{
							valor=0;
						}
						valor=valor+cont;*/
						/*
						cont=cont+1;
						regAutActiva.setCantMedidor(cont);
						//regAutActiva.setCantMedidor(cont++);
					}
					
									
				if(i==0) {
					regAutActiva.setMedidorInstallIdMedidor(compEstUbicacion.getIdComponente());
					if(registroOrden.getNovedadInstall().compareTo("I")==0) {
						if(regAutActiva.getIdSrvElectrico()==compEstUbicacion.getIdServicio()) {
							regAutActiva.setMedidorInstallIdMedidor(0L);
						}
					}
					
					
				}
				if(i==1) {
					regAutActiva.setMedidorRetIdMedidor(compEstUbicacion.getIdComponente());
					if(registroOrden.getNovedadRet().compareTo("I")==0) {
						if(regAutActiva.getIdSrvElectrico()==compEstUbicacion.getIdServicio()) {
							regAutActiva.setMedidorRetIdMedidor(0L);
						} 
					}
					
					if(regAutActiva.getMedidorRetIdMedidor()>0) {
						try {
							idMedida = jdbcTemplatePostgres.queryForObject(sqlBuscarMedidas, buscarMedidasMapper, regAutActiva.getMedidorRetIdMedidor());
						} catch(EmptyResultDataAccessException e) {
							idMedida = 0L;
						}
						
						regAutActiva.setMedidorRetIdMedida(idMedida);
						cont=cont+1;
						regAutActiva.setCantMedidor(cont);
						//regAutActiva.setCantMedidor(cont++);
					}					
				}
			}*/
		}
		
		if(cant_error>0)
		{
			return 0;			
		}
		return 1;
	}

private int valida_DTC(String pdes,String ptab)
{
	String valor=pdes;
	String nomTabla=ptab;
	String sqlVtaAutoTabla = VtaAutoTablaMapper.SQLORACLE_SELECT_FOR_VTAAUTOTABLA;
	VtaAutoTablaMapper vtaAutoTablaMapper= new VtaAutoTablaMapper();
	VtaAutoTablaDTO vtaAutoTabla = new VtaAutoTablaDTO();
	
	try {
		vtaAutoTabla = jdbcTemplateOracle.queryForObject(sqlVtaAutoTabla, vtaAutoTablaMapper, valor, nomTabla);
		pcod=vtaAutoTabla.getCodigo();
		pcod=pcod.trim();
	} catch(EmptyResultDataAccessException e) {
		log.error("Error: SELECT id_componente ....\n");
		Log.logError2("Error: SELECT id_componente ....\n");
		return 0;
	}
	
	return 1;	
}

	private long buscar_medidor(RegistroOrdenDTO registroOrden)
	{
		CompEstUbicacionDTO compEstUbicacion = new CompEstUbicacionDTO();
		String sqlCompEstUbicacion = CompEstUbicacionMapper.SQLPOSTGRE_SELECT_FOR_COMPEST;
		CompEstUbicacionMapper compEstUbicacionMapper = new CompEstUbicacionMapper();
		
		try {
			compEstUbicacion = jdbcTemplatePostgres.queryForObject(sqlCompEstUbicacion, compEstUbicacionMapper, registroOrden.getCodModeloInstall(), registroOrden.getCodMarcaInstall(), registroOrden.getNumMedidorInstall());
		} catch(EmptyResultDataAccessException e) {
			compEstUbicacion.setIdComponente(0L);
			compEstUbicacion.setIdEstComp(0L);
			compEstUbicacion.setIdServicio(0L);
			compEstUbicacion.setTipoUbic("");
			srv=0L;
		}
		catch(Exception e)
		{
			compEstUbicacion.setIdComponente(0L);
			compEstUbicacion.setIdEstComp(0L);
			compEstUbicacion.setIdServicio(0L);
			compEstUbicacion.setTipoUbic("");
			/*si no existe el medidor*/
			 srv=0L;
			log.error("Error: SELECT id_componente ....\n");
			Log.logError2("Error: SELECT id_componente ....\n");
			return 0;
		}
		compEstUbicacion.setTipoUbic(compEstUbicacion.getTipoUbic().trim());
		if( compEstUbicacion.getIdEstComp()==17 ){ /*si esta disponible*/
			  srv=0L;
		}
		else{
		     srv=compEstUbicacion.getIdServicio();
		     if(
		    		 compEstUbicacion.getIdEstComp()!=18 ||  
		    		 compEstUbicacion.getTipoUbic().compareTo("com.synapsis.synergia.nucleo.domain.interfaces.servicioelectrico.ServicioElectrico")!=0 
		       )
		        {
		    	 /*si no esta asignado a otro servicio electrico*/
		    	 compEstUbicacion.setIdComponente(0L);
		        }
		     }
		 return compEstUbicacion.getIdComponente();
	}
	
	int informar_ejecucion(RegistroOrdenDTO registroOrden,RegAutActivaDTO regAutActiva,long pid_est_srv_electrico, Long numOrdenLong)
	{
		String fecha_eje;
		//ServicioElectricoOrdenDTO servicioElectricoOrden = new ServicioElectricoOrdenDTO();	
		String sqlUpdSrvElectric = UpdSrvElectricoMapper.SQLORACLE_UPDATE_FOR_SRVELECTRIC;
		String sqlSrvVenta = SrvVentaMapper.SQLORACLE_SELECT_FOR_SRVVENTA;
		long  id_est_srv_electrico = pid_est_srv_electrico; /*MVC*/
		
		/*Si se trata de una Orden Venta por Modificacion en Estado PorEjecutar, cambiar estado de Servicio electrico*/
		//if(servicioElectricoOrden.getIdEstSrvElectrico()==0) {
		if(id_est_srv_electrico==0) {
			jdbcTemplateOracle.update(sqlUpdSrvElectric, paut.getIdSrvElectrico());
		}
		fecha_eje=registroOrden.getFecLecturaInstall();
		
		if(registroOrden.getNumMedidorInstall()==null)
		{
			registroOrden.setNumMedidorInstall("");
		}
		
		if(origen.equals("EORDER") && registroOrden.getNumMedidorInstall().length()==0)
		{
			//Para casos en que no hay cambio de medidor
			fecha_eje=registroOrden.getFechaEjecucion();
		}
		
		fecha_eje=fecha_eje+" 00:00:00";
		
		/*recuperar el presupuesto del servicio de venta*/
		SrvVentaDTO srvVenta = new SrvVentaDTO();
		SrvVentaMapper srvVentaMapper = new SrvVentaMapper();
		try {
			srvVenta = jdbcTemplateOracle.queryForObject(sqlSrvVenta, srvVentaMapper, paut.getIdSrvVenta());
		} catch(EmptyResultDataAccessException e) {
			srvVenta.setIdConfPres(0L);
			srvVenta.setIdOrden(0L);
			Log.logError2("Orden sin Servicios de Venta. ");
			return -1;
		}
		Long idOrden=srvVenta.getIdOrden();
		String sqlUpdOrdConex = UpdOrdConexMapper.SQLORACLE_UPDATE_FOR_ORDCONEX;
		String sqlUpdSrvVenta4 = UpdSrvVentaMapper.SQLORACLE_UPDATE_FOR_SRVVENTA4;
		String sqlUpdVtaSolSrvEle = UpdVtaSolSrvEle.SQLORACLE_UPDATE_FOR_VTASOLSRVELE;
		String sqlUpdSrvVenta = UpdSrvVentaMapper.SQLORACLE_UPDATE_FOR_SRVVENTA;
		
		String sqlUpdSrvVenta3 = UpdSrvVentaMapper.SQLORACLE_UPDATE_FOR_SRVVENTA3;
		String sqlCountSrvVenta = SrvVentaMapper.SQLORACLE_SELECT_FOR_SRVVENTA_COUNT;
		
		String fechaEje = registroOrden.getFechaEjecucion();
		String fechaEjesinT = fechaEje.replace('T', ' ');
		SimpleDateFormat formato2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"); 
		Date fechaEjeDate;
		try {
			fechaEjeDate= formato2.parse(fechaEjesinT);
		} catch (ParseException e) {
			fechaEjeDate = null;
			e.printStackTrace();
		}
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");  
		String strDate = dateFormat.format(fechaEjeDate);  
		log.info("strDate es : " + strDate);
		
		
		//jdbcTemplateOracle.update(sqlUpdVtaSolSrvEle, strDate, regAutActiva.getIdSolSrvEle());
		
		/*actualizar los datos del servicio electrico*/
		jdbcTemplateOracle.update(sqlUpdSrvVenta, strDate, paut.getIdSrvVenta());
		//do_error("UPDATE vta_srv_venta SET fecha_ejecucion ... ( Srv_Electrico)"); // TFRC 20210422 - no usar do_error

		  /* se quito la actualizacion*/
		/*actualizar los datos de la orden de conexion*/
		jdbcTemplateOracle.update(sqlUpdOrdConex, strDate, paut.getIdOrdConex());
		//do_error("UPDATE fecha_puser SET fecha_ejecucion ... ( ord_conex)"); // TFRC 20210422 - no usar do_error
		jdbcTemplateOracle.update(sqlUpdVtaSolSrvEle, strDate, paut.getIdSolSrvEle());
		//do_error("UPDATE vta_sol_srv_ele SET fecha_puser..."); // TFRC 20210422 - no usar do_error


		  /*actualizar todos los servicios de venta del mismo presupuesto que no sean servicios electricos*/
		jdbcTemplateOracle.update(sqlUpdSrvVenta3, strDate, srvVenta.getIdOrden(), srvVenta.getIdConfPres());
		//do_error("UPDATE vta_srv_venta SET fecha_ejecucion ...(otros serv.)"); // TFRC 20210422 - no usar do_error


		  /*verificar si aun quedan servicios por Ejecutar*/
		
		int cant;
		srvVenta=jdbcTemplateOracle.queryForObject(sqlCountSrvVenta, srvVentaMapper,srvVenta.getIdOrden());
		cant=srvVenta.getCount();
		
		if(cant>0){ /*Si es asi, salir sin cambiar de estado, pero guardar los cambios realizados*/
			/*TODO: MVC reponer cuando se valie su funcionamiento correcto*/
			Log.ifnMsgToLogFile("COMMIT WORK...");
			return 0;
		}
		/*
		String fechaEjesinT = fechaEje.replace('T', ' ');
		SimpleDateFormat formato2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"); 
		Date fechaEjeDate;
		try {
			fechaEjeDate= formato2.parse(fechaEjesinT);
		} catch (ParseException e) {
			fechaEjeDate = null;
			e.printStackTrace();
		}
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");  
		String strDate = dateFormat.format(fechaEjeDate);  
		System.out.println("strDate es : " + strDate);
		*/
		
		/* CODIGO MBP */
		String sqlEstadoOrden="SELECT  B.ID_STATE\r\n"
				+ "	  FROM    ORD_ORDEN A, WKF_WORKFLOW B\r\n"
				+ "	  WHERE   A.ID_ORDEN        = ?\r\n"
				+ "        	  AND A.ID_WORKFLOW = B.ID_WORKFLOW";
		String idEstado = jdbcTemplateOracle.queryForObject(sqlEstadoOrden, String.class, idOrden);//srvVenta.getIdOrden());
		idEstado=idEstado.trim();
		
		String resul="";
		if(idEstado.equals("PorEjecutar"))
		{
			/*sino se pudo cambiar el estado deshacer cambios*/
	
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplateOracle)
					.withSchemaName("NOSYN")
					.withCatalogName("PRECO")
		            .withProcedureName("PrecoActualizaEstadoVenta")
		            .declareParameters(
		            		new SqlParameter("parNroSolic", Types.VARCHAR),
		            		new SqlParameter("parStateFinal", Types.VARCHAR),
		            		new SqlParameter("parActividad", Types.VARCHAR),
		            		new SqlParameter("parIndicador", Types.NUMERIC),
		            		new SqlOutParameter("varRptaTempora", Types.VARCHAR)
		            		);
			
			try
			{
				
		    Map<String, Object> out5 = simpleJdbcCall.execute(
		    		new MapSqlParameterSource("parNroSolic", numOrdenLong)
		    		.addValue("parStateFinal", "RecEjecucion")
		    		.addValue("parActividad", "AceptarEje")
		    		.addValue("parIndicador", 1)
		    		);
		    
		    resul=out5.get("varRptaTempora").toString();
			}catch(Exception ex)
			{
				String mensaje=ex.getMessage();
			}
		}
		
		if(idEstado.equals("Ejecucion"))
		{
			/*sino se pudo cambiar el estado deshacer cambios */

			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplateOracle)
					.withSchemaName("NOSYN")
					.withCatalogName("PRECO")
		            .withProcedureName("PrecoActualizaEstadoVenta")
		            .declareParameters(
		            		new SqlParameter("parNroSolic", Types.VARCHAR),
		            		new SqlParameter("parStateFinal", Types.VARCHAR),
		            		new SqlParameter("parActividad", Types.VARCHAR),
		            		new SqlParameter("parIndicador", Types.NUMERIC),
		            		new SqlOutParameter("varRptaTempora", Types.VARCHAR)
		            		);
			
			try
			{
				
		    Map<String, Object> out5 = simpleJdbcCall.execute(
		    		new MapSqlParameterSource("parNroSolic", numOrdenLong)
		    		.addValue("parStateFinal", "RecEjecucion")
		    		.addValue("parActividad", "AutoEje")
		    		.addValue("parIndicador", 1)
		    		);
		    
		    resul=out5.get("varRptaTempora").toString();
			}catch(Exception ex)
			{
				String mensaje=ex.getMessage();
			}
			
		}
		/*EXEC SQL CALL Preco.PrecoActualizaEstadoVenta( :vreg.num_orden, 'RecEjecucion', 'AceptarEje',1,:resul); */
		
		//do_error("CALL Preco.PrecoActualizaEstadoVenta...");
		resul=resul.trim();
		if(!resul.equals("OK")) /*sino se pudo cambiar el estado deshacer cambios*/
		{
			Log.logError2(String.format("%s\n", resul));
			return -1;
		}
		/*si todo ok, guardar el cambio de estado*/
		
		return 1;
	}
	
	void do_error(String msgerror)
	{
		/*
		if(sqlca.sqlcode != 0 && sqlca.sqlcode != 1403 ){

		   	printf("\nError [ %d ] en %s \n %s\n", sqlca.sqlcode, msgerror, sqlca.sqlerrm.sqlerrmc);

		   	if(sqlca.sqlerrd[1] != 0 ){
		   	   printf("Error Isam : %d\n" , sqlca.sqlerrd[1]);
		   	}

		   	cierra_archivos();

		   	EXEC SQL ROLLBACK WORK;
		   	exit(1);
		   }
		   */
	}
	private Long FN_OBTIENE_RUTALECT( String v_numorden)
	{
		String sql_stmt="";
		try
		{
		DireccionOrdenMapper oDireccionOrdenMapper= new DireccionOrdenMapper();
		
		String cDireccion_Orden=oDireccionOrdenMapper.SQLORACLE_SELECT_FOR_DIRECCIONORDEN;
		List<DireccionOrdenDTO> listaDireccionOrden = new ArrayList();
		
		String vlote="",vmanzana="",vdistrito="",vprovincia="",vagrupacion="",vnombvia="",vnumvia="",vtipovia="";
		Long vrutalect;
		
		listaDireccionOrden = jdbcTemplateOracle.query(cDireccion_Orden, oDireccionOrdenMapper, v_numorden);
		for (DireccionOrdenDTO direccionOrdenDTO : listaDireccionOrden) {
			if(direccionOrdenDTO.getAtributo().equals("LOTE"))
			{
				vlote= direccionOrdenDTO.getValor();
			}
			if(direccionOrdenDTO.getAtributo().equals("MANZANA"))
			{
				vmanzana= direccionOrdenDTO.getValor();
			}
			if(direccionOrdenDTO.getAtributo().equals("DISTRITO"))
			{
				vdistrito= direccionOrdenDTO.getDes_valor();
			}
			if(direccionOrdenDTO.getAtributo().equals("PROVINCIA"))
			{
				vprovincia= direccionOrdenDTO.getDes_valor();
			}
			if(direccionOrdenDTO.getAtributo().equals("NOMBREAGR"))
			{
				vagrupacion= direccionOrdenDTO.getDes_valor();
			}
			if(direccionOrdenDTO.getAtributo().equals("NOMBREVIA"))
			{
				vnombvia= direccionOrdenDTO.getValor();
			}
			if(direccionOrdenDTO.getAtributo().equals("NUMERO"))
			{
				vnumvia= direccionOrdenDTO.getValor();
			}
			if(direccionOrdenDTO.getAtributo().equals("TIPOVIA"))
			{
				vtipovia= direccionOrdenDTO.getDes_valor();
			}
		}
		
		
		sql_stmt="SELECT NVL(ID_RUTA,0) ID_RUTA  FROM schscom.RUTA_AUT_LECTURA WHERE ESTADO = 'S' and MANZANA = '"+vmanzana+"'"
				+ " AND TRIM(UPPER(LOTE)) = '"+vlote+"'"
				+ " AND TRIM(UPPER(DISTRITO)) = '"+vdistrito+"'"
				+ " AND TRIM(UPPER(PROVINCIA)) = '"+vprovincia+"'"
				+ " AND TRIM(UPPER(AGRUPACION)) like '%"+vagrupacion+"%'";
		
		if(vnombvia!="")
		{
			sql_stmt = sql_stmt + " AND TRIM(UPPER(NOMBRE_VIA)) = '" + vnombvia + "'";
		}
		if(vtipovia!="")
		{
			sql_stmt = sql_stmt + " AND TRIM(UPPER(TIPO_VIA)) = '" + vtipovia + "'";
		}		
		
		vrutalect = jdbcTemplatePostgres.queryForObject(sql_stmt, Long.class);
		
		return vrutalect;
		}
		catch(Exception ex)
		{
			return 0L;
		}
	}
	
	public void registrarCaeTpmCliente(String estadoProceso, String estado_activacion, Long numOrdConex, Long numeroCuenta,
			Long idAutoActiva, Long numOrdVenta, String resultadoFuncion) {
		
		int flagdatacaetmpclie4j = 0;
		String busquedaCaeTmpCliente4j = "vacio. Detalle: "+ resultadoFuncion;
		
		//TRAEMOS DATOS DE CAE_TMP_CLIENTE DE 4J (CaeTmpClienteDTO, CaeTmpClienteMapper)
		String sqlCaeTmpCliente4j = CaeTmpClienteMapper.SQLORACLE_SELECT_FOR_CAETMPCLIENTE4J;
		CaeTmpClienteDTO caetmpcliente4j = new CaeTmpClienteDTO();
		
		//asigacion de ejemplo para prueba
		//numeroCuenta = 3102516L;
		
		try {
			caetmpcliente4j = jdbcTemplateOracle.queryForObject(sqlCaeTmpCliente4j, new CaeTmpClienteMapper(), numeroCuenta); 
			busquedaCaeTmpCliente4j = "si se hallo registro. Detalle: "+ resultadoFuncion;
		} catch (EmptyResultDataAccessException e) {
			log.error("No hay datos para query sqlCaeTmpCliente4j, excepcion -> " + e);
			Log.logError2("No hay datos para query sqlCaeTmpCliente4j, excepcion -> " + e);
			flagdatacaetmpclie4j = 1;
			busquedaCaeTmpCliente4j = "no se encontro registro. Detalle: "+ resultadoFuncion;
		} catch (Exception ee) {
			log.error("Error sqlCaeTmpCliente4j excepcion -> " + ee);
			Log.logError2("Error sqlCaeTmpCliente4j excepcion -> " + ee);
			flagdatacaetmpclie4j = 1;
			busquedaCaeTmpCliente4j = "no se encontro registro. Detalle: "+ resultadoFuncion;
		}
		
		//--- Inicio SCOM ---	
		
		String estadoSuministro = "";
		String fechaActivacion = "";
		
		//validacion de si existe registro en caetmpcliente 4j
		if(flagdatacaetmpclie4j == 0) {

			// validamos que el servicio halla dado diferente de ERROR(pendientePcr) para INSERTAR en CAETMPCLIENTE
			if(estadoProceso.equals("PendientePcr")) {
				
				//validacion si existe en registro en CAETMPCLIENTE SCOM
				String sqlcountCaeTmpClieScom = "select count(*) cant\r\n"
						+ "from schscom.CAE_TMP_CLIENTE\r\n"
						+ "where NUMERO_CLIENTE = ?";
				
				Integer countCaeTmpClieScom = 0;
				
				//NUMERO CUENTA DE PRUEBA forzar caso else
				//numeroCuenta = 3103044L;
				try {
					countCaeTmpClieScom = jdbcTemplatePostgres.queryForObject(sqlcountCaeTmpClieScom, Integer.class, numeroCuenta); 
				} catch (EmptyResultDataAccessException e) {
					log.error("No hay datos para query sqlcountCaeTmpClieScom, excepcion -> " + e);
					flagdatacaetmpclie4j = 1;
				} catch (Exception ee) {
					log.error("Error sqlcountCaeTmpClieScom excepcion -> " + ee);
					flagdatacaetmpclie4j = 1;
				}
				
				
				/*
				if(countCaeTmpClieScom > 0) {
					//actualizacion en tabla CAE_TMP_CLIENTE
					try {
					String sqlUpdCaeTmpCliente = "UPDATE schscom.CAE_TMP_CLIENTE "
							+ " SET ID_CAE_TMP_CLIENTE = ?, \r\n"
							+ " TARIFA = ?, SUCURSAL = ?, DIRECCION = ?, COMUNA = ?, \r\n"
							+ " SECTOR = ?, ZONA = ?, CORR_RUTA = ?,\r\n"
							+ " COD_GIRO = ?, TIPO_CLIENTE = ?, \r\n"
							+ " ESTADO_CLIENTE = ?, ESTADO_SUMINISTRO = ?, FECHA_ACTIVACION = ?, \r\n"
							+ " DV_NUMERO_CLIENTE = ?, INFO_ADIC_LECTURA = ?, \r\n"
							+ " POTENCIA_CONT_FP = ?, POTENCIA_INST_FP = ?, POTENCIA_INST_HP = ?,\r\n"
							+ " ID_PCR = ?, PCR = ?, LLAVE = ?, SUBESTAC_DISTRIB = ?,\r\n"
							+ " ALIMENTADOR = ?, SUBESTAC_TRANSMI = ?,\r\n"
							+ "	NOMBRE = ?, TELEFONO = ?, RUT = ?, TIPO_IDENT = ? \r\n"
							+ " WHERE NUMERO_CLIENTE = ?";
					
					jdbcTemplatePostgres.update(sqlUpdCaeTmpCliente, 
							caetmpcliente4j.getID_CAE_TMP_CLIENTE(),
							caetmpcliente4j.getTARIFA(), caetmpcliente4j.getSUCURSAL(), caetmpcliente4j.getDIRECCION(), caetmpcliente4j.getCOMUNA(),
							caetmpcliente4j.getSECTOR(), caetmpcliente4j.getZONA(), caetmpcliente4j.getCORR_RUTA(),
							caetmpcliente4j.getCOD_GIRO(), caetmpcliente4j.getTIPO_CLIENTE(),
							caetmpcliente4j.getESTADO_CLIENTE(), caetmpcliente4j.getESTADO_SUMINISTRO(), caetmpcliente4j.getFECHA_ACTIVACION(),
							caetmpcliente4j.getDV_NUMERO_CLIENTE(), caetmpcliente4j.getINFO_ADIC_LECTURA(),
							caetmpcliente4j.getPOTENCIA_CONT_FP(), caetmpcliente4j.getPOTENCIA_INST_FP(), caetmpcliente4j.getPOTENCIA_INST_HP(),
							caetmpcliente4j.getID_PCR(), caetmpcliente4j.getPCR(), caetmpcliente4j.getLLAVE(), caetmpcliente4j.getSUBESTAC_DISTRIB(),
							caetmpcliente4j.getALIMENTADOR(),caetmpcliente4j.getSUBESTAC_TRANSMI(),
							caetmpcliente4j.getNOMBRE(), caetmpcliente4j.getTELEFONO(), caetmpcliente4j.getRUT(), caetmpcliente4j.getTIPO_IDENT(),
							numeroCuenta);
					} catch (Exception e) {
						System.out.println("Error actualizacion sqlUpdCaeTmpCliente -> " + e );
						Log.logError2("Error actualizacion sqlUpdCaeTmpCliente -> " + e );
					}
					
				} else {
					try {
					// insercion en tabla DE CAE_TMP_CLIENTE
					String sqlInsCaeTmpCliente = "	 INSERT INTO schscom.CAE_TMP_CLIENTE (ID_CAE_TMP_CLIENTE, NUMERO_CLIENTE, TARIFA, SUCURSAL, DIRECCION, COMUNA, SECTOR, ZONA, \r\n"
							+ "          CORR_RUTA, COD_GIRO, TIPO_CLIENTE, ESTADO_CLIENTE, ESTADO_SUMINISTRO, FECHA_ACTIVACION, DV_NUMERO_CLIENTE, INFO_ADIC_LECTURA,\r\n"
							+ "          POTENCIA_CONT_FP, POTENCIA_INST_FP, POTENCIA_INST_HP,\r\n"
							+ "          ID_PCR, PCR, LLAVE, SUBESTAC_DISTRIB, ALIMENTADOR, SUBESTAC_TRANSMI,\r\n"
							+ "          TIPO_ACCION, ESTADO_PROCESO, NRO_ENVIOS,\r\n"
							+ "			NOMBRE, TELEFONO, RUT, TIPO_IDENT,"
							+ "	 FECHA_RETIRO, MOTIVO_RECHAZO)\r\n"
							+ "  VALUES(?, ?, ?, ?, ?, ?, ?, ?, \r\n"
							+ "     ?, ?, ?, ?, ?, ?, ?, ?,\r\n"
							+ "     ?, ?, ?,\r\n"
							+ "     ?, ?, ?, ?, ?, ?, \r\n"
							+ "     ?, ?, ?, \r\n"
							+ "		?, ?, ?, ?,"
							+ "		?, ?)";

					jdbcTemplatePostgres.update(sqlInsCaeTmpCliente,caetmpcliente4j.getID_CAE_TMP_CLIENTE(), caetmpcliente4j.getNUMERO_CLIENTE(),caetmpcliente4j.getTARIFA(),caetmpcliente4j.getSUCURSAL(),
							caetmpcliente4j.getDIRECCION(), caetmpcliente4j.getCOMUNA(), caetmpcliente4j.getSECTOR(), caetmpcliente4j.getZONA(),
							caetmpcliente4j.getCORR_RUTA(), caetmpcliente4j.getCOD_GIRO(), caetmpcliente4j.getTIPO_CLIENTE(), caetmpcliente4j.getESTADO_CLIENTE(), 
							caetmpcliente4j.getESTADO_SUMINISTRO(), caetmpcliente4j.getFECHA_ACTIVACION(), caetmpcliente4j.getDV_NUMERO_CLIENTE(), caetmpcliente4j.getINFO_ADIC_LECTURA(),
							caetmpcliente4j.getPOTENCIA_CONT_FP(), caetmpcliente4j.getPOTENCIA_INST_FP(), caetmpcliente4j.getPOTENCIA_INST_HP(),
							caetmpcliente4j.getID_PCR(), caetmpcliente4j.getPCR(), caetmpcliente4j.getLLAVE(), caetmpcliente4j.getSUBESTAC_DISTRIB(), caetmpcliente4j.getALIMENTADOR(), 
							caetmpcliente4j.getSUBESTAC_TRANSMI(), caetmpcliente4j.getTIPO_ACCION(), "PendientePcr", caetmpcliente4j.getNRO_ENVIOS(),
							caetmpcliente4j.getNOMBRE(), caetmpcliente4j.getTELEFONO(), caetmpcliente4j.getRUT(), caetmpcliente4j.getTIPO_IDENT(),
							null, null);
					
					} catch (Exception e) {
						System.out.println("Error insercion sqlInsCaeTmpCliente -> " + e );
						Log.logError2("Error insercion sqlInsCaeTmpCliente -> " + e );
					}
				}
				*/
			}
			
			estadoSuministro = caetmpcliente4j.getESTADO_SUMINISTRO();
			fechaActivacion = caetmpcliente4j.getFECHA_ACTIVACION();

		} else {
			log.error("Error , no se pudo registrar en CaeTmpCliente , excepcion -> no data para sqlCaeTmpCliente4j");
			
			String queryalt1 = CaeTmpClienteAlterMapper.SQLORACLE_SELECT_FOR_CAETMPCLIENTEALTER;
			CaeTmpClienteAlterDTO caetmpclieAlter = new CaeTmpClienteAlterDTO();
			
			try {
				caetmpclieAlter = jdbcTemplateOracle.queryForObject(queryalt1, new CaeTmpClienteAlterMapper(), numeroCuenta); 
			} catch (EmptyResultDataAccessException e) {
				log.error("No hay datos para query queryalt1, excepcion -> " + e);
				Log.logError2("No hay datos para query queryalt1, excepcion -> " + e);
				caetmpclieAlter.setID_STATE("");
				caetmpclieAlter.setFEC_ACTIVACION("");
			} catch (Exception ee) {
				log.error("Error queryalt1 excepcion -> " + ee);
				Log.logError2("Error queryalt1 excepcion -> " + ee);
				caetmpclieAlter.setID_STATE("");
				caetmpclieAlter.setFEC_ACTIVACION("");
			}
			
			String VESTADO = caetmpclieAlter.getID_STATE();
			
			if(VESTADO.equals("Habilitado"))
				estadoSuministro="0";
			else if(VESTADO.equals("NotificadoCorte"))
				estadoSuministro="0";
			else if(VESTADO.equals("NotificadoVencimiento"))
				estadoSuministro="0";
			else if(VESTADO.equals("NotificadoDesmantelamiento"))
				estadoSuministro="1";
			else if(VESTADO.equals("SujetoCorte"))
				estadoSuministro="0";
			else if(VESTADO.equals("SujetoCorteIndividual"))
				estadoSuministro="0";
			else if(VESTADO.equals("SujetoVerificacion"))
				estadoSuministro="1";
			else if(VESTADO.equals("SujetoReposicion"))
				estadoSuministro="1";
			else if(VESTADO.equals("SujetoDesmantelamiento"))
				estadoSuministro="1";
			else if(VESTADO.equals("Deshabilitado"))
				estadoSuministro="1";  

			fechaActivacion = caetmpclieAlter.getFEC_ACTIVACION();
			
		}

		//fechaActivacion en duro para prueba 
//		fechaActivacion = "09/02/2022 14:55:28";
		//adaptacion de fechaactivacion
		// ejemplo de fecha de activacion desde CAE_TMP_CLIENTE 4J : 09/02/2022 14:55:28
		LocalDateTime fechaActivacionDate = null;
		String fecActivacion="";
		if(!fechaActivacion.equals("")) {
//			String fechaCadena = "09/02/2022 14:55:28";
			DateTimeFormatter formateador = new DateTimeFormatterBuilder().parseCaseInsensitive().append(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")).toFormatter();
//			LocalDateTime fecha = LocalDateTime.parse(fechaActivacion, formateador);
			fechaActivacionDate = LocalDateTime.parse(fechaActivacion, formateador);
			log.info("La fecha de activacion(fechaActivacionDate) es : " + fechaActivacionDate);
		}
		long countActivacion=0L;
		try {
			String sqlCountActivacion="Select count(1) from activacion where numero_cuenta=? and numero_orden_conexion=? and numero_orden_venta=?";
			countActivacion=jdbcTemplatePostgres.queryForObject(sqlCountActivacion,Long.class,numeroCuenta,numOrdConex.toString(),numOrdVenta);			
		}
		catch(Exception ex)
		{
			countActivacion=0L;
		}
		
		if(countActivacion==0)
		{
			try
			{
				String sqlInsActivacion;				
				if(fechaActivacion.equals(""))
				{
					sqlInsActivacion = "insert into activacion (numero_cuenta, numero_orden_conexion, estado_suministro, fecha_activacion, estado_proceso, estado_activacion, id_auto_act, numero_orden_venta, resultado_caetmpcliente_4j) \r\n"
							+ "	values(?, ?, ?, null, ?, ?, ?, ?, ?)";
					jdbcTemplatePostgres.update(sqlInsActivacion, numeroCuenta, numOrdConex.toString(), estadoSuministro,
							 estadoProceso, estado_activacion, idAutoActiva, numOrdVenta, busquedaCaeTmpCliente4j);
				}
				else
				{
				//INSERCION EN TABLA ACTIVACION 
				sqlInsActivacion = "insert into activacion (numero_cuenta, numero_orden_conexion, estado_suministro, fecha_activacion, estado_proceso, estado_activacion, id_auto_act, numero_orden_venta, resultado_caetmpcliente_4j) \r\n"
						+ "	values(?, ?, ?, to_timestamp(? ,'DD/MM/YYYY hh24:mi:ss'), ?, ?, ?, ?, ?)";
				/*
				jdbcTemplatePostgres.update(sqlInsActivacion, numeroCuenta, numOrdConex, caetmpcliente4j.getESTADO_SUMINISTRO(),
						caetmpcliente4j.getFECHA_ACTIVACION(), estadoProceso, estado_activacion, idAutoActiva, numOrdVenta, busquedaCaeTmpCliente4j);
				*/
				jdbcTemplatePostgres.update(sqlInsActivacion, numeroCuenta, numOrdConex.toString(), estadoSuministro,
						fechaActivacion, estadoProceso, estado_activacion, idAutoActiva, numOrdVenta, busquedaCaeTmpCliente4j);
				}
			
			} catch (Exception e) {
				log.error("Error insercion sqlInsActivacion -> " + e );
				log.error(" Valores de insercion -> numeroCuenta: " + numeroCuenta + " numOrdConex:"  + numOrdConex.toString() + " estadoSuministro:"  + estadoSuministro + " fechaActivacion:"  +
						fechaActivacionDate + " estadoProceso:"  + estadoProceso + " estado_activacion:"  + estado_activacion + " idAutoActiva:"  + idAutoActiva + " numOrdVenta:"  + numOrdVenta + " busquedaCaeTmpCliente4j:"  + busquedaCaeTmpCliente4j );
				Log.logError2("Error insercion sqlInsActivacion -> " + e );
				Log.logError2(" Valores de insercion -> numeroCuenta: " + numeroCuenta + " numOrdConex:"  + numOrdConex.toString() + " estadoSuministro:"  + estadoSuministro + " fechaActivacion:"  +
						fechaActivacionDate + " estadoProceso:"  + estadoProceso + " estado_activacion:"  + estado_activacion + " idAutoActiva:"  + idAutoActiva + " numOrdVenta:"  + numOrdVenta + " busquedaCaeTmpCliente4j:"  + busquedaCaeTmpCliente4j );
				
			}
		}
		else
		{
			//update en tabla activacion
			String sqlUpdateActivacion;
			
			if(fechaActivacion.equals(""))
			{
				sqlUpdateActivacion="update activacion set estado_suministro=?, estado_proceso=?, estado_activacion=?,id_auto_act=?, resultado_caetmpcliente_4j=?,fecha_activacion=null where numero_cuenta=? and numero_orden_conexion=? and numero_orden_venta=? ";
				
				try
				{
					jdbcTemplatePostgres.update(sqlUpdateActivacion, estadoSuministro, estadoProceso, estado_activacion,idAutoActiva,
						busquedaCaeTmpCliente4j, numeroCuenta, numOrdConex.toString(), numOrdVenta);
				}
				catch(EmptyResultDataAccessException ex)
				{					
					log.error("sqlUpdateActivacion: "+ sqlUpdateActivacion+", estadoSuministro:" +estadoSuministro+ ", estadoProceso: "+ estadoProceso+ ", estado_activacion: "+ estado_activacion+ ", idAutoActiva: "+ idAutoActiva+", busquedaCaeTmpCliente4j: "+busquedaCaeTmpCliente4j+ ", numeroCuenta: "+ numeroCuenta+", numOrdConex: "+ numOrdConex.toString()+", numOrdVenta: "+numOrdVenta);
					log.error("ex: "+ ex.getMessage());
					Log.logError2("sqlUpdateActivacion: "+ sqlUpdateActivacion+", estadoSuministro:" +estadoSuministro+ ", estadoProceso: "+ estadoProceso+ ", estado_activacion: "+ estado_activacion+ ", idAutoActiva: "+ idAutoActiva+", busquedaCaeTmpCliente4j: "+busquedaCaeTmpCliente4j+ ", numeroCuenta: "+ numeroCuenta+", numOrdConex: "+ numOrdConex.toString()+", numOrdVenta: "+numOrdVenta);
					Log.logError2("EmptyResultDataAccessException: "+ ex.getMessage());
				}
				catch(Exception ex)
				{
					log.error("sqlUpdateActivacion: "+ sqlUpdateActivacion+", estadoSuministro:" +estadoSuministro+ ", estadoProceso: "+ estadoProceso+ ", estado_activacion: "+ estado_activacion+ ", idAutoActiva: "+ idAutoActiva+", busquedaCaeTmpCliente4j: "+busquedaCaeTmpCliente4j+ ", numeroCuenta: "+ numeroCuenta+", numOrdConex: "+ numOrdConex.toString()+", numOrdVenta: "+numOrdVenta);
					log.error("ex: "+ ex.getMessage());
					Log.logError2("sqlUpdateActivacion: "+ sqlUpdateActivacion+", estadoSuministro:" +estadoSuministro+ ", estadoProceso: "+ estadoProceso+ ", estado_activacion: "+ estado_activacion+ ", idAutoActiva: "+ idAutoActiva+", busquedaCaeTmpCliente4j: "+busquedaCaeTmpCliente4j+ ", numeroCuenta: "+ numeroCuenta+", numOrdConex: "+ numOrdConex.toString()+", numOrdVenta: "+numOrdVenta);
					Log.logError2("Exception: "+ ex.getMessage());
				}
			}
			else
			{
				sqlUpdateActivacion="update activacion set estado_suministro=?,estado_proceso=?,estado_activacion=?,id_auto_act=?, resultado_caetmpcliente_4j=?,fecha_activacion=to_timestamp(? ,'DD/MM/YYYY hh24:mi:ss') where numero_cuenta=? and numero_orden_conexion=? and numero_orden_venta=? ";
				
				try
				{
				jdbcTemplatePostgres.update(sqlUpdateActivacion, estadoSuministro, estadoProceso, estado_activacion,idAutoActiva,
						busquedaCaeTmpCliente4j, fechaActivacion, numeroCuenta, numOrdConex.toString(), numOrdVenta);
				}
				catch(EmptyResultDataAccessException ex)
				{					
					log.error("sqlUpdateActivacion: "+ sqlUpdateActivacion+", estadoSuministro:" +estadoSuministro+ ", estadoProceso: "+ estadoProceso+ ", estado_activacion: "+ estado_activacion+ ", idAutoActiva: "+ idAutoActiva+", busquedaCaeTmpCliente4j: "+busquedaCaeTmpCliente4j+ ", numeroCuenta: "+ numeroCuenta+", numOrdConex: "+ numOrdConex.toString()+", numOrdVenta: "+numOrdVenta);
					log.error("ex: "+ ex.getMessage());
					Log.logError2("sqlUpdateActivacion: "+ sqlUpdateActivacion+", estadoSuministro:" +estadoSuministro+ ", estadoProceso: "+ estadoProceso+ ", estado_activacion: "+ estado_activacion+ ", idAutoActiva: "+ idAutoActiva+", busquedaCaeTmpCliente4j: "+busquedaCaeTmpCliente4j+ ",fechaActivacion: "+ fechaActivacion +", numeroCuenta: "+ numeroCuenta+", numOrdConex: "+ numOrdConex.toString()+", numOrdVenta: "+numOrdVenta);
					Log.logError2("EmptyResultDataAccessException: "+ ex.getMessage());
				}
				catch(Exception ex)
				{
					log.error("sqlUpdateActivacion: "+ sqlUpdateActivacion+", estadoSuministro:" +estadoSuministro+ ", estadoProceso: "+ estadoProceso+ ", estado_activacion: "+ estado_activacion+ ", idAutoActiva: "+ idAutoActiva+", busquedaCaeTmpCliente4j: "+busquedaCaeTmpCliente4j+ ", numeroCuenta: "+ numeroCuenta+", numOrdConex: "+ numOrdConex.toString()+", numOrdVenta: "+numOrdVenta);
					log.error("ex: "+ ex.getMessage());
					Log.logError2("sqlUpdateActivacion: "+ sqlUpdateActivacion+", estadoSuministro:" +estadoSuministro+ ", estadoProceso: "+ estadoProceso+ ", estado_activacion: "+ estado_activacion+ ", idAutoActiva: "+ idAutoActiva+", busquedaCaeTmpCliente4j: "+busquedaCaeTmpCliente4j+ ",fechaActivacion: "+ fechaActivacion +", numeroCuenta: "+ numeroCuenta+", numOrdConex: "+ numOrdConex.toString()+", numOrdVenta: "+numOrdVenta);
					Log.logError2("Exception: "+ ex.getMessage());
				}
			}
		}
	}
	
	public boolean homologacionDe4JaScom(Long numOrdenVta) {
		log.info("inicio homologacionDe4JaScom");
		
		boolean homologacionexistosa = true;
		int control1 = 0;
		int control2 = 0;
		int control3 = 0;
		int control4 = 0;
		int control5 = 0;
		int control6 = 0;
		
		String numOrdenVtaString = String.valueOf(numOrdenVta);
		
		//obtenemos datos de medidor
		String sqlObtenerdatosMedidor = MedidorNumMarModMapper.SQLPOSTGRESQL_SELECT_FOR_MEDIDOR;
		MedidorNumMarModMapper medidorNumMarModMapper = new MedidorNumMarModMapper();
		List<MedidorNumMarModDTO> listaMedidorNumMarModDTO = new ArrayList<MedidorNumMarModDTO>();
		try {
			listaMedidorNumMarModDTO = jdbcTemplatePostgres.query(sqlObtenerdatosMedidor, medidorNumMarModMapper, numOrdenVtaString) ;
		} catch (EmptyResultDataAccessException ex) {
			log.error("Error EmptyResultDataAccessException en query sqlObtenerdatosMedidor -> "+  ex);
			return false;
		} catch (Exception e) {
			log.error("Error en query sqlObtenerdatosMedidor -> "+  e);
			return false;
		}
		log.info("cantidad de datos medidor obtenidos : " + listaMedidorNumMarModDTO.size());
		for(int i=0; i<listaMedidorNumMarModDTO.size(); i++) {
			log.info("Datos medidor numero " + (i+1) + " \n");
			log.info("accionMedidor : " + listaMedidorNumMarModDTO.get(i).getAccionmedidor());
			log.info("numMedidor : " + listaMedidorNumMarModDTO.get(i).getNummedidor());
			log.info("numMarca : " + listaMedidorNumMarModDTO.get(i).getMarmedidor());
			log.info("numModelo : " + listaMedidorNumMarModDTO.get(i).getModmedidor());
			log.info("fecha_Conexion_Red : " + listaMedidorNumMarModDTO.get(i).getFechaconexion());
		}
		
		log.info("inicio RECORRIDO FOR listaMedidorNumMarModDTO");
		for (MedidorNumMarModDTO medidorNumMarModDTO : listaMedidorNumMarModDTO) {
			String accionMedidor = medidorNumMarModDTO.getAccionmedidor();
			String fecha_Conexion_Red = medidorNumMarModDTO.getFechaconexion();
			String numMedidor = medidorNumMarModDTO.getNummedidor();
			String numMarca = medidorNumMarModDTO.getMarmedidor();
			String numModelo = medidorNumMarModDTO.getModmedidor();
			
			log.info("AccionMedidor actual :" + accionMedidor);
			
			//obtenemos id_componente
			String sqlObtenerIdcomponente = "select  ID_COMPONENTE \r\n"
					+ " from med_componente mc , med_marca mm ,med_modelo mm2 \r\n"
					+ " where 1=1\r\n"
					+ " and mc.id_modelo = mm2.ID_MODELO \r\n"
					+ " and mm2.id_marca = mm.ID_MARCA \r\n"
					+ " and mc.nro_componente = ? "
					+ " and mm2.cod_modelo = ? "
					+ " and mm.cod_marca = ?";
			
			Long idComponente;
			try {
				idComponente = jdbcTemplateOracle.queryForObject(sqlObtenerIdcomponente, Long.class, numMedidor, numModelo, numMarca);
				log.info("idComponente obtenido : " + idComponente);
			} catch (Exception e) {
				idComponente = null;
				log.error("Error al obtener el idComponente , query sqlObtenerIdcomponente -> " + e);
			}
			
			if(idComponente == null) {
				log.error("No se pudo obtener el idComponente ");
				return false;
			}
			
			//INICIO HOMOLOGACIONES
			//select *  from fwk_auditevent fa where fa.id_fk =10111111980 // ID_COMPONENTE  -> INSERT
			log.info("START HOMOLOGACION fwk_auditevent");
			String sqlFwfAuditEvent = FwkAuditEventMapper.SQLORACLESQL_SELECT_FOR_FWK_AUDITEVENT;
			FwkAuditEventMapper fwkAuditEventMapper = new FwkAuditEventMapper();
			FwkAuditEventDTO fwkAuditEventDTO = new FwkAuditEventDTO();
			try {
				fwkAuditEventDTO = jdbcTemplateOracle.queryForObject(sqlFwfAuditEvent,fwkAuditEventMapper, idComponente);
				log.info("obtenido de oracle fwkAuditEventDTO : " + fwkAuditEventDTO);
			} catch (Exception e) {
				log.error("No se pudo Homologar tabla fwk_auditevent");
				log.error("Error en query sqlFwfAuditEvent -> " + e);
				control1 = 1;
			}
			if(control1 == 0) {
				String sqlInsFwfAuditEvent = FwkAuditEventMapper.SQLPOSTGREEQL_INSERT_FOR_FWK_AUDITEVENT;
				try {
					jdbcTemplatePostgres.update(sqlInsFwfAuditEvent, fwkAuditEventDTO.getId(), fwkAuditEventDTO.getUsecase(),
							fwkAuditEventDTO.getObjectref(), fwkAuditEventDTO.getId_fk(), fwkAuditEventDTO.getFecha_ejecucion(),
							fwkAuditEventDTO.getSpecific_auditevent(), fwkAuditEventDTO.getId_user());
					log.info("Se inserto correctamente en tabla fwk_auditevent , id_fk: " + fwkAuditEventDTO.getId());
				} catch (Exception e) {
					log.error("No se pudo insertar en tabla fwk_auditevent -> " + e);
				}
			}
			
			//SELECT * FROM schscom.med_his_componente where id_componente = 10111111980  -> INSERT Y UPDATE
			log.info("START HOMOLOGACION schscom.med_his_componente");
			String sqlMedHisComponenteORCLfecDESDE =  MedHisComponenteMapper.SQLORACLESQL_SELECT_FOR_MED_HIS_COMPONENTE_FEC_DESDE;
			String sqlMedHisComponenteORCLfecHASTA =  MedHisComponenteMapper.SQLORACLESQL_SELECT_FOR_MED_HIS_COMPONENTE_FEC_HASTA;
			MedHisComponenteMapper medHisComponenteMapper = new MedHisComponenteMapper();
			MedHisComponenteDTO ORCLMedHisCompDesde = new MedHisComponenteDTO();
			MedHisComponenteDTO ORCLMedHisCompHasta = new  MedHisComponenteDTO();
			try {
				ORCLMedHisCompDesde = jdbcTemplateOracle.queryForObject(sqlMedHisComponenteORCLfecDESDE, medHisComponenteMapper, idComponente, fecha_Conexion_Red);
				log.info("ORCLMedHisCompDesde obtenido " + ORCLMedHisCompDesde);
			} catch(Exception e) {
				log.error("No se pudo Homologar tabla med_his_componente");
				log.error("Error en query sql sqlMedHisComponenteORCLfecDESDE -> " + e);
				control2 = 1;
			}
			try {
				ORCLMedHisCompHasta = jdbcTemplateOracle.queryForObject(sqlMedHisComponenteORCLfecHASTA, medHisComponenteMapper, idComponente, fecha_Conexion_Red);
				log.info("ORCLMedHisCompHasta obtenido " + ORCLMedHisCompHasta);
			} catch(Exception e) {
				log.error("No se pudo Homologar tabla med_his_componente");
				log.error("Error en query sql sqlMedHisComponenteORCLfecHASTA -> " + e);
				control4 = 1;
			}
			if(control2 == 0) {
				String sqlInsMedHisComp = MedHisComponenteMapper.SQLPOSTGREEQL_INSERT_FOR_MED_HIS_COMPONENTE;
				try {
					
					jdbcTemplatePostgres.update(sqlInsMedHisComp,ORCLMedHisCompDesde.getId_his_componente(), ORCLMedHisCompDesde.getId_componente(), 
							ORCLMedHisCompDesde.getId_est_componente(), ORCLMedHisCompDesde.getFec_desde(), ORCLMedHisCompDesde.getFec_hasta(),
							ORCLMedHisCompDesde.getId_ubicacion(), ORCLMedHisCompDesde.getType_ubicacion(), ORCLMedHisCompDesde.getId_orden());
					
					log.info("sqlInsMedHisComp ejecutado");
				} catch (Exception e) {
					log.error("Error al insertar tabla med_his_componenete , query sqlInsMedHisComp -> " + e);
				}
				
			}
			if(control4 == 0) {
				String sqlUpdMedHisComp = MedHisComponenteMapper.SQLPOSTGREEQL_UPDATE_FOR_MED_HIS_COMPONENTE;
				try {
					jdbcTemplatePostgres.update(sqlUpdMedHisComp, ORCLMedHisCompHasta.getId_est_componente(), ORCLMedHisCompHasta.getFec_desde(),
							ORCLMedHisCompHasta.getFec_hasta(), ORCLMedHisCompHasta.getId_ubicacion(), ORCLMedHisCompHasta.getType_ubicacion(),
							ORCLMedHisCompHasta.getId_orden(),
							ORCLMedHisCompHasta.getId_componente(), ORCLMedHisCompHasta.getId_his_componente());
					log.info("sqlUpdMedHisComp ejecutado");
				} catch (Exception e) {
					log.error("Error al actualizar tabla med_his_componenete , query sqlUpdMedHisComp -> " + e);
				}
			}
			
			
			//select * from schscom.med_componente where id =10111111980 ->// ID_COMPONENTE -> UPDATE
	
			log.info("START HOMOLOGACION schscom.med_componente");
			String sqlMedComponente4j = MedComponenteMapper.SQLORACLESQL_SELECT_FOR_MED_COMPONENTE;
			MedComponenteMapper medComponenteMapper = new MedComponenteMapper();
			MedComponenteDTO medComponenteDTO = new MedComponenteDTO();
			try {
				medComponenteDTO = jdbcTemplateOracle.queryForObject(sqlMedComponente4j, medComponenteMapper, idComponente);
				log.info("medComponenteDTO obtenido " + medComponenteDTO);
			} catch(Exception e) {
				log.error("No se pudo Homologar tabla med_componente");
				log.error("Error en query sqlMedComponente4j -> " + e);
				control3 = 1;
			}
			if(control3 == 0) {
				String sqlUpdmedComponente = MedComponenteMapper.SQLPOSTGREEQL_UPDATE_FOR_MED_COMPONENTE;
				try {
					jdbcTemplatePostgres.update(sqlUpdmedComponente, medComponenteDTO.getId_modelo(), medComponenteDTO.getId_est_componente(),
							medComponenteDTO.getFec_creacion(), medComponenteDTO.getId_propiedad(), medComponenteDTO.getAno_fabricacion(), 
							medComponenteDTO.getNro_fabrica(), medComponenteDTO.getId_accion_realizada(), medComponenteDTO.getId_cond_creacion(),
							medComponenteDTO.getEs_control_interno(),medComponenteDTO.getEs_prototipo(), medComponenteDTO.getId_tip_transfor(),
							medComponenteDTO.getCod_tip_componente(), medComponenteDTO.getId_equipo(), medComponenteDTO.getId_resp_medicion(),
							medComponenteDTO.getId_mod_medicion(), medComponenteDTO.getTel_medicion(), medComponenteDTO.getUbi_instalacion(),
							medComponenteDTO.getId_ubicacion(), medComponenteDTO.getType_ubicacion(), medComponenteDTO.getId_dynamicobject(),
							medComponenteDTO.getHora_reloj(), medComponenteDTO.getHora_verif(), medComponenteDTO.getId_relacion(),
							medComponenteDTO.getId_nev(), medComponenteDTO.getId_tip_medida(), medComponenteDTO.getNro_componente(),
							medComponenteDTO.getUbicacion_inst_medidor(), medComponenteDTO.getFecha_suministro(),
							medComponenteDTO.getSuministrador_componente(),	medComponenteDTO.getReacondicionado(), medComponenteDTO.getId_motivo_baja(),
							medComponenteDTO.getFactor_interno(), medComponenteDTO.getSerial_number(), 
							medComponenteDTO.getId_componente())
							;
					log.info("Se actualizo tabla med_componente correctamente");
				} catch (Exception e) {
					log.error("No se pudo actualizar tabla med_componente en SCOM -> " + e);
				}
			}
			
			//select * from dyo_object do2 where do2.id = 2628987 // EL ID ESTA EN LA TABLA MED_COMPONENTE 4J, ID_DYNAMIC_OBJECT , SI ESTA VACIO NO HAY DATO ASOCIADO -> INSERT
			log.info("START HOMOLOGACION dyo_object");
			if(control3 == 0) {
				Long idDynamicObject = medComponenteDTO.getId_dynamicobject();
				if((idDynamicObject==null?0L:idDynamicObject) != 0) {
					String sqlSelectDyoObjectPOSTGREE = "select count(1) as id\r\n"
							+ " from dyo_object\r\n"
							+ " WHERE ID = ?";
					Integer countidDynamicObjectPOSTGREE;
					try {
						countidDynamicObjectPOSTGREE = jdbcTemplatePostgres.queryForObject(sqlSelectDyoObjectPOSTGREE, Integer.class, idDynamicObject);
					} catch (Exception e) {
						countidDynamicObjectPOSTGREE = -1;
						log.error("hubo un error con el id de dyo_object en postgree -> " + e);
					}
					
					//si no existe el idDynmaicObjectPostgree se hace la insercion
					if(countidDynamicObjectPOSTGREE == 0) {
						String sqlInsdyoObject = "insert into dyo_object (id)\r\n"
								+ " values(?)";
						try {
							jdbcTemplatePostgres.update(sqlInsdyoObject, idDynamicObject);
							log.info("ejecutado sqlInsdyoObject");
						} catch (Exception e) {
							log.error("no se pudo actualizar la tabla dyo_object en scom " + e);
						}
					} else {
						log.info("el idDynamicObjectPOSTGREE ya existe en postgree , cant de registros existentes : "+ countidDynamicObjectPOSTGREE);
					}
					
				} else {
					log.error("no se pudo actualizar la tabla dyo_object, el valor idDynamicObject es null o 0 .");
				}
			} else {
				log.error("no existe valor idDynamicObject - medComponenteDTO.getId_dynamicobject()");
			}
			
			
			//Obtienes el registro , lo elimino y inserto el que se genero en 4j
			//select * from med_lec_ultima mlu where id_componente  = 10111111980 // ID_COMPONENTE
			log.info("START HOMOLOGACION med_lec_ultima");
			String sqlMedLecUltima = MedLecUltimaMapper.SQLORACLESQL_SELECT_FOR_MED_LEC_ULTIMA;
			MedLecUltimaMapper medLecUltimaMapper = new MedLecUltimaMapper();
			MedLecUltimaDTO medLecUltimaDTO = new MedLecUltimaDTO();
			try {
				medLecUltimaDTO = jdbcTemplateOracle.queryForObject(sqlMedLecUltima, medLecUltimaMapper, idComponente);
				log.info("medLecUltimaDTO obtenido " + medLecUltimaDTO);
			} catch(Exception e) {
				log.error("No se pudo Homologar tabla med_lec_ultima");
				log.error("Error en query sqlMedLecUltima -> " + e);
				control5 = 1;
			}
			
	
			if(control5 == 0) {
				String sqlContadorMedLecUltima = "select COUNT(1) \r\n"
						+ " FROM med_lec_ultima\r\n"
						+ " WHERE id_componente = ? ";
				Integer contadorMedLecUltima = 0;
				try {
					contadorMedLecUltima = jdbcTemplatePostgres.queryForObject(sqlContadorMedLecUltima, Integer.class, idComponente);
				} catch (Exception e) {
					log.error("Error en query sqlContadorMedLecUltima " + e);
					contadorMedLecUltima = 0;
				}
				
				//ENTRO -> SI ES QUE HAY REGISTRO
				if(contadorMedLecUltima != 0) {
					//BUSCA DATOS EN POSTGRE
					String sqlMedLecUltimaPOSTGREE = MedLecUltimaMapper.SQLPOSTGRESQL_SELECT_FOR_MED_LEC_ULTIMA;
					MedLecUltimaMapper medLecUltimaMapper2 = new MedLecUltimaMapper();
					MedLecUltimaDTO medLecUltimaDTO2 = new MedLecUltimaDTO();
					try {
						medLecUltimaDTO2 = jdbcTemplatePostgres.queryForObject(sqlMedLecUltimaPOSTGREE, medLecUltimaMapper2, idComponente);
						log.info("medLecUltimaDTO POSTGREE  obtenido " + medLecUltimaDTO2);
					} catch(Exception e) {
						log.error("No se pudo Homologar tabla med_lec_ultima");
						log.error("Error en query sqlMedLecUltimaPOSTGREE -> " + e);
						control6 = 1;
					}
					if(control6 == 0) {
						//ELIMINA LO QUE ENCONTRO DE POSTGRE
						String sqlDelMedLecUltima = MedLecUltimaMapper.SQLPOSTGREEQL_DELETE_FOR_MED_LEC_ULTIMA;
						try {
							jdbcTemplatePostgres.update(sqlDelMedLecUltima, medLecUltimaDTO2.getId_lectura(), 
									medLecUltimaDTO2.getId_componente(), medLecUltimaDTO2.getId_medida(), medLecUltimaDTO2.getId_empresa());
							log.info("se elimino registro con Id_lectura: " +medLecUltimaDTO2.getId_lectura()
							+ " Id_componente : " + medLecUltimaDTO2.getId_componente()
							+ " Id_medida : " + medLecUltimaDTO2.getId_medida()
							+ " Id_empresa : " + medLecUltimaDTO2.getId_empresa()
							+ " de la tabla med_lec_ultima");
						} catch (Exception e) {
							log.error("Error al eliminar registro de la tabla med_lec_ultima -> "+ e );
						}
					}
				}
	
				//siempre se inserta en med lec ultima
				String sqlInsMedLecUltima = MedLecUltimaMapper.SQLPOSTGREEQL_INSERT_FOR_MED_LEC_ULTIMA;
				try {
					jdbcTemplatePostgres.update(sqlInsMedLecUltima, medLecUltimaDTO.getId_lectura(), 
							medLecUltimaDTO.getId_componente(), medLecUltimaDTO.getId_medida(), medLecUltimaDTO.getId_empresa() );
					log.info("ejecutado sqlInsMedLecUltima");
				} catch (Exception e) {
					log.error("Error al insertar en tabla med_lec_ultima " + e);
				}
			}
		
		}
		log.info("FIN homologacionDe4JaScom");
		return homologacionexistosa;
	}
	
}




