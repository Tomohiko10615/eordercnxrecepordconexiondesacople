package com.enel.scom.api.eordercnxrecepordconexion.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.enel.scom.api.eordercnxrecepordconexion.autoactiva.Autoactiva;
import com.enel.scom.api.eordercnxrecepordconexion.dto.AuditoriaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.DatosOrdenDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.LecturasMedDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.MedidorInstallDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.MedidorNumMarModDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.OrdenVentaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.ParametrosDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.ProductoSrvVentaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.RegistroOrdenDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.SrvPtoEntregaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.TablaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.VtaAutoTablaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.AuditoriaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.CajaMedicionSrvVentaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.CentroOperativoMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.ContPropiedadMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.ContServiciosMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.DescripcionProductoMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.EnterosGlobalMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.InsAuditoriaVentaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.LecturasMedMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.MotivoMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.NroCuentaSrvVentaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.OrdServVentaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.OrdenVentaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.ProductoSrvVentaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.SrvPtoEntregaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.TablaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.TarifaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.UpdEorOrdTransferDetMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.UpdEorOrdTransferMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.UpdOrdenVentaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.VtaAutoTablaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.util.ArchivoUtil;
import com.enel.scom.api.eordercnxrecepordconexion.util.Constante;
import com.enel.scom.api.eordercnxrecepordconexion.util.Log;

import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.OracleTypes;

@Slf4j
@Component
public class ProcesoPrincipalDAO {

	@Autowired
	@Qualifier("jdbcTemplate1")
	private JdbcTemplate jdbcTemplateOracle;

	@Autowired
	@Qualifier("jdbcTemplate2")
	private JdbcTemplate jdbcTemplatePostgres;

	@Autowired
	AutoActivaDAO autoActivaDAO;

	@Autowired
	private Autoactiva autoactiva;

	int lCant_Reg_Cargados;
	String vCodOperacion;
	String vObservacion;
	String orden;

	String nroCuenta;
	String cajaMedicion;
	ProductoSrvVentaDTO producto = new ProductoSrvVentaDTO();
	String codTarifa;
	SrvPtoEntregaDTO descPtoEntrega = new SrvPtoEntregaDTO();
	VtaAutoTablaDTO vtaAutoTabla;
	String descripDTC2;
	String centroOperativo = "";
	String desProducto = "";

	Integer ReqInstalMed = 0;
	Integer ReqCambioMed = 0;
	Integer contMedInstall = 0;
	Integer contMedRet = 0;
	Integer contPropiedad = 0;

	// int CountMedInstall;
	// int CountMedRet;

	public List<RegistroOrdenDTO> LogicaControlProceso(List<DatosOrdenDTO> lista, ParametrosDTO parametros,
			Long nroOrden) {

		String Nro_PCR;
		String Tension;
		String Sum_PCR_Ref;
		String SETi1;
		String ALIMENTADOR;
		String SED;
		String LLAVE;
		String SECTOR;
		String ZONA;
		String CORRELATIVO;
		String Ubicacion;
		String Tipo_Conductor;
		String Capacidad_Interruptor;
		String Fecha_Ejecucion;
		String cAuxErr;

		String Propiedad_Empalme;
		String Sistema_Proteccion;
		String Caja_Toma;
		String Longitud_Acometida;

		String Sum_Derecho;
		String Sum_Izquierdo;
		int ltmpNumber;
		int curMedInstall;
		int curMedRet;
		int lCant_Reg_Leidos = 0;
		int contador1 = 0;

		ImprimeCabecera();
		// inicio: GetDatosAdicSrvVenta
		int Valido_OK = 0;
		List<RegistroOrdenDTO> listaRegistroOrden = new ArrayList<>();

		log.info("INICIO FOR PARA OBTENER LISTA ");
		for (DatosOrdenDTO datosOrden : lista) {
			contador1++;
			log.info("[CONTADOR1] iteracion : " + contador1);
			vCodOperacion = parametros.getCodErrSY000();
			vObservacion = "";

			String sqlDescripcionProducto = DescripcionProductoMapper.SQLORACLE_SELECT_FOR_DESPRODUCTO;
			String sqlCentroOperativo = CentroOperativoMapper.SQLORACLE_SELECT_FOR_CENTROOPERATIVO;
			String sqlLecturasMed = LecturasMedMapper.SQLPOSTGRESQL_SELECT_FOR_LECTURAMED;

			CentroOperativoMapper centroOperativoMapper = new CentroOperativoMapper();
			LecturasMedMapper lecturasMedMapper = new LecturasMedMapper();
			DescripcionProductoMapper descripcionProductoMapper = new DescripcionProductoMapper();

			if (Constante.debug == 1) {
				System.out.println(String.format("lista.vIdOrdTransfer = %d - lista.vNroRecepciones %d\n",
						datosOrden.getOrdenesTransfer().getIdOrdTransfer(),
						datosOrden.getOrdenesTransfer().getNroRecepciones()));
			}

			System.out.println(
					String.format("Orden de Conexion[%s] \n", datosOrden.getDatosProcesos().getCodigoExternoTdC()));

			if ((datosOrden.getDatosProcesos().getCodigoResultado() == null ? 1
					: datosOrden.getDatosProcesos().getCodigoResultado().compareTo("NRE")) == 0) {
				try {
					paralizarTransaccion(datosOrden, parametros);
				} catch (Exception ex) {

				}
				continue;
			}

			if (ValidaOrden(parametros, datosOrden)) /* 6.2.2. Valida los datos obtenidos en el paso anterior */
			{
				String descPtoEntrega = GetDescripPtoEntregaString(parametros, datosOrden);
				datosOrden.getActaConexion().setPuntoFisicoConexion(descPtoEntrega);

				String tipoConductor = GetDescripDTCString("TIPO_CONDUCTOR",
						datosOrden.getActaConexion().getTipoConductor());
				datosOrden.getActaConexion().setTipoConductor(tipoConductor);

				String capacidadInterruptor = GetDescripDTCString("CAP_INTERRUP",
						datosOrden.getActaConexion().getCapacidadInterruptor());
				datosOrden.getActaConexion().setCapacidadInterruptor(capacidadInterruptor);

				Valido_OK = 1;
				cAuxErr = "";
				Propiedad_Empalme = "CLIENTE";
				Sistema_Proteccion = "Interruptor Termomagnetico";
				Caja_Toma = " ";
				Longitud_Acometida = datosOrden.getActaConexion().getLongitudAcometida();

				Sum_Derecho = "";

				if (datosOrden.getActaConexion().getIdCodClienteVecinoDerecho() == null) {
					datosOrden.getActaConexion().setIdCodClienteVecinoDerecho("");
				}

				if (datosOrden.getActaConexion().getIdCodClienteVecinoDerecho().length() > 0) {
					try {
						ltmpNumber = Integer
								.parseInt((datosOrden.getActaConexion().getIdCodClienteVecinoDerecho()).trim());
						datosOrden.getActaConexion().setIdCodClienteVecinoDerecho(ltmpNumber + "");
						Sum_Derecho = datosOrden.getActaConexion().getIdCodClienteVecinoDerecho();
					} catch (Exception ex) {
						log.error("error con ltmpNumber Sum_Derecho, valor brindado "
								+ datosOrden.getActaConexion().getIdCodClienteVecinoIzq() + " Excepcion -> " + ex);
					}
				}

				Sum_Izquierdo = "";
				if (datosOrden.getActaConexion().getIdCodClienteVecinoIzq() == null) {
					datosOrden.getActaConexion().setIdCodClienteVecinoIzq("");
				}

				if (datosOrden.getActaConexion().getIdCodClienteVecinoIzq().length() > 0) {
					try {
						ltmpNumber = Integer.parseInt((datosOrden.getActaConexion().getIdCodClienteVecinoIzq()).trim());
						datosOrden.getActaConexion().setIdCodClienteVecinoIzq(ltmpNumber + "");
						Sum_Izquierdo = datosOrden.getActaConexion().getIdCodClienteVecinoIzq();
					} catch (Exception ex) {
						log.error("error con ltmpNumber Sum_Izquierdo, valor brindado "
								+ datosOrden.getActaConexion().getIdCodClienteVecinoIzq() + " Excepcion -> " + ex);
					}
				}

				Nro_PCR = "";
				Tension = datosOrden.getActaConexion().getCodTipoRed();
				Sum_PCR_Ref = "";
				SETi1 = datosOrden.getActaConexion().getSetr();
				ALIMENTADOR = datosOrden.getActaConexion().getAlimentador();
				SED = datosOrden.getActaConexion().getSed();
				LLAVE = datosOrden.getActaConexion().getLlave();
				SECTOR = datosOrden.getActaConexion().getSector();
				ZONA = datosOrden.getActaConexion().getZona();
				CORRELATIVO = datosOrden.getActaConexion().getCorrelativo();

				Ubicacion = datosOrden.getActaConexion().getPuntoFisicoConexion();
				Tipo_Conductor = datosOrden.getActaConexion().getTipoConductor();
				Capacidad_Interruptor = datosOrden.getActaConexion().getCapacidadInterruptor();
				Fecha_Ejecucion = "";
				try {
					Fecha_Ejecucion = ObtenerFechaDDMMYYYY(datosOrden.getDatosProcesos().getInicioTdC());
				} catch (Exception ex) {
					cAuxErr = String.format("Fecha de ejecucion de TDC no valida[%s] formato debe ser AAAA-MM-DD\n",
							Fecha_Ejecucion);
					RegErrorProcesoOrd(datosOrden, "", cAuxErr, parametros);
					// commit;
					continue;
				}

				if (datosOrden.getDatosProcesos().getInicioTdC() == null) {
					datosOrden.getDatosProcesos().setInicioTdC("");
				}

				if (datosOrden.getDatosProcesos().getInicioTdC().length() == 0) {
					cAuxErr = String.format("Fecha de ejecucion de TDC no valida[%s] formato debe ser AAAA-MM-DD\n",
							Fecha_Ejecucion);
					RegErrorProcesoOrd(datosOrden, "", cAuxErr, parametros);
					// commit;
					continue;
				}

				curMedInstall = 0;
				curMedRet = 0;

				// Inicializar arrays adicionales de medidores

				List<MedidorInstallDTO> listaMedInstall = new ArrayList<>();// ArrayLecturasMed
				List<MedidorInstallDTO> listaMedRet = new ArrayList<>();

				if (ReqInstalMed == 1 || ReqCambioMed == 1) {

					for (int ind = 0; ind < datosOrden.getListaMedidores().size(); ind++) {

						if (datosOrden.getListaMedidores().get(ind).getAccionMedidor().compareTo("Retirado") == 0) {
							continue;
						}

						// NroLecturasMed=0;

						/*
						 * List<LecturasMedDTO> listaLecturasMed =
						 * jdbcTemplatePostgres.query(sqlLecturasMed, lecturasMedMapper, i, i, i, i,
						 * datosOrden.getOrdenesTransfer().getIdOrdTransfer(),
						 * datosOrden.getOrdenesTransfer().getNroRecepciones());
						 */
						int lIndMed = ind + 1;
						Long auxidOrdTransfer = datosOrden.getOrdenesTransfer().getIdOrdTransfer();
						Long auxnroRecepciones = datosOrden.getOrdenesTransfer().getNroRecepciones();
						// GetLecturasMedidor

						List<LecturasMedDTO> listaLecturasMed = jdbcTemplatePostgres.query(sqlLecturasMed,
								lecturasMedMapper, lIndMed, auxidOrdTransfer, auxnroRecepciones);

						if (listaLecturasMed.isEmpty()) {
							break;
						}

						if (datosOrden.getListaMedidores().get(ind).getAccionMedidor().compareTo("Instalado") == 0) {
							curMedInstall++;
							MedidorInstallDTO medInstall = new MedidorInstallDTO();
							medInstall.setNumeroMedidor(datosOrden.getListaMedidores().get(ind).getNumeroMedidor());
							medInstall.setCodMarca(datosOrden.getListaMedidores().get(ind).getMarcaMedidor());
							medInstall.setCodModelo(datosOrden.getListaMedidores().get(ind).getModeloMedidor());
							medInstall.setNovedad("I");
							medInstall.setFactor(datosOrden.getListaMedidores().get(ind).getFactorMedidor());

							for (int ind2 = 0; ind2 < listaLecturasMed.size(); ind2++) {
								if (ind2 == 0) {
									try {
										medInstall.setFecLectTerreno(
												ObtenerFechaDDMMYYYY(listaLecturasMed.get(ind2).getFechaLectura()));
									} catch (Exception ex) {
										cAuxErr = String.format(
												"Fecha de lectura no valida[%s] formato debe ser AAAA-MM-DD\n",
												medInstall.getFecLectTerreno());
										break;
									}

									if (medInstall.getFecLectTerreno() == null) {
										medInstall.setFecLectTerreno("");
									}

									if (medInstall.getFecLectTerreno().length() == 0) {
										cAuxErr = String.format(
												"Fecha de lectura no valida[%s] formato debe ser AAAA-MM-DD\n",
												medInstall.getFecLectTerreno());
										break;
									}

									/* ANL_20230912 */
									Double sumaPrimera = 0.0;
									for (int ind3 = 0; ind3 < listaLecturasMed.size(); ind3++) {
										if (listaLecturasMed.get(ind3).getHorarioLectura().compareTo("ACFP") == 0) {
											sumaPrimera = sumaPrimera
													+ Double.parseDouble(listaLecturasMed.get(ind3).getLectura());
										}
										if (listaLecturasMed.get(ind3).getHorarioLectura().compareTo("ACFPB") == 0) {
											sumaPrimera = sumaPrimera
													+ Double.parseDouble(listaLecturasMed.get(ind3).getLectura());
										}
										if (listaLecturasMed.get(ind3).getHorarioLectura().compareTo("ACFPM") == 0) {
											sumaPrimera = sumaPrimera
													+ Double.parseDouble(listaLecturasMed.get(ind3).getLectura());
										}
									}
									/* ANL_20230912 */

									medInstall.setPrimeraLectura(listaLecturasMed.get(ind2).getLectura());
								}
								// falta ObtenerFechaDDMMYYYY
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("ACFP") == 0) {
									medInstall.setMACFP(listaLecturasMed.get(ind2).getLectura());
								}
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("ACHP") == 0) {
									medInstall.setMACHP(listaLecturasMed.get(ind2).getLectura());
								}
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("ACTI") == 0) {
									medInstall.setMACTI(listaLecturasMed.get(ind2).getLectura());
								}
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("DMFP") == 0) {
									medInstall.setMDMFP(listaLecturasMed.get(ind2).getLectura());
								}
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("DMHP") == 0) {
									medInstall.setMDMHP(listaLecturasMed.get(ind2).getLectura());
								}
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("REAC") == 0) {
									medInstall.setMREAC(listaLecturasMed.get(ind2).getLectura());
								}
								/* ANL_20230912 nuevas lecturas */
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("ACFPB") == 0) {
									medInstall.setMACFPB(listaLecturasMed.get(ind2).getLectura());
								}
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("ACFPM") == 0) {
									medInstall.setMACFPM(listaLecturasMed.get(ind2).getLectura());
								}
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("DMFPB") == 0) {
									medInstall.setMDMFPB(listaLecturasMed.get(ind2).getLectura());
								}
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("DMFPM") == 0) {
									medInstall.setMDMFPM(listaLecturasMed.get(ind2).getLectura());
								}
								/* ANL_20230912 fin */
								listaMedInstall.add(medInstall);
							}

						}

						// MVC 150211 Se cambio a Encontrado en lugar de Retirado 150430 Sólo se
						// procesan medidores encontrados si se trata de producto con cambio de medidor
						if (datosOrden.getListaMedidores().get(ind).getAccionMedidor().compareTo("Encontrado") == 0
								&& ReqCambioMed == 1) {
							curMedRet++;
							MedidorInstallDTO medRet = new MedidorInstallDTO();
							medRet.setNumeroMedidor(datosOrden.getListaMedidores().get(ind).getNumeroMedidor());
							medRet.setCodMarca(datosOrden.getListaMedidores().get(ind).getMarcaMedidor());
							medRet.setCodModelo(datosOrden.getListaMedidores().get(ind).getModeloMedidor());
							medRet.setNovedad("R");

							for (int ind2 = 0; ind2 < listaLecturasMed.size(); ind2++) {
								if (ind2 == 0) {
									try {
										medRet.setFecLectTerreno(
												ObtenerFechaDDMMYYYY(listaLecturasMed.get(ind2).getFechaLectura()));
									} catch (Exception ex) {
										cAuxErr = String.format(
												"Fecha de lectura no válida[%s] formato debe ser AAAA-MM-DD%n",
												medRet.getFecLectTerreno());
										break;
									}

									if (medRet.getFecLectTerreno() == null) {
										medRet.setFecLectTerreno("");
									}

									if (medRet.getFecLectTerreno().length() == 0) {
										cAuxErr = String.format(
												"Fecha de lectura no válida[%s] formato debe ser AAAA-MM-DD%n",
												medRet.getFecLectTerreno());
										break;
									}

									/* ANL_20230912 */
									Double sumaPrimera = 0.0;
									for (int ind3 = 0; ind3 < listaLecturasMed.size(); ind3++) {
										if (listaLecturasMed.get(ind3).getHorarioLectura().compareTo("ACFP") == 0) {
											sumaPrimera = sumaPrimera
													+ Double.parseDouble(listaLecturasMed.get(ind3).getLectura());
										}
										if (listaLecturasMed.get(ind3).getHorarioLectura().compareTo("ACFPB") == 0) {
											sumaPrimera = sumaPrimera
													+ Double.parseDouble(listaLecturasMed.get(ind3).getLectura());
										}
										if (listaLecturasMed.get(ind3).getHorarioLectura().compareTo("ACFPM") == 0) {
											sumaPrimera = sumaPrimera
													+ Double.parseDouble(listaLecturasMed.get(ind3).getLectura());
										}
									}
									/* ANL_20230912 */

									medRet.setPrimeraLectura(listaLecturasMed.get(ind2).getLectura());
								}
								/*
								 * if (ind2 == 0) {
								 * medRet.setFecLectTerreno(listaLecturasMed.get(ind2).getFechaLectura());
								 * // falta ObtenerFechaDDMMYYYY
								 * 
								 * if (medRet.getFecLectTerreno() == null) {
								 * medRet.setFecLectTerreno("");
								 * }
								 * 
								 * if (ObtenerFechaDDMMYYYY(medRet.getFecLectTerreno()).length() == 0) {
								 * cAuxErr = String.format(
								 * "Fecha de lectura no valida[%s] formato debe ser AAAA-MM-DD\n",
								 * medRet.getFecLectTerreno());
								 * break;
								 * }
								 * 
								 * // Guardar primera lectura para archivo de autoactivacion
								 * 
								 * medRet.setPrimeraLectura(listaLecturasMed.get(ind2).getLectura());
								 * }
								 */
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("ACFP") == 0) {
									medRet.setMACFP(listaLecturasMed.get(ind2).getLectura());
								}
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("ACHP") == 0) {
									medRet.setMACHP(listaLecturasMed.get(ind2).getLectura());
								}
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("ACTI") == 0) {
									medRet.setMACTI(listaLecturasMed.get(ind2).getLectura());
								}
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("DMFP") == 0) {
									medRet.setMDMFP(listaLecturasMed.get(ind2).getLectura());
								}
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("DMHP") == 0) {
									medRet.setMDMHP(listaLecturasMed.get(ind2).getLectura());
								}
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("REAC") == 0) {
									medRet.setMREAC(listaLecturasMed.get(ind2).getLectura());
								}
								/* ANL_20230912 nuevas lecturas */
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("ACFPB") == 0) {
									medRet.setMACFPB(listaLecturasMed.get(ind2).getLectura());
								}
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("ACFPM") == 0) {
									medRet.setMACFPM(listaLecturasMed.get(ind2).getLectura());
								}
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("DMFPB") == 0) {
									medRet.setMDMFPB(listaLecturasMed.get(ind2).getLectura());
								}
								if (listaLecturasMed.get(ind2).getHorarioLectura().compareTo("DMFPM") == 0) {
									medRet.setMDMFPM(listaLecturasMed.get(ind2).getLectura());
								}
								/* ANL_20230912 fin */
								listaMedRet.add(medRet);// ArrayMedRet
							}

						}

					}
				}

				if (listaMedInstall.isEmpty()) {
					MedidorInstallDTO medRetVacio = new MedidorInstallDTO();
					medRetVacio.setCodMarca("0");
					medRetVacio.setCodModelo("0");
					medRetVacio.setFactor("0");
					medRetVacio.setFecLectTerreno("0");
					medRetVacio.setMACFP("0");
					medRetVacio.setMACHP("0");
					medRetVacio.setMACTI("0");
					medRetVacio.setMDMFP("0");
					medRetVacio.setMDMHP("0");
					medRetVacio.setMREAC("0");
					medRetVacio.setNovedad("0");
					medRetVacio.setNumeroMedidor("0");
					medRetVacio.setPrimeraLectura("0");
					/* ANL_20230912 nuevas lecturas */
					medRetVacio.setMACFPB("0");
					medRetVacio.setMACFPM("0");
					medRetVacio.setMDMFPB("0");
					medRetVacio.setMDMFPM("0");
					// ACFPB ACFPM DMFPB DMFPM
					/* ANL_20230912 fin */
					listaMedInstall.add(medRetVacio);
				}

				if (listaMedRet.isEmpty()) {
					MedidorInstallDTO medRetVacio = new MedidorInstallDTO();
					medRetVacio.setCodMarca("0");
					medRetVacio.setCodModelo("0");
					medRetVacio.setFactor("0");
					medRetVacio.setFecLectTerreno("0");
					medRetVacio.setMACFP("0");
					medRetVacio.setMACHP("0");
					medRetVacio.setMACTI("0");
					medRetVacio.setMDMFP("0");
					medRetVacio.setMDMHP("0");
					medRetVacio.setMREAC("0");
					medRetVacio.setNovedad("0");
					medRetVacio.setNumeroMedidor("0");
					medRetVacio.setPrimeraLectura("0");
					/* ANL_20230912 nuevas lecturas */
					medRetVacio.setMACFPB("0");
					medRetVacio.setMACFPM("0");
					medRetVacio.setMDMFPB("0");
					medRetVacio.setMDMFPM("0");
					/* ANL_20230912 fin */
					listaMedRet.add(medRetVacio);
				}

				if (cAuxErr == null) {
					cAuxErr = "";
				}

				if (cAuxErr.length() > 0) {
					RegErrorProcesoOrd(datosOrden, "", cAuxErr, parametros);
					// commit; //Queda registrada recepcion con error
					continue;
				}

				lCant_Reg_Cargados++; // Solo se considera recepcionado si no hubo error alguno en las validaciones

				RegistroOrdenDTO registro = new RegistroOrdenDTO();
				if (orden == null) {
					orden = "";
				}
				if (nroCuenta == null) {
					nroCuenta = "";
				}
				if (Ubicacion == null) {
					Ubicacion = "";
				}
				if (Propiedad_Empalme == null) {
					Propiedad_Empalme = "";
				}
				if (Tipo_Conductor == null) {
					Tipo_Conductor = "";
				}
				if (datosOrden.getActaConexion().getCapacidadInterruptor() == null) {
					datosOrden.getActaConexion().setCapacidadInterruptor("");
				}
				if (Sistema_Proteccion == null) {
					Sistema_Proteccion = "";
				}
				if (cajaMedicion == null) {
					cajaMedicion = "";
				}
				if (Caja_Toma == null) {
					Caja_Toma = "";
				}
				if (Longitud_Acometida == null) {
					Longitud_Acometida = "";
				}
				if (datosOrden.getActaConexion().getIdCodClienteVecinoDerecho() == null) {
					datosOrden.getActaConexion().setIdCodClienteVecinoDerecho("");
				}
				if (datosOrden.getActaConexion().getIdCodClienteVecinoIzq() == null) {
					datosOrden.getActaConexion().setIdCodClienteVecinoIzq("");
				}
				if (Nro_PCR == null) {
					Nro_PCR = "";
				}
				if (Tension == null) {
					Tension = "";
				}
				if (Sum_PCR_Ref == null) {
					Sum_PCR_Ref = "";
				}

				if (SETi1 == null) {
					SETi1 = "";
				}
				if (ALIMENTADOR == null) {
					ALIMENTADOR = "";
				}
				if (SED == null) {
					SED = "";
				}
				if (LLAVE == null) {
					LLAVE = "";
				}
				if (SECTOR == null) {
					SECTOR = "";
				}
				if (ZONA == null) {
					ZONA = "";
				}
				if (CORRELATIVO == null) {
					CORRELATIVO = "";
				}
				if (Fecha_Ejecucion == null) {
					Fecha_Ejecucion = "";
				}
				if (datosOrden.getDatosProcesos().getLatitudMedidor() == null) {
					datosOrden.getDatosProcesos().setLatitudMedidor("");
				}

				if (datosOrden.getDatosProcesos().getLongitudMedidor() == null) {
					datosOrden.getDatosProcesos().setLongitudMedidor("");
				}
				if (datosOrden.getDatosProcesos().getLatitudEmpalme() == null) {
					datosOrden.getDatosProcesos().setLatitudEmpalme("");
				}
				if (datosOrden.getDatosProcesos().getLongitudEmpalme() == null) {
					datosOrden.getDatosProcesos().setLongitudEmpalme("");
				}

				registro.setNumOrden(orden);
				registro.setNumCuenta(nroCuenta);
				registro.setPtoEntrega(Ubicacion);
				registro.setPropEmpalme(Propiedad_Empalme);
				registro.setTipoConductor(Tipo_Conductor);
				registro.setCapInterruptor(datosOrden.getActaConexion().getCapacidadInterruptor());
				registro.setSisProteccion(Sistema_Proteccion);
				registro.setCajaMedicion(cajaMedicion);
				registro.setCajaToma(Caja_Toma);
				registro.setAcometida(Longitud_Acometida);
				registro.setSumDerecho("");
				if (datosOrden.getActaConexion().getIdCodClienteVecinoDerecho() != null
						|| !(datosOrden.getActaConexion().getIdCodClienteVecinoDerecho().equals("")))
					registro.setSumDerecho(datosOrden.getActaConexion().getIdCodClienteVecinoDerecho());
				registro.setSumIzquierdo("");
				if (datosOrden.getActaConexion().getIdCodClienteVecinoIzq() != null
						|| !(datosOrden.getActaConexion().getIdCodClienteVecinoIzq().equals("")))
					registro.setSumIzquierdo(datosOrden.getActaConexion().getIdCodClienteVecinoIzq());
				registro.setNumPcr(Nro_PCR);
				registro.setTensionPcr(Tension);
				registro.setSumPcrRef(Sum_PCR_Ref);
				if (listaMedInstall.size() > 0) {
					registro.setNumMedidorInstall(listaMedInstall.get(0).getNumeroMedidor());
					registro.setCodMarcaInstall(listaMedInstall.get(0).getCodMarca());
					registro.setCodModeloInstall(listaMedInstall.get(0).getCodModelo());
					registro.setFecLecturaInstall(listaMedInstall.get(0).getFecLectTerreno());
					registro.setNovedadInstall(listaMedInstall.get(0).getNovedad());
					registro.setLecturaInstall(listaMedInstall.get(0).getPrimeraLectura());
					// R.I. REQ MAXIMETROS INICIO
					/*
					 * while (listaMedInstall.size() < 5) {
					 * // Crear un nuevo objeto MedidorInstallDTO con todos los campos en "0"
					 * MedidorInstallDTO nuevoMedidor = new MedidorInstallDTO();
					 * nuevoMedidor.setNumeroMedidor("0");
					 * nuevoMedidor.setCodMarca("0");
					 * nuevoMedidor.setCodModelo("0");
					 * nuevoMedidor.setFecLectTerreno("0");
					 * nuevoMedidor.setNovedad("0");
					 * nuevoMedidor.setMACFP("0");
					 * nuevoMedidor.setMACHP("0");
					 * nuevoMedidor.setMACTI("0");
					 * nuevoMedidor.setMDMFP("0");
					 * nuevoMedidor.setMDMHP("0");
					 * nuevoMedidor.setMREAC("0");
					 * nuevoMedidor.setPrimeraLectura("0");
					 * nuevoMedidor.setFactor("0");
					 * 
					 * // Agregar el nuevo objeto a la lista
					 * listaMedInstall.add(nuevoMedidor);
					 * }
					 * registro.setMedidorInstalar(listaMedInstall);
					 */
					// R.I. REQ MAXIMETROS FIN
				}
				if (listaMedRet.size() > 0) {
					registro.setNumMedidorRet(listaMedRet.get(0).getNumeroMedidor());// cae
					registro.setCodMarcaRet(listaMedRet.get(0).getCodMarca());
					registro.setCodModeloRet(listaMedRet.get(0).getCodModelo());
					registro.setFecLecturaRet(listaMedRet.get(0).getFecLectTerreno());
					registro.setNovedadRet(listaMedRet.get(0).getNovedad());
					registro.setLecturaRet(listaMedRet.get(0).getPrimeraLectura());
					// R.I. REQ MAXIMETROS INICIO
					/*
					 * while (listaMedRet.size() < 5) {
					 * // Crear un nuevo objeto MedidorInstallDTO con todos los campos en "0"
					 * MedidorInstallDTO nuevoMedidor = new MedidorInstallDTO();
					 * nuevoMedidor.setNumeroMedidor("0");
					 * nuevoMedidor.setCodMarca("0");
					 * nuevoMedidor.setCodModelo("0");
					 * nuevoMedidor.setFecLectTerreno("0");
					 * nuevoMedidor.setNovedad("0");
					 * nuevoMedidor.setMACFP("0");
					 * nuevoMedidor.setMACHP("0");
					 * nuevoMedidor.setMACTI("0");
					 * nuevoMedidor.setMDMFP("0");
					 * nuevoMedidor.setMDMHP("0");
					 * nuevoMedidor.setMREAC("0");
					 * nuevoMedidor.setPrimeraLectura("0");
					 * nuevoMedidor.setFactor("0");
					 * 
					 * // Agregar el nuevo objeto a la lista
					 * listaMedRet.add(nuevoMedidor);
					 * }
					 * registro.setMedidorRetirar(listaMedRet);
					 */
					// R.I. REQ MAXIMETROS FIN
				}
				registro.setSet(SETi1);
				registro.setAlimentador(ALIMENTADOR);
				registro.setSed(SED);
				registro.setLlave(LLAVE);
				registro.setSector(SECTOR);
				registro.setZona(ZONA);
				registro.setCorrelativo(CORRELATIVO);
				// if(datosOrden.getActaConexion().getSetr() != null)
				// registro.setSet(datosOrden.getActaConexion().getSetr());
				// if(datosOrden.getActaConexion().getAlimentador() !=null)
				// registro.setAlimentador(datosOrden.getActaConexion().getAlimentador());
				// if(datosOrden.getActaConexion().getSed() !=null)
				// registro.setSed(datosOrden.getActaConexion().getSed());
				// if(datosOrden.getActaConexion().getLlave() !=null)
				// registro.setLlave(datosOrden.getActaConexion().getLlave());
				// if(datosOrden.getActaConexion().getSector() !=null)
				// registro.setSector(datosOrden.getActaConexion().getSector());
				// if(datosOrden.getActaConexion().getZona() !=null)
				// registro.setZona(datosOrden.getActaConexion().getZona());
				// if(datosOrden.getActaConexion().getCorrelativo() !=null)
				// registro.setCorrelativo(datosOrden.getActaConexion().getCorrelativo());
				registro.setFechaEjecucion(Fecha_Ejecucion);
				registro.setLatitudMedidor(datosOrden.getDatosProcesos().getLatitudMedidor());
				registro.setLongitudMedidor(datosOrden.getDatosProcesos().getLongitudMedidor());
				registro.setLatitudEmpalme(datosOrden.getDatosProcesos().getLatitudEmpalme());
				registro.setLongitudEmpalme(datosOrden.getDatosProcesos().getLongitudEmpalme());

				listaRegistroOrden.add(registro);
				// GetCentroOperativo
				try {
					centroOperativo = jdbcTemplateOracle.queryForObject(sqlCentroOperativo, centroOperativoMapper,
							Double.parseDouble(datosOrden.getDatosProcesos().getCodigoExternoTdC()), orden);
				} catch (EmptyResultDataAccessException e) {

					// vCodOperacion = parametros.getCodErrSY099();
					// vObservacion = parametros.getDesErrSY099() + " Error al obtener centro
					// operativo";

					RegErrorProcesoOrd(datosOrden, "", "Error al obtener centro operativo\n", parametros);
					continue;
				}

				int lCodCOP = 0;
				int ExistPendActivar = 0;
				String cMedidoresRetAdic = "";

				String cMedidoresRetAdicAux;
				String cDesProducto;
				String cCodCOP = "";
				try {
					lCodCOP = Integer.parseInt(centroOperativo);
					cCodCOP = centroOperativo;
				} catch (Exception ex) {
					lCodCOP = 0;
				}
				if (centroOperativo.compareTo("5300") == 0 || centroOperativo.compareTo("5400") == 0
						|| centroOperativo.compareTo("5100") == 0 || centroOperativo.compareTo("5200") == 0) {
					// GetProducto
					try {
						desProducto = jdbcTemplateOracle.queryForObject(sqlDescripcionProducto,
								descripcionProductoMapper, datosOrden.getDatosProcesos().getCodigoExternoTdC(), orden);
						cDesProducto = desProducto;
					} catch (EmptyResultDataAccessException e) {

						// vCodOperacion = parametros.getCodErrSY099();
						// vObservacion = parametros.getDesErrSY099() + " Error al obtener producto de
						// venta";

						RegErrorProcesoOrd(datosOrden, "", "Error al obtener producto de venta\n", parametros);
						continue;
					}
					cMedidoresRetAdic = "";

					if (contMedRet > 2) {
						for (int ind = 1; ind < contMedRet; ind++) {
							if (ind > 1)
								cMedidoresRetAdic = cMedidoresRetAdic + "\t";

							cMedidoresRetAdicAux = String.format("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s",
									listaMedRet.get(0).getNumeroMedidor(),
									listaMedRet.get(0).getCodMarca(),
									listaMedRet.get(0).getCodModelo(),
									listaMedRet.get(0).getFecLectTerreno(),
									listaMedRet.get(0).getNovedad(),
									listaMedRet.get(0).getMACFP(),
									listaMedRet.get(0).getMACHP(),
									listaMedRet.get(0).getMACTI(),
									listaMedRet.get(0).getMDMFP(),
									listaMedRet.get(0).getMDMHP(),
									listaMedRet.get(0).getMREAC(),
									listaMedRet.get(0).getFactor());
							cMedidoresRetAdic = cMedidoresRetAdic + cMedidoresRetAdicAux;
						}
					}

					// escribir linea en archivo de centros operativos especiales EXPANSION /
					// CLIENTES LIBRES / EMPRESARIALES-INSTITUCIONALES / CONSTRUCTORES
					ExistPendActivar = 1;
					String f_repR = String.format(
							"%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
							registro.getNumOrden(),
							registro.getNumCuenta(),
							registro.getPtoEntrega(),
							registro.getPropEmpalme(),
							registro.getTipoConductor(),
							registro.getCapInterruptor(),
							registro.getSisProteccion(),
							registro.getCajaMedicion(),
							registro.getCajaToma(),
							registro.getAcometida(),
							registro.getSumDerecho(),
							registro.getSumIzquierdo(),
							registro.getNumPcr(),
							registro.getTensionPcr(),
							registro.getSumPcrRef(),
							registro.getNumMedidorInstall(),
							registro.getCodMarcaInstall(),
							registro.getCodModeloInstall(),
							registro.getFecLecturaInstall(),
							registro.getNovedadInstall(),
							listaMedInstall.get(0).getMACFP(),
							listaMedInstall.get(0).getMACHP(),
							listaMedInstall.get(0).getMACTI(),
							listaMedInstall.get(0).getMDMFP(),
							listaMedInstall.get(0).getMDMHP(),
							listaMedInstall.get(0).getMREAC(),
							listaMedInstall.get(0).getFactor(),
							listaMedRet.get(0).getNumeroMedidor(),
							listaMedRet.get(0).getCodMarca(),
							listaMedRet.get(0).getCodModelo(),
							listaMedRet.get(0).getFecLectTerreno(),
							listaMedRet.get(0).getNovedad(),
							listaMedRet.get(0).getMACFP(),
							listaMedRet.get(0).getMACHP(),
							listaMedRet.get(0).getMACTI(),
							listaMedRet.get(0).getMDMFP(),
							listaMedRet.get(0).getMDMHP(),
							listaMedRet.get(0).getMREAC(),
							listaMedInstall.get(0).getMACFPB(), /* ANL_20230912 */
							listaMedInstall.get(0).getMACFPM(),
							listaMedInstall.get(0).getMDMFPB(),
							listaMedInstall.get(0).getMDMFPM(), /* ANL_20230912 fin */
							listaMedRet.get(0).getFactor(),
							registro.getSet(),
							registro.getAlimentador(),
							registro.getSed(),
							registro.getLlave(),
							registro.getSector(),
							registro.getZona(),
							registro.getCorrelativo(),
							cDesProducto,
							cCodCOP,
							cMedidoresRetAdic);
					ArchivoUtil.file2Writer.println(f_repR);
				}

			} else {
				String var = String.format("%d\t%s\tFila: %d Error: Los datos de la orden [%s] no son válidos\n",
						nroOrden, "CNXRECEPORDCONEXION", lCant_Reg_Leidos,
						datosOrden.getDatosProcesos().getCodigoExternoTdC());
				Log.logError(var);
				// R.I. Correctivo 10/08/2023 INC000112466943 / INC000112023494 INICIO
				// RegErrorProcesoOrd(datosOrden, "ValidaOrden", var, parametros);
				log.info("Llamando Guardar_registro_Transferencia...");
				Guardar_registro_Transferencia(datosOrden, parametros);
				// R.I. Correctivo 10/08/2023 INC000112466943 / INC000112023494 FIN
			}
			lCant_Reg_Leidos++;
		}
		log.info("Fin FOR para lista listaRegistroOrden , cantidad : " + listaRegistroOrden.size());
		if (Valido_OK == 1) /* Valido_OK) */
		{
			int exitValue = 0;
			try {
				autoactiva.iniciar(listaRegistroOrden, parametros);
			} catch (InterruptedException e) {
				e.printStackTrace();
				log.error("Error: El proceso se detuvo.");
				System.exit(1);
			}

			if (exitValue == 0) {
				log.info("El proceso se ejecutó con éxito");
			} else {
				log.error("Error al ejecutar el proceso");
			}

			// ejecuta_proceso_Autoactivacion(listaRegistroOrden, parametros);
		}

		/* Cantidad_Reg_Cargados(); */
		if (Constante.debug == 1) {
			System.out.println(String.format("lCant_Reg_Leidos %d - lCant_Reg_Cargados %d\n", lCant_Reg_Leidos,
					lCant_Reg_Cargados));
		}

		System.out.println(String.format("Cantidad Registros Procesados Correctamente: %d \n", lCant_Reg_Cargados));
		System.out.println(String.format("Cantidad Registros Recepcionados: %d \n", lCant_Reg_Leidos));
		System.out
				.println(String.format("Cantidad Registros con Error: %d \n", (lCant_Reg_Leidos - lCant_Reg_Cargados)));

		return listaRegistroOrden;
	}

	private void ejecuta_proceso_Autoactivacion(List<RegistroOrdenDTO> listRegistro, ParametrosDTO parametros) {
		autoActivaDAO.procesoAutoActiva(listRegistro, parametros);
	}

	@Transactional
	public void paralizarTransaccion(DatosOrdenDTO datosOrden, ParametrosDTO parametros) {
		log.info("Inicio paralizarOrden con datosOrden : " + datosOrden);
		log.info("parametros : " + parametros);
		Integer iResultParalizar = paralizarOrden(datosOrden, parametros);

		switch (iResultParalizar) {
			case 0:

				System.out.println("Orden fue paralizada por resultado No Realizado\n");
				lCant_Reg_Cargados++;
				break;
			case 1:
				System.out.println("Error al paralizar por resultado No Realizado\n");
				break;
			case 2:
				System.out.println(
						"Resultado No Realizado; orden no se ha paralizado por incluir mas de un servicio electrico\n");
				break;

			case 7:
				System.out.println("Error al invocar preco.PrecoActualizaEstadoVenta\n");
				break;
			case 71:
				System.out.println("Error PrecoActualizaEstadoVenta ha devuelto error\n");
				break;
			case 8:
				System.out.println("Error al actualizar vta_ord_vta - paralizada\n");
				break;
			case 9:
				System.out.println("Error al insertar auditoria de paralizacion\n");
				break;
			case 10:
				System.out.println("No existe Motivo de Paralizacion reportado\n");
				break;
			case 33:
				System.out.println(
						"No procede la Recepción, debido a que la Orden de Venta se encuentra Finalizada o en RevisionFinal\n");
				break;
			case 50: // cfdiaze correcion INC000104436236
				System.out.println("No procede la Recepción, debido a que la Orden de Venta no se encuentra\n"); // cfdiaze
																													// correcion
																													// INC000104436236
				break; // cfdiaze correcion INC000104436236
			default:
				System.out.println(String.format(
						"No se pudo Paralizar la orden reportada con Resultado No Realizado (cod. resultado paralizacion [%d])",
						iResultParalizar));
				break;
		}

		if (iResultParalizar != 0) {
			String cAuxErr = "";
			String cAuxErr2 = "";
			if (iResultParalizar == 2) {
				cAuxErr = "Resultado No Realizado; orden no se ha paralizado por incluir mas de un servicio electrico ";
				cAuxErr2 = "";
			} else if (iResultParalizar == 10) {
				cAuxErr = "No existe Motivo de Paralizacion reportado ";
				cAuxErr2 = datosOrden.getDatosProcesos().getCodigoCausalResultado();
			} else if (iResultParalizar == 33) {
				cAuxErr = "No procede la Recepción, debido a que la Orden de Venta se encuentra Finalizada o en RevisionFinal ";
				cAuxErr2 = "";
			} else {
				cAuxErr = "No se pudo Paralizar la orden reportada con Resultado No Realizado (cod. resultado paralizacion "
						+ iResultParalizar;
				cAuxErr2 = "";
			}

			// vCodOperacion = parametros.getCodErrSY099();
			// vObservacion = parametros.getDesErrSY099() + " " + cAuxErr + " - " +
			// cAuxErr2;

			// AGREGADO RSOSA INDEPE-5702 23062023
			// RegErrorProcesoOrd(datosOrden, cAuxErr2, cAuxErr, parametros);
			// throw new NullPointerException("iResultParalizar !=0");

			if (iResultParalizar != 2) {
				RegErrorProcesoOrd(datosOrden, cAuxErr2, cAuxErr, parametros);
				throw new NullPointerException("iResultParalizar !=0");
			}
			// AGREGADO RSOSA INDEPE-5702 23062023

		}
	}

	public Integer paralizarOrden(DatosOrdenDTO datosOrden, ParametrosDTO parametros) {
		String resul = "";
		String vCodOperacion = parametros.getCodErrSY000();
		Long idMotivo;// id_mot_paralizar
		OrdenVentaDTO ordenVenta = new OrdenVentaDTO();
		Integer contServicios;
		int contValOrdPend = 0; // AGREGADO RSOSA INDEPE-5702 23062023
		AuditoriaDTO auditoria = new AuditoriaDTO();

		String sqlMotivo = MotivoMapper.SQLORACLE_SELECT_FOR_IDMOTIVO;
		String sqlOrdenVenta = OrdenVentaMapper.SQLORACLE_SELECT_FOR_ORDENVENTA; // grogu
		String sqlContServicios = ContServiciosMapper.SQLORACLE_SELECT_FOR_CONTSERVICIOS;
		String sqlAuditoria = AuditoriaMapper.SQLORACLE_SELECT_FOR_AUDITORIA;
		String sqlUpdOrdenVenta = UpdOrdenVentaMapper.SQLPOSTGRE_UPDATE_FOR_ORDENVENTA;
		String sqlInsAuditoria = InsAuditoriaVentaMapper.SQLORACLE_INSERT_FOR_AUDITORIAVENTA;
		String sqlUpdEorOrdTransfer = UpdEorOrdTransferMapper.SQLPOSTGRE_UPDATE_FOR_SUSPEORORDTRANSFER;

		MotivoMapper motivoMapper = new MotivoMapper();
		OrdenVentaMapper ordenVentaMapper = new OrdenVentaMapper();
		ContServiciosMapper contServiciosMapper = new ContServiciosMapper();
		AuditoriaMapper auditoriaMapper = new AuditoriaMapper();

		try {
			idMotivo = jdbcTemplateOracle.queryForObject(sqlMotivo, motivoMapper,
					datosOrden.getDatosProcesos().getCodigoCausalResultado());
		} catch (EmptyResultDataAccessException e) {
			idMotivo = 0L;
			return 10;
		}

		// cfdiaze ini correcion INC000104436236
		System.out.println(String.format("orden de venta: %s", orden));
		Log.ifnMsgToLogFile(String.format("orden de venta: %s", orden));
		log.info("orden de venta: {}", orden);

		System.out
				.println(String.format("codigo externo tdc: %s", datosOrden.getDatosProcesos().getCodigoExternoTdC()));
		Log.ifnMsgToLogFile(
				String.format("codigo externo tdc: %s", datosOrden.getDatosProcesos().getCodigoExternoTdC()));
		log.info("codigo externo tdc: {}", datosOrden.getDatosProcesos().getCodigoExternoTdC());

		orden = "";
		try {
			orden = jdbcTemplatePostgres.queryForObject(OrdServVentaMapper.SQLORACLE_SELECT_FOR_ORDENSRVVENTA,
					new OrdServVentaMapper(), datosOrden.getDatosProcesos().getCodigoExternoTdC());
			// orden = Long.valueOf(numOrden);
		} catch (EmptyResultDataAccessException e) {
			orden = "";
			log.error("ERROR SELECT Nro Orden INTO , sqlOrdServVenta -> " + e);
		} catch (Exception ex) {
			orden = "";
			log.error("ERROR SELECT Nro Orden INTO , sqlOrdServVenta -> " + ex);
		}
		if (orden.equals("")) {
			Log.ifnMsgToLogFile(String.format("Orden de conexion [%s] incompleta o esta vacia ",
					datosOrden.getDatosProcesos().getCodigoExternoTdC()));
			return 50;
		}

		System.out.println(String.format("orden de venta: %s", orden));
		Log.ifnMsgToLogFile(String.format("orden de venta: %s", orden));
		log.info("orden de venta: {}", orden);

		// cfdiaze fin correcion INC000104436236

		try {
			ordenVenta = jdbcTemplateOracle.queryForObject(sqlOrdenVenta, ordenVentaMapper,
					datosOrden.getDatosProcesos().getCodigoExternoTdC(), orden);
		} catch (EmptyResultDataAccessException e) {
			ordenVenta.setIdOrdVta(0L);
			ordenVenta.setIndParalizada("");
			ordenVenta.setNroOrdenVta("");
			ordenVenta.setEstadoOven("");
			return 1;
		}

		if (ordenVenta.getEstadoOven().compareTo("Finalizada") == 0
				|| ordenVenta.getEstadoOven().compareTo("RevisionFinal") == 0) {
			return 33;
		}

		try {
			contServicios = jdbcTemplateOracle.queryForObject(sqlContServicios, contServiciosMapper,
					ordenVenta.getNroOrdenVta());
			// INICIO RSOSA INDEPE-5702 23062023
			if (contServicios > 1) {
				contValOrdPend = validarOrdenes(ordenVenta.getNroOrdenVta());
			}
			// FIN RSOSA INDEPE-5702 23062023
		} catch (EmptyResultDataAccessException e) {
			contServicios = 0;
			System.out.println(
					"SELECT COUNT(1) FROM ORD_ORDEN OVEN, VTA_SRV_VENTA SVEN...\n ProcesoPrincipalDAO.java Linea 570");
			Log.ifnMsgToLogFile(
					"SELECT COUNT(1) FROM ORD_ORDEN OVEN, VTA_SRV_VENTA SVEN...\n ProcesoPrincipalDAO.java Linea 571");
			return 11;
		}

		if (contServicios == 0) {
			return 12;
		}

		// if(contServicios > 1){ // COMENTADO RSOSA INDEPE-5702 23062023
		if (contServicios > 1 || contValOrdPend > 1) { // AGREGADO RSOSA INDEPE-5702 23062023
			actualizarOrdenTransfer(datosOrden, parametros); // AGREGADO RSOSA INDEPE-5702 23062023
			return 2;
		}

		// if(contServicios == 1) { // COMENTADO RSOSA INDEPE-5702 23062023
		if (contServicios == 1 || contValOrdPend == 1) { // AGREGADO RSOSA INDEPE-5702 23062023
			if (ordenVenta.getIndParalizada().compareTo("N") == 0) {

				try {
					SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplateOracle)
							.withProcedureName("PRECO.PrecoActualizaEstadoVenta")
							.withoutProcedureColumnMetaDataAccess();

					jdbcCall.addDeclaredParameter(new SqlParameter("parNroSolic", OracleTypes.VARCHAR));
					jdbcCall.addDeclaredParameter(new SqlParameter("parStateFinal", OracleTypes.VARCHAR));
					jdbcCall.addDeclaredParameter(new SqlParameter("parActividad", OracleTypes.VARCHAR));
					jdbcCall.addDeclaredParameter(new SqlParameter("parIndicador", OracleTypes.NUMBER));
					jdbcCall.addDeclaredParameter(new SqlOutParameter("varRptaTempora", OracleTypes.VARCHAR));

					SqlParameterSource inParams = new MapSqlParameterSource()
							.addValue("parNroSolic", ordenVenta.getNroOrdenVta())
							.addValue("parStateFinal", "SinEjecucion")
							.addValue("parActividad", "DevEje")
							.addValue("parIndicador", 1);

					log.info("PRECO.PrecoActualizaEstadoVenta input: {}", inParams);
					Map<String, Object> outMap = jdbcCall.execute(inParams);

					resul = (String) outMap.get("varRptaTempora");

					log.info("PRECO.PrecoActualizaEstadoVenta output: {}", resul);

					if (!"OK".equals(resul)) {
						return 71;
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					System.out.println("CALL PRECO.PrecoActualizaEstadoVenta\n ProcesoPrincipalDAO.java ");
					Log.ifnMsgToLogFile("CALL PRECO.PrecoActualizaEstadoVenta\n ProcesoPrincipalDAO.java ");
				}

				try {
					auditoria = jdbcTemplateOracle.queryForObject(sqlAuditoria, auditoriaMapper,
							ordenVenta.getIdOrdVta());
				} catch (EmptyResultDataAccessException e) {
					auditoria.setEstadoAct("");
					auditoria.setIdSegmento(0L);
					auditoria.setTipoObra("");
					System.out.println("SELECT estado_destino, tipo_obra, id_segmento\n ProcesoPrincipalDAO.java ");
					Log.ifnMsgToLogFile("SELECT estado_destino, tipo_obra, id_segmento\n ProcesoPrincipalDAO.java ");
					return 13;
				}

				try {
					jdbcTemplateOracle.update(sqlUpdOrdenVenta, ordenVenta.getIdOrdVta());
				} catch (Exception e) {
					System.out.println("UPDATE vta_ord_vta SET paralizada = 'S'\n ProcesoPrincipalDAO.java ");
					Log.ifnMsgToLogFile("UPDATE vta_ord_vta SET paralizada = 'S' \n ProcesoPrincipalDAO.java ");
					return 8;
				}
				long SQAUDITORIAVENTA;
				try {
					String sqlSQAUDITORIAVENTA = "SELECT SQAUDITORIAVENTA.nextval FROM dual";
					SQAUDITORIAVENTA = jdbcTemplateOracle.queryForObject(sqlSQAUDITORIAVENTA, long.class);
				} catch (Exception ex) {
					System.out.println("SELECT SQAUDITORIAVENTA.nextval\n ProcesoPrincipalDAO.java ");
					Log.ifnMsgToLogFile("SELECT SQAUDITORIAVENTA.nextval\n ProcesoPrincipalDAO.java ");
					return 14;
				}

				try {
					jdbcTemplateOracle.update(sqlInsAuditoria, SQAUDITORIAVENTA, parametros.getIdEmpresa(),
							parametros.getIdUsuario(), datosOrden.getDatosComunes().getNotasOperacion(),
							auditoria.getTipoObra(), auditoria.getIdSegmento(), auditoria.getEstadoAct(),
							auditoria.getEstadoAct(), ordenVenta.getIdOrdVta(),
							idMotivo);
				} catch (EmptyResultDataAccessException e) {
					System.out.println("INSERT INTO vta_aud_ord\n ProcesoPrincipalDAO.java ");
					Log.ifnMsgToLogFile("INSERT INTO vta_aud_ord\n ProcesoPrincipalDAO.java ");
					return 9;
				}

				try {
					jdbcTemplatePostgres.update(sqlUpdEorOrdTransfer, parametros.getCodEstEnvia(), vCodOperacion,
							datosOrden.getOrdenesTransfer().getIdOrdTransfer());
				} catch (EmptyResultDataAccessException e) {
					System.out.println("UPDATE EOR_ORD_TRANSFER - suspender ");
					System.out.println("Error: al suspender orden en tablas de  Transferencia de Orden\n ");
					Log.ifnMsgToLogFile("UPDATE EOR_ORD_TRANSFER - suspender ");
					return 15;
				}

			}
		}
		return 0;
	}

	// Guardar_registro_Transferencia
	/*
	 * public void regErrorProcesoOrd(DatosOrdenDTO datosOrden, ParametrosDTO
	 * parametros, String vCodOperacion, String vObservacion) {
	 * 
	 * String sqlUpdEorOrdTransfer2 =
	 * UpdEorOrdTransferMapper.SQLPOSTGRE_UPDATE_FOR_ERRORORDTRANSFER;
	 * String sqlUpdOrdTransferDet =
	 * UpdEorOrdTransferDetMapper.SQLPOSTGRE_UPDATE_FOR_ERRORORORDTRANSFERDET;
	 * 
	 * jdbcTemplatePostgres.update(sqlUpdEorOrdTransfer2, vCodOperacion,
	 * vObservacion, vCodOperacion, parametros.getCodErrSY000(),
	 * parametros.getCodEstRecep(), parametros.getCodEstRecer(),
	 * datosOrden.getOrdenesTransfer().getIdOrdTransfer());
	 * jdbcTemplatePostgres.update(sqlUpdOrdTransferDet, vCodOperacion,
	 * datosOrden.getOrdenesTransfer().getIdOrdTransfer(),
	 * datosOrden.getOrdenesTransfer().getNroRecepciones());
	 * }
	 */
	private void SetErrGen(String pErrorNCat, ParametrosDTO parametro) {
		vCodOperacion = parametro.getCodErrSY099();
		vObservacion = parametro.getDesErrSY099() + " " + pErrorNCat;
	}

	// R.I. CORRECCION 12/09/2023
	// Se cambia el argumento y se agrega ordConex a la consulta de codTarifa
	private boolean GetTarifa(String ordConex) {
		String sqlTarifa = TarifaMapper.SQLORACLE_SELECT_FOR_TARIFA;
		TarifaMapper tarifaMapper = new TarifaMapper();

		try {
			codTarifa = jdbcTemplateOracle.queryForObject(sqlTarifa, tarifaMapper, ordConex);
		} catch (EmptyResultDataAccessException e) {
			codTarifa = "";
			System.out.println("Error: SELECT ftb.cod_interno INTO :cod_tarifa\n");
			// vCodOperacion = parametros.getCodErrSY099();
			// vObservacion = parametros.getDesErrSY099() + " Error en obtener tarifa\n";
			return false;
		}
		System.out.println(String.format("\ncod_tarifa: %s\n", codTarifa));
		return true;
	}

	private boolean GetDescripDTC(String tabla, String cDato) {
		String sqlDescripDTC = VtaAutoTablaMapper.SQLORACLE_SELECT_FOR_DESCRIPDTC;
		VtaAutoTablaMapper descripDTCMapper = new VtaAutoTablaMapper();

		try {
			vtaAutoTabla = jdbcTemplateOracle.queryForObject(sqlDescripDTC, descripDTCMapper, cDato, tabla);
			/*-----------------------------------*/
			// datosOrden.getActaConexion().setTipoConductor(descripDTC);
			/*-----------------------------------*/

		} catch (EmptyResultDataAccessException e) {

			Log.ifnMsgToLogFile("SELECT descripcion FROM NOSYN.vta_auto_tabla. ProcesoPrincipalDAO.java");
			// vCodOperacion = parametros.getCodErrSY099();
			// vObservacion = parametros.getDesErrSY099() + " Valor No válido de Tipo de
			// Conductor " + datosOrden.getActaConexion().getTipoConductor();
			return false;
		}
		return true;
	}

	private String GetDescripDTCString(String tabla, String cDato) {
		String sqlDescripDTC = VtaAutoTablaMapper.SQLORACLE_SELECT_FOR_DESCRIPDTC;
		VtaAutoTablaMapper descripDTCMapper = new VtaAutoTablaMapper();

		try {
			vtaAutoTabla = jdbcTemplateOracle.queryForObject(sqlDescripDTC, descripDTCMapper, cDato, tabla);
			/*-----------------------------------*/
			// datosOrden.getActaConexion().setTipoConductor(descripDTC);
			/*-----------------------------------*/

		} catch (EmptyResultDataAccessException e) {

			Log.ifnMsgToLogFile("SELECT descripcion FROM NOSYN.vta_auto_tabla. ProcesoPrincipalDAO.java");
			// vCodOperacion = parametros.getCodErrSY099();
			// vObservacion = parametros.getDesErrSY099() + " Valor No válido de Tipo de
			// Conductor " + datosOrden.getActaConexion().getTipoConductor();
		}
		return vtaAutoTabla.getDescripcion();
	}

	private boolean GetDescripPtoEntrega(ParametrosDTO parametros, DatosOrdenDTO datosOrden) {
		String sqlDescripPtoEntrega = SrvPtoEntregaMapper.SQLORACLE_SELECT_FOR_DESCRIPPTOENTREGA;

		SrvPtoEntregaMapper srvPtoEntregaMapper = new SrvPtoEntregaMapper();

		try {
			descPtoEntrega = jdbcTemplateOracle.queryForObject(sqlDescripPtoEntrega, srvPtoEntregaMapper,
					datosOrden.getActaConexion().getPuntoFisicoConexion());
			/*-----------------------------------*/
			// datosOrden.getActaConexion().setPuntoFisicoConexion(descPtoEntrega);
			/*-----------------------------------*/
		} catch (EmptyResultDataAccessException e) {

			Log.ifnMsgToLogFile("SELECT descripcion FROM srv_pto_entrega. ProcesoPrincipalDAO.java");
			// vCodOperacion = parametros.getCodErrSY099();
			// vObservacion = parametros.getDesErrSY099() + " Valor No válido de punto
			// fisico de conexion " + datosOrden.getActaConexion().getPuntoFisicoConexion();
			return false;
		}

		return true;
	}

	private String GetDescripPtoEntregaString(ParametrosDTO parametros, DatosOrdenDTO datosOrden) {
		String sqlDescripPtoEntrega = SrvPtoEntregaMapper.SQLORACLE_SELECT_FOR_DESCRIPPTOENTREGA;

		SrvPtoEntregaMapper srvPtoEntregaMapper = new SrvPtoEntregaMapper();

		try {
			descPtoEntrega = jdbcTemplateOracle.queryForObject(sqlDescripPtoEntrega, srvPtoEntregaMapper,
					datosOrden.getActaConexion().getPuntoFisicoConexion());
			/*-----------------------------------*/
			// datosOrden.getActaConexion().setPuntoFisicoConexion(descPtoEntrega);
			/*-----------------------------------*/
		} catch (EmptyResultDataAccessException e) {
			// descPtoEntrega = "";
			Log.ifnMsgToLogFile("SELECT descripcion FROM srv_pto_entrega. ProcesoPrincipalDAO.java");
		}

		return descPtoEntrega.getDescripcion();
	}

	private boolean GetDatosAdicSrvVenta(ParametrosDTO parametros, DatosOrdenDTO datosOrden) {
		String sqlOrdServVenta = OrdServVentaMapper.SQLORACLE_SELECT_FOR_ORDENSRVVENTA; // grogu
		String sqlNroCuentaSrvVenta = NroCuentaSrvVentaMapper.SQLORACLE_SELECT_FOR_NROCUENTASRVVENTA;
		String sqlCajaMedicionSrvVenta = CajaMedicionSrvVentaMapper.SQLORACLE_SELECT_FOR_CAJAMEDSRVVENTA;
		String sqlProductoSrvVenta = ProductoSrvVentaMapper.SQLORACLE_SELECT_FOR_PRODUCTOSRVVENTA;
		String sqlContPropiedad = ContPropiedadMapper.SQLORACLE_SELECT_FOR_CONTPROPIEDAD;

		OrdServVentaMapper ordServVentaMapper = new OrdServVentaMapper();
		NroCuentaSrvVentaMapper nroCuentaSrvVentaMapper = new NroCuentaSrvVentaMapper();
		CajaMedicionSrvVentaMapper cajaMedicionSrvVentaMapper = new CajaMedicionSrvVentaMapper();
		ProductoSrvVentaMapper productoSrvVentaMapper = new ProductoSrvVentaMapper();
		ContPropiedadMapper contPropiedadMapper = new ContPropiedadMapper();

		String numOrden;
		try {
			orden = jdbcTemplatePostgres.queryForObject(sqlOrdServVenta, ordServVentaMapper,
					datosOrden.getDatosProcesos().getCodigoExternoTdC());
			// orden = Long.valueOf(numOrden);
		} catch (EmptyResultDataAccessException e) {
			orden = "";
			// orden = 0L;
			log.error("ERROR SELECT Nro Orden INTO , sqlOrdServVenta -> " + e);
			// return false;
		} catch (Exception ex) {
			orden = "";
			log.error("ERROR SELECT Nro Orden INTO , sqlOrdServVenta -> " + ex);
		}

		if (orden.equals("")) {
			Log.ifnMsgToLogFile(String.format("Orden de conexion [%s] incompleta o esta vacia ",
					datosOrden.getDatosProcesos().getCodigoExternoTdC()));
			return false;
		}

		String numCuenta;
		try {
			nroCuenta = jdbcTemplateOracle.queryForObject(sqlNroCuentaSrvVenta, nroCuentaSrvVentaMapper,
					datosOrden.getDatosProcesos().getCodigoExternoTdC(), orden);
			// nroCuenta = Long.parseLong(numCuenta);
		} catch (EmptyResultDataAccessException e) {
			// nroCuenta = 0L;
			nroCuenta = "";
			log.error("Error: ERROR SELECT  Nro Cuenta INTO\n , sqlNroCuentaSrvVenta -> " + e);

		} catch (Exception ex) {
			nroCuenta = "";
			log.error("ERROR SELECT Nro Orden INTO , sqlNroCuentaSrvVenta -> " + ex);
		}

		if (nroCuenta.equals("")) {
			Log.ifnMsgToLogFile(String.format("Orden de conexion [%s] no tiene asociada una cuenta o esta vacia ",
					datosOrden.getDatosProcesos().getCodigoExternoTdC()));
			return false;
		}

		try {
			cajaMedicion = jdbcTemplateOracle.queryForObject(sqlCajaMedicionSrvVenta, cajaMedicionSrvVentaMapper,
					datosOrden.getDatosProcesos().getCodigoExternoTdC(), orden);
		} catch (EmptyResultDataAccessException e) {
			// cajaMedicion = "";
			log.error("EmptyResultDataAccessException: ERROR SELECT Caja Medicion INTO\n  , sqlCajaMedicionSrvVenta -> "
					+ e);
			// vCodOperacion = parametros.getCodErrSY099();
			// vObservacion = parametros.getDesErrSY099() + " Error al obtener Datos de
			// servicio de Venta";

		} catch (Exception ex) {
			log.error("Error: ERROR SELECT Caja Medicion INTO\n  , sqlCajaMedicionSrvVenta -> " + ex);
			return false;
		}

		if (orden.equals("")) {
			Log.ifnMsgToLogFile(String.format("Orden de conexion [%s].No se pudo determinar una caja de medicion ",
					datosOrden.getDatosProcesos().getCodigoExternoTdC()));
			return false;
		}

		try {
			producto = jdbcTemplateOracle.queryForObject(sqlProductoSrvVenta, productoSrvVentaMapper,
					datosOrden.getDatosProcesos().getCodigoExternoTdC(), orden);
		} catch (EmptyResultDataAccessException e) {
			// producto.setIdProducto(0L);
			// producto.setTipProducto("");
			log.error("Error: SELECT PROD.ID_PRODUCTO, PROD.TIP_PRODUCTO\n ");
			// vCodOperacion = parametros.getCodErrSY099();
		} catch (Exception ex) {
			log.error("Error: SELECT PROD.ID_PRODUCTO, PROD.TIP_PRODUCTO\n ");
			return false;
		}

		ReqInstalMed = 0;
		ReqCambioMed = 0;
		// R.I. CORRECCION 12/09/2023
		// Ahora se usa el nro de orden de conexión como argumento
		if (GetTarifa(datosOrden.getDatosProcesos().getCodigoExternoTdC()) == false) {
			System.out.println("Error en obtener tarifa\n");
			return false;
		}

		if (codTarifa.compareTo("BT6") != 0) {
			if (producto.getTipProducto().compareTo("NVO") == 0 || producto.getTipProducto().compareTo("REI") == 0) {
				ReqInstalMed = 1;
			} else {
				if (producto.getTipProducto().compareTo("MOD") == 0) {
					try {
						contPropiedad = jdbcTemplateOracle.queryForObject(sqlContPropiedad, contPropiedadMapper,
								producto.getIdProducto());
					} catch (EmptyResultDataAccessException e) {
						// vCodOperacion = parametros.getCodErrSY099();
						// vObservacion = parametros.getDesErrSY099() + " Error al obtener Datos de
						// servicio de Venta";
						System.out.println("Error: SELECT COUNT(1) FROM PRD_PROPIEDAD\\n");
						return false;
					}

					if (contPropiedad > 0) {
						ReqCambioMed = 1;
					}
				}
			}
		}

		// Obtener cantidad de medidores (Retiro / instalacion)
		contMedInstall = 0;
		contMedRet = 0;

		for (int ind = 0; ind < datosOrden.getListaMedidores().size(); ind++) {
			if (datosOrden.getListaMedidores().get(ind).getAccionMedidor().compareTo("Instalado") == 0) {
				contMedInstall++;
			}

			if (datosOrden.getListaMedidores().get(ind).getAccionMedidor().compareTo("Encontrado") == 0) {
				contMedRet++;
			}
		}

		return true;
	}

	public boolean ValidaOrden(ParametrosDTO parametros, DatosOrdenDTO datosOrden) {

		if (!GetDatosAdicSrvVenta(parametros, datosOrden)) {
			SetErrGen("Error al obtener Datos de servicio de Venta", parametros);
			orden = "";
			return false;
		}

		if (!GetDescripPtoEntrega(parametros, datosOrden)) {
			String cAux = String.format("Valor No válido de punto fisico de conexion [%s]",
					datosOrden.getActaConexion().getPuntoFisicoConexion());
			SetErrGen(cAux, parametros);
			return false;
		}

		if (!GetDescripDTC("TIPO_CONDUCTOR", datosOrden.getActaConexion().getTipoConductor())) {
			String cAux = String.format("Valor No válido de Tipo de Conductor [%s]",
					datosOrden.getActaConexion().getTipoConductor());
			SetErrGen(cAux, parametros);
			return false;
		}

		if (!GetDescripDTC("CAP_INTERRUP", datosOrden.getActaConexion().getCapacidadInterruptor())) {
			String cAux = String.format("Valor No válido de Capacidad Interruptor [%s]",
					datosOrden.getActaConexion().getCapacidadInterruptor());
			SetErrGen(cAux, parametros);
			return false;

		}

		/********************** Validaciones de medidores ************************/

		if (ReqInstalMed == 1) {
			String cAux = "";
			if (contMedInstall == 0) {
				// vCodOperacion = parametros.getCodErrSY099();
				// vObservacion = parametros.getDesErrSY099() + " Falta datos de medidor
				// Instalado";
				cAux = "Falta datos de medidor Instalado";
			}
			// if (contMedInstall > 1) {
			// // vCodOperacion = parametros.getCodErrSY099();
			// // vObservacion = parametros.getDesErrSY099() + " Hay mas de un medidor; solo
			// se
			// // puede Instalar uno";
			// cAux = "Hay mas de un medidor; solo se puede Instalar uno";
			// }

			if (cAux == null) {
				cAux = "";
			}

			if (cAux.length() > 0) {
				SetErrGen(cAux, parametros);
				return false;
			}
		}

		if (ReqCambioMed == 1) {
			String cAux = "";
			if (contMedInstall == 0) {
				// vCodOperacion = parametros.getCodErrSY099();
				// vObservacion = parametros.getDesErrSY099() + " Falta datos de medidor
				// Instalado";
				cAux = "Falta datos de medidor Instalado";
			}

			/*
			 * if (contMedInstall > 1) {
			 * // vCodOperacion = parametros.getCodErrSY099();
			 * // vObservacion = parametros.getDesErrSY099() + " Hay mas de un medidor; solo
			 * se
			 * // puede Instalar uno";
			 * cAux = "Hay mas de un medidor; solo se puede Instalar uno";
			 * }
			 */
			if (contMedRet == 0) {
				// vCodOperacion = parametros.getCodErrSY099();
				// vObservacion = parametros.getDesErrSY099() + " Falta datos de medidor
				// Retirado";
				cAux = "Falta datos de medidor Retirado";
			}
			if (contMedRet > 6) {
				// vCodOperacion = parametros.getCodErrSY099();
				// vObservacion = parametros.getDesErrSY099() + " Hay mas de seis medidores;
				// solo se puede retirar la cantidad de medidores que tiene instalado el
				// servicio";
				cAux = "Hay mas de seis medidores; solo se puede retirar la cantidad de medidores que tiene instalado el servicio";
			}

			if (cAux == null) {
				cAux = "";
			}

			if (cAux.length() > 0) {
				SetErrGen(cAux, parametros);
				return false;
			}
		}

		/***************** Validar Dominios ******************************/
		// Medidores

		for (int j = 0; j < datosOrden.getListaMedidores().size(); j++) {
			if (!bfnValidarDominio("MED_MARCA", "COD_MARCA", datosOrden.getListaMedidores().get(j).getMarcaMedidor(),
					parametros, "SCOM")) {
				return false;
			}
			if (!bfnValidarDominio("MED_MODELO", "COD_MODELO", datosOrden.getListaMedidores().get(j).getModeloMedidor(),
					parametros, "SCOM")) {
				return false;
			}
			if (!bfnValidarDominio("COM_FASE", "COD_FASE", datosOrden.getListaMedidores().get(j).getFaseMedidor(),
					parametros, "SCOM")) {
				return false;
			}
			if (!bfnValidarDominio("MED_PROPIEDAD", "COD_PROPIEDAD",
					datosOrden.getListaMedidores().get(j).getPropiedadMedidor(), parametros, "SCOM")) {
				return false;
			}
		}

		//// Lecturas (dominios y fechas)
		for (int j = 0; j < datosOrden.getListaLecturas().size(); j++) {
			String cAux = "";
			if (!bfnValidarDominio("MED_MEDIDA", "COD_MEDIDA", datosOrden.getListaLecturas().get(j).getHorarioLectura(),
					parametros, "SCOM")) {
				return false;
			}

			String cFechaTmp = datosOrden.getListaLecturas().get(j).getFechaLectura();

			try {
				datosOrden.getListaLecturas().get(j).setFechaLectura(ObtenerFechaDDMMYYYY(cFechaTmp));
			} catch (Exception ex) {
				cAux = String.format("fecha de lectura no valida[%s] (formato debe ser AAAA-MM-DD)", cFechaTmp);
				SetErrGen(cAux, parametros);
				return false;
			}
			if (datosOrden.getListaLecturas().get(j).getFechaLectura().equals("")) {
				cAux = String.format("fecha de lectura no valida[%s] (formato debe ser AAAA-MM-DD)", cFechaTmp);
				SetErrGen(cAux, parametros);
				return false;
			}
		}

		if (!bfnValidarDominio("NUC_TIP_DOC_PERSONA", "COD_TIP_DOC_PERSONA",
				datosOrden.getActaConexion().getTipoDocumentoPropietario(), parametros, "4J")) {
			return false;
		}
		if (!bfnValidarDominio("COM_TENSION", "COD_TENSION", datosOrden.getActaConexion().getCodTipoRed(), parametros,
				"SCOM")) {
			return false;
		}
		if (!bfnValidarDominio("SRV_SET", "COD_SET", datosOrden.getActaConexion().getSetr(), parametros, "SCOM")) {
			return false;
		}
		if (!bfnValidarDominio("SRV_ALIMENTADOR", "COD_ALIMENTADOR", datosOrden.getActaConexion().getAlimentador(),
				parametros, "SCOM")) {
			return false;
		}
		if (!bfnValidarDominio("SRV_LLAVE", "DESCRIPCION", datosOrden.getActaConexion().getLlave(), parametros,
				"SCOM")) {
			return false;
		}
		return true;
		// fin ValidaOrden
	}

	private boolean bfnValidarDominio(String pNomTabla, String pNomCampo, String pValorCampo, ParametrosDTO parametros,
			String conexion) {
		TablaMapper oTablaMapper = new TablaMapper();
		TablaDTO oTabla = new TablaDTO();
		String sqlTabla = oTablaMapper.SQLORACLE_SELECT_FOR_TABLA;
		boolean bResult = true;

		if (pValorCampo == null) {
			pValorCampo = "";
		}

		if (pValorCampo.length() == 0) {
			return bResult;
		}

		if (Constante.debug_sql == 1) {
			System.out.println(String.format("SELECT COUNT(1) Validar Dominio: [%s] \n", sqlTabla));
		}

		try {
			if (conexion == "SCOM") {
				oTabla = jdbcTemplatePostgres.queryForObject(String.format(sqlTabla, pNomTabla, pNomCampo),
						oTablaMapper, pValorCampo);
			} else {
				oTabla = jdbcTemplateOracle.queryForObject(String.format(sqlTabla, pNomTabla, pNomCampo), oTablaMapper,
						pValorCampo);
			}
		} catch (Exception ex) {
			Log.ifnMsgToLogFile("Error PREPARE SELECT COUNT(1) FROM ... Dominio");
		}

		if (oTabla.getCount() == 0) {
			bResult = false;
		}
		if (!bResult) {
			vCodOperacion = parametros.getCodErrSY045();
			vObservacion = String.format("%s: %s-%s", parametros.getDesErrSY045(), pNomCampo, pValorCampo);
		}
		return bResult;
	}

	private String ObtenerFechaDDMMYYYY(String fechaIn) {

		SimpleDateFormat formato2 = new SimpleDateFormat("yyyy-MM-dd");
		Date fechaInDate;
		String strDate;

		try {
			fechaInDate = formato2.parse(fechaIn);
		} catch (ParseException e) {
			fechaInDate = null;
			e.printStackTrace();
		}

		try {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY");
			strDate = dateFormat.format(fechaInDate);
			System.out.println("strDate es : " + strDate);
		} catch (Exception ex) {
			strDate = "";
		}
		return strDate;
	}

	private void RegErrorProcesoOrd(DatosOrdenDTO datosOrden, String pcDato, String pcError, ParametrosDTO parametros) {
		String cErrTmp;
		cErrTmp = String.format("%s - [%s]", pcError, pcDato);
		SetErrGen(cErrTmp, parametros);
		Guardar_registro_Transferencia(datosOrden, parametros);
	}

	private void Guardar_registro_Transferencia(DatosOrdenDTO datosOrden, ParametrosDTO parametros) {
		String sqlUpdEorOrdTransfer2 = UpdEorOrdTransferMapper.SQLPOSTGRE_UPDATE_FOR_ERRORORDTRANSFER;
		String sqlUpdOrdTransferDet = UpdEorOrdTransferDetMapper.SQLPOSTGRE_UPDATE_FOR_ERRORORORDTRANSFERDET;

		jdbcTemplatePostgres.update(sqlUpdEorOrdTransfer2, vCodOperacion, vObservacion, vCodOperacion,
				parametros.getCodErrSY000(),
				parametros.getCodEstRecep(), parametros.getCodEstRecer(),
				datosOrden.getOrdenesTransfer().getIdOrdTransfer());
		jdbcTemplatePostgres.update(sqlUpdOrdTransferDet, vCodOperacion,
				datosOrden.getOrdenesTransfer().getIdOrdTransfer(),
				datosOrden.getOrdenesTransfer().getNroRecepciones());

	}

	private void ImprimeCabecera() {
		String f_repR = String.format(
				"%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
				"Nro Orden", "Nro Cuenta", "Ubicacion", "Propiedad Empalme", "Tipo Conductor", "Capacidad Interruptor",
				"Sistema Proteccion", "Caja Medicion", "Caja Toma",
				"Longitud Acometida", "Sum Derecho", "Sum Izquierdo", "Nro PCR", "Tension", "Sum_PCR_Ref",
				"Numero Medidor", "Cod Marca", "Cod Modelo", "Fecha Lect Terreno",
				"Novedad", "ACFP", "ACHP", "ACTI", "DMFP", "DMHP", "REAC", "Cod_Factor", "Numero Medidor", "Cod Marca",
				"Cod Modelo", "Fecha Lect Terreno", "Novedad", "ACFP", "ACHP",
				"ACTI", "DMFP", "DMHP", "REAC", "Cod_Factor", "SET", "Alimentador", "SED", "Llave", "Sector", "Zona",
				"Correlativo", "Producto", "Centro Operativo",
				"Nro Medidor (Retiro 2)", "Cod Marca", "Cod Modelo", "Fecha Lect Terreno", "Novedad", "ACFP", "ACHP",
				"ACTI", "DMFP", "DMHP", "REAC", "Factor");
		ArchivoUtil.file2Writer.println(f_repR);

		Log.logError("Nro. Orden\tTipo Orden\tDescripcion Error\tDatos error\tDatos relacionados:\n \n");
	}

	// INICIO RSOSA INDEPE-5702 23062023

	public int validarOrdenes(String nroOrdenVtas) {
		int response = 0;
		String sqlordenesConexionPorSuspender = EnterosGlobalMapper.SQLPOSTGRE_SELECT_FOR_COUNT_ESTADO_ORDCNXPORSUSP;
		EnterosGlobalMapper enterosGlobalMapper = new EnterosGlobalMapper();
		response = jdbcTemplatePostgres.queryForObject(sqlordenesConexionPorSuspender, enterosGlobalMapper,
				nroOrdenVtas);
		return response;

	}

	public void actualizarOrdenTransfer(DatosOrdenDTO datosOrden, ParametrosDTO parametros) {

		String sqlUpdEorOrdTransfer = UpdEorOrdTransferMapper.SQLPOSTGRE_UPDATE_FOR_ORDTRANSFER;
		String sqlUpdOrdTransferDet = UpdEorOrdTransferDetMapper.SQLPOSTGRE_UPDATE_FOR_ORORDTRANSFERDET;

		try {
			jdbcTemplatePostgres.update(sqlUpdEorOrdTransfer, parametros.getCodErrSY200(), parametros.getDesErrSY200(),
					datosOrden.getOrdenesTransfer().getIdOrdTransfer());
		} catch (Exception ex) {
			Log.logError("Ocurrio un error al actualizar eor_ord_transfer de paralizacion: " + ex.getMessage());
		}

		try {
			jdbcTemplatePostgres.update(sqlUpdOrdTransferDet, parametros.getCodErrSY200(),
					datosOrden.getOrdenesTransfer().getIdOrdTransfer(),
					datosOrden.getOrdenesTransfer().getNroRecepciones());
		} catch (Exception ex) {
			Log.logError("Ocurrio un error al actualizar eor_ord_transfer_det de paralizacion: " + ex.getMessage());
		}

	}
	// FIN RSOSA INDEPE-5702 23062023

}
