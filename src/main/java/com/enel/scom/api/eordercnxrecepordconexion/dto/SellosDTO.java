package com.enel.scom.api.eordercnxrecepordconexion.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SellosDTO {

	private String numeroSello;
	private String ubicacionSello;
	private String serieSello;
	private String tipoSello;
	private String colorSello;
	private String estadoSello;
	private String accionSello;
}
