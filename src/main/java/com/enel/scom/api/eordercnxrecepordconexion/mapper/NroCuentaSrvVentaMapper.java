package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class NroCuentaSrvVentaMapper implements RowMapper<String> {

	public static final String SQLORACLE_SELECT_FOR_NROCUENTASRVVENTA = "SELECT"
			+ " NC.NRO_CUENTA as nroCuenta"
			+ " FROM ORD_ORDEN ORD, ORD_CONEX OCNX, VTA_SRV_VENTA SVEN,"
			+ "	VTA_SOL_SRV_ELE SELE, NUC_CUENTA NC"
			+ " WHERE ORD.ID_ORDEN = OCNX.ID_ORD_VENTA"
			+ " AND ORD.ID_ORDEN = SVEN.ID_ORD_VENTA"
			+ " AND OCNX.ID_ORD_CONEX = SVEN.ID_ORD_CONEX"
			+ " AND SVEN.ID_SOL_SRV_ELE = SELE.ID_SOL_SRV_ELE"
			+ " AND SELE.ID_CUENTA_FAC = NC.ID_CUENTA"
			+ " AND OCNX.NRO_ORDEN = ?"
			+ " AND ORD.NRO_ORDEN = ?"
			;

	@Override
	public String mapRow(ResultSet rs, int rowNum) throws SQLException {
		String nroCuenta = rs.getString("nroCuenta");
		return nroCuenta;
	}
	
}
