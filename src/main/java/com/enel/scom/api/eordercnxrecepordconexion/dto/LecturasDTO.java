package com.enel.scom.api.eordercnxrecepordconexion.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class LecturasDTO {

	private String tipoLectura;
	private String lectura;
	private String horarioLectura;
	private String fechaLectura;
}
