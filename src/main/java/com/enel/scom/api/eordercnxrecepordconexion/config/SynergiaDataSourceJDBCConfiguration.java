package com.enel.scom.api.eordercnxrecepordconexion.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.enel.scom.api.eordercnxrecepordconexion.bean.BeanDataSource;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class SynergiaDataSourceJDBCConfiguration {

	public static final BeanDataSource beanConn = DataSourceConfig.getConnectionDatos("ORCL");
	//public static final BeanDataSource beanConnMOD = DataSourceConfig.getConnectionDatos("ORCLMOD");

	/*
	 * @Bean(name = "oracleDatasource")
	 * 
	 * @ConfigurationProperties(prefix = "oracle.datasource") public DataSource
	 * oracleDataSource() { return DataSourceBuilder.create().build(); }
	 * 
	 * @Bean(name = "oracleJdbcTemplate") public JdbcTemplate
	 * jdbcTemplate(@Qualifier("oracleDb") DataSource dsOracle) { return new
	 * JdbcTemplate(dsOracle); }
	 */

	@Bean(name = "oracleDatasource")
	@Primary
	public DataSource postgresDataSource() {

		HikariConfig dataSourceBuilder = new HikariConfig();
		dataSourceBuilder.setDriverClassName(beanConn.getDbDriver());
		dataSourceBuilder.setJdbcUrl(beanConn.getDbURL());
		dataSourceBuilder.setUsername(beanConn.getDbUser());
		dataSourceBuilder.setPassword(beanConn.getDbPass());
		dataSourceBuilder.setMaximumPoolSize(beanConn.getDbMax());
		dataSourceBuilder.setMinimumIdle(beanConn.getDbMin());

		HikariDataSource dataSource = new HikariDataSource(dataSourceBuilder);
		return dataSource;
	}

	@Bean(name = "jdbcTemplate1")
	@DependsOn("oracleDatasource")
	public JdbcTemplate jdbcTemplate() {
		return new JdbcTemplate(new SingleConnectionDataSource(beanConn.getConexion(), true));
	}

//	@Bean(name = "oracleDatasourceMOD")
//	@Primary
//	public DataSource postgresDataSourceMOD() {
//
//		HikariConfig dataSourceBuilder = new HikariConfig();
//		dataSourceBuilder.setDriverClassName(beanConnMOD.getDbDriver());
//		dataSourceBuilder.setJdbcUrl(beanConnMOD.getDbURL());
//		dataSourceBuilder.setUsername(beanConnMOD.getDbUser());
//		dataSourceBuilder.setPassword(beanConnMOD.getDbPass());
//		dataSourceBuilder.setMaximumPoolSize(beanConnMOD.getDbMax());
//		dataSourceBuilder.setMinimumIdle(beanConnMOD.getDbMin());
//
//		HikariDataSource dataSource = new HikariDataSource(dataSourceBuilder);
//		return dataSource;
//	}
//
//	@Bean(name = "jdbcTemplateMOD")
//	@DependsOn("oracleDatasourceMOD")
//	public JdbcTemplate jdbcTemplateMOD() {
//		return new JdbcTemplate(new SingleConnectionDataSource(beanConnMOD.getConexion(), true));
//	}

}
