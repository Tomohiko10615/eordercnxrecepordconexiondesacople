package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.MedLecUltimaDTO;

public class MedLecUltimaMapper implements RowMapper<MedLecUltimaDTO> {

	public static final String SQLORACLESQL_SELECT_FOR_MED_LEC_ULTIMA = "SELECT ID_LECTURA,\r\n"
			+ " ID_COMPONENTE,\r\n"
			+ " ID_MEDIDA,\r\n"
			+ " ID_EMPRESA\r\n"
			+ " FROM med_lec_ultima\r\n"
			+ " WHERE id_componente = ?"
			;
	
	public static final String SQLPOSTGRESQL_SELECT_FOR_MED_LEC_ULTIMA = "SELECT ID_LECTURA,\r\n"
			+ " ID_COMPONENTE,\r\n"
			+ " ID_MEDIDA,\r\n"
			+ " ID_EMPRESA\r\n"
			+ " FROM med_lec_ultima\r\n"
			+ " WHERE id_componente = ?"
			;
	
	public static final String SQLPOSTGREEQL_INSERT_FOR_MED_LEC_ULTIMA = "INSERT INTO med_lec_ultima "
			+ "(id_lectura,\r\n"
			+ " id_componente,\r\n"
			+ " id_medida,\r\n"
			+ " id_empresa)\r\n"
			+ " VALUES (?, ? , ?, ?)"
			;
	
	public static final String SQLPOSTGREEQL_DELETE_FOR_MED_LEC_ULTIMA = "delete from med_lec_ultima\r\n"
			+ "where id_lectura = ?\r\n"
			+ "and id_componente = ?\r\n"
			+ "and id_medida = ?\r\n"
			+ "and id_empresa = ?"
			;
	
	@Override
	public MedLecUltimaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long ID_LECTURA = rs.getLong("ID_LECTURA");
		Long ID_COMPONENTE = rs.getLong("ID_COMPONENTE");
		Long ID_MEDIDA = rs.getLong("ID_MEDIDA");
		Long ID_EMPRESA = rs.getLong("ID_EMPRESA");
		
		return new MedLecUltimaDTO(ID_LECTURA, ID_COMPONENTE, ID_MEDIDA, ID_EMPRESA);
	}
	
}
