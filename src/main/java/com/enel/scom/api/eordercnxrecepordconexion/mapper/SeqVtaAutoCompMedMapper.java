package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SeqVtaAutoCompMedMapper implements RowMapper<Long> {

	public static final String SQLORACLE_SELECT_FOR_SEQVTAAUTOCOMPMED = "SELECT"
			+ " SQVTAAUTOCOMPMED.nextval as seqVtaAutoCompMed"
			+ " FROM dual";

	@Override
	public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long seqVtaAutoCompMed = rs.getLong("seqVtaAutoCompMed");
		return seqVtaAutoCompMed;
	}
	
}
