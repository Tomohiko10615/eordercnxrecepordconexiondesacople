package com.enel.scom.api.eordercnxrecepordconexion.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.enel.scom.api.eordercnxrecepordconexion.dto.ParametrosDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.ValorAlfDescripcionDTO;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.NucEmpresaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.SegUsuarioMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.ValorAlfDescripcionMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.ValorAlfaNumericoMapper;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.ValorNumericoMapper;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ParametrosDAO {

	@Autowired
	@Qualifier("jdbcTemplate1")
	private JdbcTemplate jdbcTemplateOracle;
	
	@Autowired
	@Qualifier("jdbcTemplate2")
	private JdbcTemplate jdbcTemplatePostgres;
	
	public ParametrosDTO obtenerParametros(String codEmpresa, String usuario) {
		ParametrosDTO parametrosDTO = new ParametrosDTO();
		
		parametrosDTO.setCodEmpresa(codEmpresa);
		parametrosDTO.setCodUsuario(usuario);
		
		String sqlNucEmpresa = NucEmpresaMapper.SQLORACLE_SELECT_FOR_IDEMPRESA;
		String sqlSegUsuario = SegUsuarioMapper.SQLORACLE_SELECT_FOR_IDUSUARIO;
		String sqlValorAlfaNumerico = ValorAlfaNumericoMapper.SQLORACLE_SELECT_FOR_VALALFNUMERICO;
		String sqlValorNumerico = ValorNumericoMapper.SQLORACLE_SELECT_FOR_VALNUMERICO;
		String sqlValorAlfDescripcion = ValorAlfDescripcionMapper.SQLORACLE_SELECT_FOR_VALALFDESCRIPCION;
		NucEmpresaMapper nucEmpresaMapper = new NucEmpresaMapper();
		SegUsuarioMapper segUsuarioMapper = new SegUsuarioMapper();
		ValorAlfaNumericoMapper valorAlfaNumericoMapper = new ValorAlfaNumericoMapper();
		ValorNumericoMapper valorNumericoMapper = new ValorNumericoMapper();
		ValorAlfDescripcionMapper valorAlfDescripcionMapper = new ValorAlfDescripcionMapper();
		
		ValorAlfDescripcionDTO valor1 = new ValorAlfDescripcionDTO();
		ValorAlfDescripcionDTO valor2 = new ValorAlfDescripcionDTO();
		ValorAlfDescripcionDTO valor3 = new ValorAlfDescripcionDTO();
		ValorAlfDescripcionDTO valor4 = new ValorAlfDescripcionDTO();
		ValorAlfDescripcionDTO valor5 = new ValorAlfDescripcionDTO();
		ValorAlfDescripcionDTO valor6 = new ValorAlfDescripcionDTO();
		ValorAlfDescripcionDTO valor7 = new ValorAlfDescripcionDTO(); // AGREGADO RSOSA INDEPE-5702 23062023
		
		Long idEmpresa = jdbcTemplateOracle.queryForObject(sqlNucEmpresa, nucEmpresaMapper, codEmpresa);
		String idUsuario = jdbcTemplateOracle.queryForObject(sqlSegUsuario, segUsuarioMapper, usuario);
		String pathCnx = jdbcTemplatePostgres.queryForObject(sqlValorAlfaNumerico, valorAlfaNumericoMapper, "EORDER", "PATH_CNX", "LGC_NC_OU");
		Long codEstProce = jdbcTemplateOracle.queryForObject(sqlValorNumerico, valorNumericoMapper, "EORDER", "ESTADO_TRANSFERENCIA", "PROCE");
		Long codEstPrece = jdbcTemplateOracle.queryForObject(sqlValorNumerico, valorNumericoMapper, "EORDER", "ESTADO_TRANSFERENCIA", "PRECE");
		Long codEstRecep = jdbcTemplateOracle.queryForObject(sqlValorNumerico, valorNumericoMapper, "EORDER", "ESTADO_TRANSFERENCIA", "RECEP");
		Long codEstRecer = jdbcTemplateOracle.queryForObject(sqlValorNumerico, valorNumericoMapper, "EORDER", "ESTADO_TRANSFERENCIA", "RECER");
		Long codEstEnvia = jdbcTemplateOracle.queryForObject(sqlValorNumerico, valorNumericoMapper, "EORDER", "ESTADO_TRANSFERENCIA", "ENVIA");
		
		try {
			valor1 = jdbcTemplateOracle.queryForObject(sqlValorAlfDescripcion, valorAlfDescripcionMapper, "EORDER", "ESTADO_ERRSYN", "ASY000");
		} catch(EmptyResultDataAccessException e) {
			valor1.setValorAlf("");
			valor1.setDescripcion("");
		}
		
		try {
			valor2 = jdbcTemplateOracle.queryForObject(sqlValorAlfDescripcion, valorAlfDescripcionMapper, "EORDER", "ESTADO_ERRSYN", "ASY013");
		} catch(EmptyResultDataAccessException e) {
			valor2.setValorAlf("");
			valor2.setDescripcion("");
		}
		
		try {
			valor3 = jdbcTemplateOracle.queryForObject(sqlValorAlfDescripcion, valorAlfDescripcionMapper, "EORDER", "ESTADO_ERRSYN", "ASY024");
		} catch(EmptyResultDataAccessException e) {
			valor3.setValorAlf("");
			valor3.setDescripcion("");
		}
		
		try { 
			valor4 = jdbcTemplateOracle.queryForObject(sqlValorAlfDescripcion, valorAlfDescripcionMapper, "EORDER", "ESTADO_ERRSYN", "ASY045");
		} catch(EmptyResultDataAccessException e) {
			valor4.setValorAlf("");
			valor4.setDescripcion("");
		}
		
		try {
			valor5 = jdbcTemplateOracle.queryForObject(sqlValorAlfDescripcion, valorAlfDescripcionMapper, "EORDER", "ESTADO_ERRSYN", "ASY099");
		} catch(EmptyResultDataAccessException e) {
			valor5.setValorAlf("");
			valor5.setDescripcion("");
		}
		
		try {
			valor6 = jdbcTemplateOracle.queryForObject(sqlValorAlfDescripcion, valorAlfDescripcionMapper, "EORDER", "ESTADO_ERRSYN", "ASY002");
		} catch(EmptyResultDataAccessException e) {
			valor6.setValorAlf("");
			valor6.setDescripcion("");
		}
		
		// INICIO RSOSA INDEPE-5702 23062023
		try {
			valor7 = jdbcTemplatePostgres.queryForObject(sqlValorAlfDescripcion, valorAlfDescripcionMapper, "EORDER",
					"ESTADO_ERRSYN", "ASY200");
		} catch (EmptyResultDataAccessException e) {
			valor7.setValorAlf("");
			valor7.setDescripcion("");
		}
		// FIN RSOSA INDEPE-5702 23062023
	
		parametrosDTO.setIdEmpresa(idEmpresa);
		parametrosDTO.setIdUsuario(idUsuario);
		parametrosDTO.setPathCnx(pathCnx);
		parametrosDTO.setCodEstPrece(codEstPrece);
		parametrosDTO.setCodEstProce(codEstProce);
		parametrosDTO.setCodEstRecep(codEstRecep);
		parametrosDTO.setCodEstRecer(codEstRecer);
		parametrosDTO.setCodEstEnvia(codEstEnvia);
		parametrosDTO.setCodErrSY000(valor1.getValorAlf());
		parametrosDTO.setDesErrSY000(valor1.getDescripcion());
		parametrosDTO.setCodErrSY002(valor6.getValorAlf());
		parametrosDTO.setDesErrSY002(valor6.getDescripcion());
		parametrosDTO.setCodErrSY013(valor2.getValorAlf());
		parametrosDTO.setDesErrSY013(valor2.getDescripcion());
		parametrosDTO.setCodErrSY024(valor3.getValorAlf());
		parametrosDTO.setDesErrSY024(valor3.getDescripcion());
		parametrosDTO.setCodErrSY045(valor4.getValorAlf());
		parametrosDTO.setDesErrSY045(valor4.getDescripcion());
		parametrosDTO.setCodErrSY099(valor5.getValorAlf());
		parametrosDTO.setDesErrSY099(valor5.getDescripcion());
		parametrosDTO.setCodErrSY200(valor7.getValorAlf()); // AGREGADO RSOSA INDEPE-5702 23062023
		parametrosDTO.setDesErrSY200(valor7.getDescripcion()); // AGREGADO RSOSA INDEPE-5702 23062023
		
		log.info("ParametroDTO: {}", parametrosDTO);
		
		return parametrosDTO;
	}
}
