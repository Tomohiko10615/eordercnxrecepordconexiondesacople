package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class OrdServVentaMapper implements RowMapper<String> {

	public static final String SQLORACLE_SELECT_FOR_ORDENSRVVENTA = "select distinct eot.nro_orden_lgc_relac orden  from eor_ord_transfer eot \r\n" // cfdiaze ini correcion INC000104436236
			+ "where nro_orden_legacy = ? and cod_tipo_orden_eorder in ('NCX.01', 'NCX.02')";

	@Override
	public String mapRow(ResultSet rs, int rowNum) throws SQLException {
		String orden = rs.getString("orden");
		return orden;
	}
	
}
