package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.LecturasMedDTO;

public class LecturasMedMapper implements RowMapper<LecturasMedDTO> {
	
	public static final String SQLPOSTGRESQL_SELECT_FOR_LECTURAMED = " SELECT\r\n"
			+ "			 unnest(xpath('/item/tipoLectura/text()', d.REG_XML))::text AS tipoLectura,\r\n"
			+ "			 unnest(xpath('/item/estadoLeido/text()', d.REG_XML))::text AS lectura,\r\n"
			+ "			 unnest(xpath('/item/horarioLectura/text()', d.REG_XML))::text AS horarioLectura,\r\n"
			+ "			 unnest(xpath('/item/fechaLectura/text()', d.REG_XML))::text AS fechaLectura\r\n"
			+ "			 FROM (SELECT  unnest(xpath('/lecturas/item', unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores/item['||?||']/lecturas', d.REG_XML)))) AS REG_XML  \r\n"
			+ "                    FROM SCHSCOM.EOR_ORD_TRANSFER_DET d  \r\n"
			+ "                    WHERE d.id_ord_transfer =   ?\r\n"
			+ "                    	AND accion = 'RECEPCION'  \r\n"
			+ "                    	AND nro_evento = ?)d ";

	
/*
	public static final String SQLPOSTGRESQL_SELECT_FOR_LECTURAMED = "SELECT"
			+ " unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores/item[?]/lecturas/item/tipoLectura/text()', d.REG_XML))::text AS tipoLectura,"
			+ " unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores/item[?]/lecturas/item/estadoLeido/text()', d.REG_XML))::text AS lectura,"
			+ "	unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores/item[?]/lecturas/item/horarioLectura/text()', d.REG_XML))::text AS horarioLectura,"
			+ " unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores/item[?]/lecturas/item/fechaLectura/text()', d.REG_XML))::text AS fechaLectura"
			+ "	from schscom.eor_ord_transfer_det d"
			+ "	where d.id_ord_transfer = ?"
			+ " and accion = 'RECEPCION'"
			+ "	and nro_evento = ?";
*/

	@Override
	public LecturasMedDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		String tipoLectura = rs.getString("tipoLectura");
		String lectura = rs.getString("lectura");
		String horarioLectura = rs.getString("horarioLectura");
		String fechaLectura = rs.getString("fechaLectura");
		return new LecturasMedDTO(tipoLectura, lectura, horarioLectura, fechaLectura);
	}
	
}
