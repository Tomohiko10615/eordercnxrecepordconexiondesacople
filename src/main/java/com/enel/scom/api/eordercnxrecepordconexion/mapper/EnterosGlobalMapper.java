package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


public class EnterosGlobalMapper implements RowMapper<Integer>{
	// INICIO RSOSA INDEPE-5702 19/06/2023
	public static final String SQLPOSTGRE_SELECT_FOR_COUNT_ESTADO_ORDCNXPORSUSP = "SELECT COUNT(*) as count"
			+ "		FROM EOR_ORD_TRANSFER OT"
			+ "		WHERE"
			+ "		OT.COD_TIPO_ORDEN_LEGACY = 'OCNX'"
			+ "		AND OT.NRO_ORDEN_LGC_RELAC = ?"
			+ "		AND OT.SUSPENDIDA = 'N'";
	
	@Override
	public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Integer count = rs.getInt("count");
		return count;
	}
	// FIN RSOSA INDEPE-5702 19/06/2023
}
