package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class BuscarMedidasMapper implements RowMapper<Long> {

	public static final String SQLPOSTGRE_SELECT_FOR_IDMEDIDA = "SELECT"
			+ " id_medida"
			+ "	FROM med_medida_medidor a, med_componente b"
			+ "	WHERE a.id_componente=b.id"
			+ "	AND a.id_componente = ?";

	@Override
	public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long idMedida = rs.getLong("id_medida");
		return idMedida;
	}
	
}
