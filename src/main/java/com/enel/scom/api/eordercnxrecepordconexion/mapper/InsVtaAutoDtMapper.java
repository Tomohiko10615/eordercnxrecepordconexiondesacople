package com.enel.scom.api.eordercnxrecepordconexion.mapper;

public class InsVtaAutoDtMapper {

	public static final String SQLORACLE_INSERT_FOR_VTAAUTODT = "INSERT INTO vta_auto_dt"
			+ " (id_auto_dt, id_auto_act, id_srv_venta, id_punto_entrega,"
			+ "	propiedad_empalme, capacidad_interruptor, sistema_proteccion, caja_medicion,"
			+ "	longitud_acometida, id_set, id_alimentador, id_sed, id_llave,"
			+ "	tipo_conductor, id_empresa)"
			+ " VALUES(?, ?, ?, ?,"
			+ "	?, ?, ?, ?,"
			+ "	?, ?, ?, ?, ?,"
			+ "	?,3)";
}
