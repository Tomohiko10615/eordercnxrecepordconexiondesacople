package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.OrdenVentaDTO;

public class OrdenVentaMapper implements RowMapper<OrdenVentaDTO> {

	public static final String SQLORACLE_SELECT_FOR_ORDENVENTA = "SELECT"
			+ " OOVEN.ID_ORDEN as idOrdVta, OOVEN.NRO_ORDEN as nroOrdenVta,"
			+ " OVEN.PARALIZADA as indParalizada, TRIM(WORD.ID_STATE) as estadoOven"
			+ " FROM ORD_ORDEN OOVEN, VTA_ORD_VTA OVEN, ORD_CONEX OCNX, WKF_WORKFLOW WORD"
			+ " WHERE OOVEN.ID_ORDEN = OVEN.ID_ORD_VENTA"
			+ " AND OVEN.ID_ORD_VENTA = OCNX.ID_ORD_VENTA"
			+ " AND OOVEN.ID_WORKFLOW = WORD.ID_WORKFLOW"
			+ " AND OCNX.NRO_ORDEN = ?" // orden conexion
			+ " AND OOVEN.NRO_ORDEN = ?"; // orden venta (nuevo)

	@Override
	public OrdenVentaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long idOrdVta = rs.getLong("idOrdVta");
		String nroOrdenVta = rs.getString("nroOrdenVta");
		String indParalizada = rs.getString("indParalizada");
		String estadoOven = rs.getString("estadoOven");
		return new OrdenVentaDTO(idOrdVta, nroOrdenVta, indParalizada, estadoOven);
	}
	
	
}
