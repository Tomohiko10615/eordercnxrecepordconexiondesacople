package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.FwkAuditEventDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.MedComponenteDTO;

public class MedComponenteMapper implements RowMapper<MedComponenteDTO> {

	public static final String SQLORACLESQL_SELECT_FOR_MED_COMPONENTE = "SELECT  "
			+ "ID_COMPONENTE     ,\r\n"
			+ "ID_EMPRESA               ,\r\n"
			+ "ID_MODELO                ,\r\n"
			+ "ID_EST_COMPONENTE        ,\r\n"
			+ "FEC_CREACION             ,\r\n"
			+ "ID_PROPIEDAD             ,\r\n"
			+ "ANO_FABRICACION          ,\r\n"
			+ "NRO_FABRICA              ,\r\n"
			+ "ID_ACCION_REALIZADA      ,\r\n"
			+ "ID_COND_CREACION         ,\r\n"
			+ "ES_CONTROL_INTERNO       ,\r\n"
			+ "ES_PROTOTIPO             ,\r\n"
			+ "ID_TIP_TRANSFOR          ,\r\n"
			+ "COD_TIP_COMPONENTE       ,\r\n"
			+ "ID_EQUIPO                ,\r\n"
			+ "ID_RESP_MEDICION         ,\r\n"
			+ "ID_MOD_MEDICION          ,\r\n"
			+ "TEL_MEDICION             ,\r\n"
			+ "UBI_INSTALACION          ,\r\n"
			+ "ID_UBICACION             ,\r\n"
			+ "TYPE_UBICACION           ,\r\n"
			+ "ID_DYNAMICOBJECT         ,\r\n"
			+ "HORA_RELOJ               ,\r\n"
			+ "HORA_VERIF               ,\r\n"
			+ "ID_RELACION              ,\r\n"
			+ "ID_NEV                   ,\r\n"
			+ "ID_TIP_MEDIDA            ,\r\n"
			+ "NRO_COMPONENTE           ,\r\n"
			+ "UBICACION_INST_MEDIDOR   ,\r\n"
			+ "FECHA_SUMINISTRO         ,\r\n"
			+ "SUMINISTRADOR_COMPONENTE ,\r\n"
			+ "REACONDICIONADO          ,\r\n"
			+ "ID_MOTIVO_BAJA           ,\r\n"
			+ "FACTOR_INTERNO           ,\r\n"
			+ "SERIAL_NUMBER\r\n"
			+ "FROM med_componente\r\n"
			+ "WHERE ID_COMPONENTE  = ? "
			;
	
	public static final String SQLPOSTGREEQL_UPDATE_FOR_MED_COMPONENTE = "update schscom.med_componente\r\n"
			+ "set \r\n"
			+ "id_modelo               = ? ,\r\n"
			+ "id_est_componente       = ? ,\r\n"
			+ "fec_creacion            = ? ,\r\n"
			+ "id_propiedad            = ? ,\r\n"
			+ "ano_fabricacion         = ? ,\r\n"
			+ "nro_fabrica             = ? ,\r\n"
			+ "id_accion_realizada     = ? ,\r\n"
			+ "id_cond_creacion        = ? ,\r\n"
			+ "es_control_interno      = ? ,\r\n"
			+ "es_prototipo            = ? ,\r\n"
			+ "id_tip_transfor         = ? ,\r\n"
			+ "cod_tip_componente      = ? ,\r\n"
			+ "id_equipo               = ? ,\r\n"
			+ "id_resp_medicion        = ? ,\r\n"
			+ "id_mod_medicion         = ? ,\r\n"
			+ "tel_medicion            = ? ,\r\n"
			+ "ubi_instalacion         = ? ,\r\n"
			+ "id_ubicacion            = ? ,\r\n"
			+ "type_ubicacion          = ? ,\r\n"
			+ "id_dynamicobject        = ? ,\r\n"
			+ "hora_reloj              = ? ,\r\n"
			+ "hora_verif              = ? ,\r\n"
			+ "id_relacion             = ? ,\r\n"
			+ "id_nev                  = ? ,\r\n"
			+ "id_tip_medida           = ? ,\r\n"
			+ "nro_componente          = ? ,\r\n"
			+ "ubicacion_inst_medidor  = ? ,\r\n"
			+ "fecha_suministro        = ? ,\r\n"
			+ "suministrador_componente= ? ,\r\n"
			+ "reacondicionado         = ? ,\r\n"
			+ "id_motivo_baja          = ? ,\r\n"
			+ "factor_interno          = ? ,\r\n"
			+ "serial_number           = ? \r\n"
//			+ "id_usuario_registro     = ? ,\r\n"
//			+ "fec_registro            = ? ,\r\n"
//			+ "id_usuario_modif        = ? ,\r\n"
//			+ "fec_modif               = ? ,\r\n"
//			+ "activo                  = ? ,\r\n"
//			+ "serie                   = ?  \r\n"
			+ "where id = ?"
			;
	
			@Override
			public MedComponenteDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

				Long ID_COMPONENTE = rs.getLong("ID_COMPONENTE");
				Long ID_EMPRESA = rs.getLong("ID_EMPRESA");
				Long ID_MODELO = rs.getLong("ID_MODELO");
				Long ID_EST_COMPONENTE = rs.getLong("ID_EST_COMPONENTE");
				Date FEC_CREACION = rs.getDate("FEC_CREACION");
				Long ID_PROPIEDAD = rs.getLong("ID_PROPIEDAD");
				Long ANO_FABRICACION = rs.getLong("ANO_FABRICACION");
				Long NRO_FABRICA = rs.getLong("NRO_FABRICA");
				Long ID_ACCION_REALIZADA = rs.getLong("ID_ACCION_REALIZADA");
				Long ID_COND_CREACION = rs.getLong("ID_COND_CREACION");
				String ES_CONTROL_INTERNO = rs.getString("ES_CONTROL_INTERNO");
				String ES_PROTOTIPO = rs.getString("ES_PROTOTIPO");
				Long ID_TIP_TRANSFOR = rs.getLong("ID_TIP_TRANSFOR");
				String COD_TIP_COMPONENTE = rs.getString("COD_TIP_COMPONENTE");
				Long ID_EQUIPO = rs.getLong("ID_EQUIPO");
				Long ID_RESP_MEDICION = rs.getLong("ID_RESP_MEDICION");
				Long ID_MOD_MEDICION = rs.getLong("ID_MOD_MEDICION");
				String TEL_MEDICION = rs.getString("TEL_MEDICION");
				String UBI_INSTALACION = rs.getString("UBI_INSTALACION");
				Long ID_UBICACION = rs.getLong("ID_UBICACION");
				String TYPE_UBICACION = rs.getString("TYPE_UBICACION");
				Long ID_DYNAMICOBJECT = rs.getLong("ID_DYNAMICOBJECT");
				Date HORA_RELOJ = rs.getDate("HORA_RELOJ");
				Date HORA_VERIF = rs.getDate("HORA_VERIF");
				Long ID_RELACION = rs.getLong("ID_RELACION");
				Long ID_NEV = rs.getLong("ID_NEV");
				Long ID_TIP_MEDIDA = rs.getLong("ID_TIP_MEDIDA");
				String NRO_COMPONENTE = rs.getString("NRO_COMPONENTE");
				String UBICACION_INST_MEDIDOR = rs.getString("UBICACION_INST_MEDIDOR");
				Date FECHA_SUMINISTRO = rs.getDate("FECHA_SUMINISTRO");
				String SUMINISTRADOR_COMPONENTE = rs.getString("SUMINISTRADOR_COMPONENTE");
				String REACONDICIONADO = rs.getString("REACONDICIONADO");
				Long ID_MOTIVO_BAJA = rs.getLong("ID_MOTIVO_BAJA");
				Long FACTOR_INTERNO = rs.getLong("FACTOR_INTERNO");
				String SERIAL_NUMBER = rs.getString("SERIAL_NUMBER");

				return new MedComponenteDTO(ID_COMPONENTE, ID_EMPRESA, ID_MODELO, ID_EST_COMPONENTE, FEC_CREACION,
						ID_PROPIEDAD, ANO_FABRICACION, NRO_FABRICA, ID_ACCION_REALIZADA, ID_COND_CREACION,
						ES_CONTROL_INTERNO, ES_PROTOTIPO, ID_TIP_TRANSFOR, COD_TIP_COMPONENTE, ID_EQUIPO,
						ID_RESP_MEDICION, ID_MOD_MEDICION, TEL_MEDICION, UBI_INSTALACION, ID_UBICACION, TYPE_UBICACION,
						ID_DYNAMICOBJECT, HORA_RELOJ, HORA_VERIF, ID_RELACION, ID_NEV, ID_TIP_MEDIDA, NRO_COMPONENTE,
						UBICACION_INST_MEDIDOR, FECHA_SUMINISTRO, SUMINISTRADOR_COMPONENTE, REACONDICIONADO,
						ID_MOTIVO_BAJA, FACTOR_INTERNO, SERIAL_NUMBER);
			}
	
}
