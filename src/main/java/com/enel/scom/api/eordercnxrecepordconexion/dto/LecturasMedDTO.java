package com.enel.scom.api.eordercnxrecepordconexion.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LecturasMedDTO {

	private String tipoLectura;
	private String lectura;
	private String horarioLectura;
	private String fechaLectura;
}
