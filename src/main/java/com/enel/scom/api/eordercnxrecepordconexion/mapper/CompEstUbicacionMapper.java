package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.CompEstUbicacionDTO;

public class CompEstUbicacionMapper implements RowMapper<CompEstUbicacionDTO> {

	public static final String SQLPOSTGRE_SELECT_FOR_COMPEST = "SELECT"
			+ "    a.id as idComponente,"
			+ "    a.id_est_componente as idEstComp,"
			+ "    a.id_ubicacion as idServicio,"
			+ "    a.type_ubicacion as tipoUbic"
			+ " FROM"
			+ "    med_componente a,"
			+ "    med_modelo b,"
			+ "    med_marca c"
			+ " WHERE"
			+ "    a.id_modelo = b.id"
			+ "    AND b.id_marca = c.id"
			+ "    AND b.cod_modelo = ?"
			+ "    AND c.cod_marca = ?"
			+ "    AND a.nro_componente = ?"
			+ "    AND a.cod_tip_componente = 'ME'";

	@Override
	public CompEstUbicacionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long idComponente = rs.getLong("idComponente");
		Long idEstComp = rs.getLong("idEstComp");
		Long idServicio = rs.getLong("idServicio");
		String tipoUbic = rs.getString("tipoUbic");
		return new CompEstUbicacionDTO(idComponente, idEstComp, idServicio, tipoUbic);
	}
	
}
