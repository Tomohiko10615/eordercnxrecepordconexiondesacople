package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SrvSetMapper implements RowMapper<Long> {

	public static final String SQLORACLE_SELECT_FOR_IDSET = "SELECT"
			+ " id_set"
			+ " FROM srv_set"
			+ " WHERE cod_set = ?"
			+ "	AND activo = 'S'";

	@Override
	public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long idSet = rs.getLong("id_set");
		return idSet;
	}
	
}
