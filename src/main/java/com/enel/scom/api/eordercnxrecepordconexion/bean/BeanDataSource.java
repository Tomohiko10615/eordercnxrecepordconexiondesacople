package com.enel.scom.api.eordercnxrecepordconexion.bean;

import java.sql.Connection;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class BeanDataSource {
	
	private Connection  conexion; 
	private String dbDriver;
	private String dbURL;
	private String dbUser;
	private String dbPass;
	private int dbMax;
	private int dbMin;
	
	
}
