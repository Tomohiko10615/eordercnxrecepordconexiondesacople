package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.TablaDTO;

public class TablaMapper implements RowMapper<TablaDTO>{
	public static final String SQLORACLE_SELECT_FOR_TABLA = "SELECT COUNT(1) count FROM %s WHERE TRIM(UPPER(%s)) = TRIM(UPPER(?))";
	@Override
	public TablaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		TablaDTO tabla= new TablaDTO();
		tabla.setCount(rs.getInt("count"));
		return tabla;
	}
}
