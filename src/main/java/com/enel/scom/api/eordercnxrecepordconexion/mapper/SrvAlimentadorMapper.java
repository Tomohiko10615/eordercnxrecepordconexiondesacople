package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SrvAlimentadorMapper implements RowMapper<Long> {

	public static final String SQLORACLE_SELECT_FOR_SRVALIMENTADOR = "SELECT"
			+ " id_alimentador"
			+ " FROM srv_alimentador"
			+ " WHERE cod_alimentador = ?"
			+ " AND id_set= ?"
			+ " AND activo = 'S'";

	@Override
	public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long idAlimentador = rs.getLong("id_alimentador");
		return idAlimentador;
	}
	
	
}
