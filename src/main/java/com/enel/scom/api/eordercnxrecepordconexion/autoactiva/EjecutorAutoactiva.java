package com.enel.scom.api.eordercnxrecepordconexion.autoactiva;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.enel.scom.api.eordercnxrecepordconexion.dto.ParametrosDTO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EjecutorAutoactiva {

	private EjecutorAutoactiva() {
		super();
	}
	
	private static final String PATH_EJECUTABLE = "/binarios/ejecutables/SCOM/ODT/VTA_Auto_Activa2.jar";

	public static int ejecutar(String nombreArchivo, ParametrosDTO parametros) throws InterruptedException {

		ProcessBuilder pb = new ProcessBuilder(
        		"java",
        		"-jar",
        		PATH_EJECUTABLE,
        		parametros.getNroOrden().toString(),
        		parametros.getPathCnx(),
        		nombreArchivo,
        		"EORDER"
        		);
		
        Process process = null;
        
		try {
			process = pb.start();
			imprimirSalidaConsola(process);
			process.waitFor();
		} catch (IOException e) {
			e.printStackTrace();
			log.error("Error al ejecutar el proceso externo.");
			System.exit(1);
		}
		return process.exitValue();
	}

	private static void imprimirSalidaConsola(Process process) {
		InputStream is = process.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line = null;
		try {
			while ((line = br.readLine()) != null) {
				log.info(line);
			}
		} catch (IOException e) {
			log.error("Error al imprimir línea.");
			e.printStackTrace();
		}
	}
}
