package com.enel.scom.api.eordercnxrecepordconexion.mapper;

public class UpdEorOrdTransferDetMapper {

	public static final String SQLPOSTGRE_UPDATE_FOR_ERRORORORDTRANSFERDET = "UPDATE"
			+ " schscom.eor_ord_transfer_det"
			+ " SET cod_error = ?"
			+ "	WHERE ID_ORD_TRANSFER = ?"
			+ "	AND accion = 'RECEPCION'"
			+ "	AND NRO_EVENTO = ?";
	
	// INICIO RSOSA INDEPE-5702 23062023
	public static final String SQLPOSTGRE_UPDATE_FOR_ORORDTRANSFERDET = "UPDATE"
			+ " schscom.eor_ord_transfer_det"
			+ " SET cod_error = ?"
			+ "	WHERE ID_ORD_TRANSFER = ?"
			+ "	AND accion = 'RECEPCION'"
			+ "	AND NRO_EVENTO = ?";
	// FIN RSOSA INDEPE-5702 23062023

}
