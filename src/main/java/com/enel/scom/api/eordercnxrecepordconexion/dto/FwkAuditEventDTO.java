package com.enel.scom.api.eordercnxrecepordconexion.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FwkAuditEventDTO {

	private Long id;
	private String usecase;
	private String objectref;
	private Long id_fk;
	private Date fecha_ejecucion;
	private String specific_auditevent;
	private Long id_user;
	
}
