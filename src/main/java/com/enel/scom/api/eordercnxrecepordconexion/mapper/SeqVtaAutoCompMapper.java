package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SeqVtaAutoCompMapper implements RowMapper<Long> {

	public static final String SQLORACLE_SELECT_FOR_SEQVTAAUTOCOMP = "SELECT"
			+ " SQVTAAUTOCOMP.nextval as seqVtaAutoComp"
			+ " FROM dual";

	@Override
	public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long seqVtaAutoComp = rs.getLong("seqVtaAutoComp");
		return seqVtaAutoComp;
	}
	
}
