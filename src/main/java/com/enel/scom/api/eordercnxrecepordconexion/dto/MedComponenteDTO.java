package com.enel.scom.api.eordercnxrecepordconexion.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MedComponenteDTO {

	private Long id_componente;
	private Long id_empresa;
	private Long id_modelo;
	private Long id_est_componente;
	private Date fec_creacion;
	private Long id_propiedad;
	private Long ano_fabricacion;
	private Long nro_fabrica;
	private Long id_accion_realizada;
	private Long id_cond_creacion;
	private String es_control_interno;
	private String es_prototipo;
	private Long id_tip_transfor;
	private String cod_tip_componente;
	private Long id_equipo;
	private Long id_resp_medicion;
	private Long id_mod_medicion;
	private String tel_medicion;
	private String ubi_instalacion;
	private Long id_ubicacion;
	private String type_ubicacion;
	private Long id_dynamicobject;
	private Date hora_reloj;
	private Date hora_verif;
	private Long id_relacion;
	private Long id_nev;
	private Long id_tip_medida;
	private String nro_componente;
	private String ubicacion_inst_medidor;
	private Date fecha_suministro;
	private String suministrador_componente;
	private String reacondicionado;
	private Long id_motivo_baja;
	private Long factor_interno;
	private String serial_number;

}
