package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.AuditoriaDTO;

public class AuditoriaMapper implements RowMapper<AuditoriaDTO>{

	public static final String SQLORACLE_SELECT_FOR_AUDITORIA = "SELECT"
			+ " estado_destino as estadoAct, tipo_obra as tipoObra, id_segmento as idSegmento"
			+ "	FROM (SELECT estado_destino, tipo_obra, id_segmento"
			+ "	FROM vta_aud_ord"
			+ "	WHERE id_ord_venta = ?"
			+ "	ORDER BY id_aud_ord desc )"
			+ " WHERE rownum=1";

	@Override
	public AuditoriaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		String estadoAct = rs.getString("estadoAct");
		String tipoObra = rs.getString("tipoObra"); 
		Long idSegmento = rs.getLong("idSegmento");
		return new AuditoriaDTO(estadoAct, tipoObra, idSegmento);
	}
	
}
