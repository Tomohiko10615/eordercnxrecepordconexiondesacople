package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.ValorAlfDescripcionDTO;

public class ValorAlfDescripcionMapper implements RowMapper<ValorAlfDescripcionDTO> {

	public static final String SQLORACLE_SELECT_FOR_VALALFDESCRIPCION = "SELECT"
			+ " VALOR_ALF, DESCRIPCION"
			+ " FROM COM_PARAMETROS"
			+ " WHERE SISTEMA = ?"
			+ " AND ENTIDAD = ?"
			+ " AND CODIGO  = ?";

	@Override
	public ValorAlfDescripcionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		String valorAlf = rs.getString("VALOR_ALF");
		String descripcion = rs.getString("DESCRIPCION");
		return new ValorAlfDescripcionDTO(valorAlf, descripcion);
	}
	
}
