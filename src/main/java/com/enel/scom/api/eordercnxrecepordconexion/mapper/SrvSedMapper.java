package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SrvSedMapper implements RowMapper<Long> {

	public static final String SQLORACLE_SELECT_FOR_SRVSED = "SELECT"
			+ " id_sed"
			+ " FROM srv_sed"
			+ " WHERE cod_sed = ?"
			+ " AND id_alimentador = ?"
			+ " AND activo = 'S'";

	@Override
	public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long idSed = rs.getLong("idSed");
		return idSed;
	}
	
	
}
