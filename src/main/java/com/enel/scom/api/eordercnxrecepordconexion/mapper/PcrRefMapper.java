package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.PcrRefDTO;

public class PcrRefMapper implements RowMapper<PcrRefDTO> {

	public static final String SQLORACLE_SELECT_FOR_PCRREF = "SELECT"
			+ " c.id_pcr as idPcr, d.id_tension as idTension"
			+ " FROM nuc_cuenta a, nuc_servicio b, srv_electrico c, srv_pcr d"
			+ " WHERE a.id_cuenta = b.id_cuenta"
			+ " AND b.id_servicio=c.id_servicio"
			+ " AND c.id_pcr = d.id_pcr"
			+ " AND b.tipo = 'ELECTRICO'"
			+ " AND c.id_estado in(0, 5, 4, 3)"
			+ " AND a.nro_cuenta = ?";
	
	public static final String SQLORACLE_SELECT_FOR_PCRREF2 = "SELECT"
			+ " pcr.id_pcr as idPcr, pcr.id_tension as idTension"
			+ "	FROM nuc_cuenta a, vta_sol_srv_ele sele, vta_srv_venta sven, vta_auto_dt dt, srv_pcr pcr"
			+ "	WHERE a.id_cuenta = sele.id_cuenta_fac"
			+ "	AND sele.id_sol_srv_ele = sven.id_sol_srv_ele"
			+ "	AND sven.id_srv_venta = dt.id_srv_venta"
			+ "	AND dt.id_pcr = pcr.id_pcr"
			+ "	AND a.nro_cuenta = ?";
	
	public static final String SQLORACLE_SELECT_FOR_PCRREF3 = "SELECT"
			+ " id_pcr as idPcr, id_tension as idTension"
			+ " FROM srv_pcr"
			+ " WHERE num_pcr = ?";
	

	@Override
	public PcrRefDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long idPcr = rs.getLong("idPcr");
		Long idTension = rs.getLong("idTension");
		return new PcrRefDTO(idPcr, idTension);
	}
	
}
