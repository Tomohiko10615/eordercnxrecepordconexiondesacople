package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.SuministroDTO;

public class SuministroMapper implements RowMapper<SuministroDTO> {

	public static final String SQLORACLE_SELECT_FOR_SUMINISTRO = "SELECT"
			+ " b.id_servicio as idRegistro, nvl(c.id_alimentador,0) as idAlim,"
			+ "	nvl(c.id_sed,0) as idSed, nvl(c.id_llave,0) as idLlave, nvl(c.id_ruta_lectura,0) as idRutaSumin"
			+ " FROM nuc_cuenta a, nuc_servicio b, srv_electrico c"
			+ " WHERE a.id_cuenta = b.id_cuenta"
			+ " AND b.id_servicio=c.id_servicio"
			+ " AND b.tipo = 'ELECTRICO'"
			+ " AND c.id_estado in(0, 5, 4)"
			+ " AND a.nro_cuenta = ?";

	@Override
	public SuministroDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long idRegistro = rs.getLong("idRegistro");
		Long idAlim = rs.getLong("idAlim");
		Long idSed = rs.getLong("idSed");
		Long idLlave = rs.getLong("idLlave");
		Long idRutaSumin = rs.getLong("idRutaSumin");
		return new SuministroDTO(idRegistro, idAlim, idSed, idLlave, idRutaSumin);
	}
	
}
