package com.enel.scom.api.eordercnxrecepordconexion.service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.eordercnxrecepordconexion.util.Log;

import lombok.extern.slf4j.Slf4j;

import com.enel.scom.api.eordercnxrecepordconexion.autoactiva.Autoactiva;
import com.enel.scom.api.eordercnxrecepordconexion.dao.AutoActivaDAO;
import com.enel.scom.api.eordercnxrecepordconexion.dao.FechaHoraDAO;
import com.enel.scom.api.eordercnxrecepordconexion.dao.OrdenesDAO;
import com.enel.scom.api.eordercnxrecepordconexion.dao.ParametrosDAO;
import com.enel.scom.api.eordercnxrecepordconexion.dao.ProcesoPrincipalDAO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.DatosOrdenDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.FechaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.ParametrosDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.RegistroOrdenDTO;
import com.enel.scom.api.eordercnxrecepordconexion.util.ArchivoUtil;
import com.enel.scom.api.eordercnxrecepordconexion.util.Constante;

@Slf4j
@Service
public class OrdRecepConexionService {

	@Autowired
	ParametrosDAO parametrosDAO;
	
	@Autowired
	OrdenesDAO ordenesDAO;
	
	@Autowired
	ProcesoPrincipalDAO procesoPrincipalDAO;
	
	@Autowired
	AutoActivaDAO autoActivaDAO;
	
	@Autowired
	FechaHoraDAO fechaHoraDAO;
	
	public void procesar(String codEmpresa, String usuario, Long nroOrden, String rutaLog) {
		FechaDTO fecha = fechaHoraDAO.getFechaHoy("YYYY-MM-DD");
		Constante constante = new Constante();
		
		Log.mbfnOpenFileError2(String.format("%sRes_AutoActivar_%s.xls",rutaLog,fecha.getFechaHora()));
		/*Log.logError("hola1");
		Log.logError2("hola2");
		Log.ifnMsgToLogFile("hola3");
		CrearArchivos(rutaLog,nroOrden);
		ArchivoUtil.file2Writer.println("hola4");
		exit(0);*/
		//////////////////////////////
		log.info("Inicio obtenerParametros");
		ParametrosDTO parametros = parametrosDAO.obtenerParametros(codEmpresa, usuario);
		//ArchivoUtil.crearArchivo(parametros.getPathCnx(), nroOrden);
		parametros.setNroOrden(nroOrden);
		log.info("Fin obtenerParametros , parametros obtenidos : " + parametros);
		
		System.out.println(String.format("Inicio: %s \n",fecha.getFechaHora()));
		
		log.info("Inicio ObtieneOrden");
		List<DatosOrdenDTO> lista = ordenesDAO.ObtieneOrden(parametros);//ObtieneOrden
		log.info("Cantidad de ordenes a recepcionar: {}", lista.size());
		log.info("Fin ObtieneOrden");
		// System.exit(1);
		if(lista.size()==0)
		{
			String var=String.format("ODT %d\t%s\tFila: 0 \tError: No se encontraron registros para procesar \n", nroOrden, "CNXRECEPORDCONEXION");
			System.out.println(var);
			Log.logError(var);
		}
		else
		{
			CrearArchivos(rutaLog,nroOrden);
			log.info("*****INICIO LogicaControlProceso******");
			List<RegistroOrdenDTO> listRegistro = procesoPrincipalDAO.LogicaControlProceso(lista, parametros,nroOrden);
			//autoActivaDAO.procesoAutoActiva(listRegistro, parametros);
			/*
			System.out.println("Cantidad Registros Procesados Correctamente: %d \n", lCant_Reg_Cargados);
			System.out.println("Cantidad Registros Recepcionados: %d \n", lCant_Reg_Leidos);
			System.out.println("Cantidad Registros con Error: %d \n", (lCant_Reg_Leidos-lCant_Reg_Cargados));
			*/
		}
		fecha= new FechaDTO();
		fecha = fechaHoraDAO.getFechaHoy("YYYY-MM-DD");
		System.out.println(String.format("Fin: %s \n",fecha.getFechaHora()));
		System.out.println("\nTerminó OK\n");
	}
	
	private static void exit(int estado)
	{
		Log.bfnCloseLogFile();
		Log.bfnCloseLogFileError();
		Log.bfnCloseLogFileError2();
		ArchivoUtil.file2Writer.close();
		System.exit(estado);
	}
	private void CrearArchivos(String Path_Procesado, long NroODT)
	{
		ArchivoUtil.crearArchivo(Path_Procesado,NroODT);//sEOR_CNX_ORD_CONEXION_PENDIENTES_ACTIVAR		
		//public static FileWriter file2;
		//public static PrintWriter file2Writer;
		//f_repR
	}
}
