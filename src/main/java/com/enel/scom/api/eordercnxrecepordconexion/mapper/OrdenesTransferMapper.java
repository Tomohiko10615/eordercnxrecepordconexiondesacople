package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.OrdenesTransferDTO;

public class OrdenesTransferMapper implements RowMapper<OrdenesTransferDTO> {

	public static final String SQLPOSTGRESQL_SELECT_FOR_ORDTRANSFER = "SELECT"
			+ " OT.ID_ORD_TRANSFER as idOrdTransfer, OT.NRO_RECEPCIONES as nroRecepciones"
			+ " FROM schscom.EOR_ORD_TRANSFER OT, schscom.EOR_ORD_TRANSFER_DET OTD"
			+ " WHERE COD_ESTADO_ORDEN = ?"
			+ " AND COD_TIPO_ORDEN_EORDER in ('NCX.01', 'NCX.02', 'NCX.04')"
			+ " AND OTD.ID_ORD_TRANSFER = OT.ID_ORD_TRANSFER"
			+ " AND OTD.ACCION= 'RECEPCION'"
			+ " AND OTD.NRO_EVENTO = OT.NRO_RECEPCIONES"
			// + " AND OT.ID_ORD_TRANSFER = 21568960" //borrar/comentar
			//Para pruebas
			//+ " LIMIT 15"
			;
	
	@Override
	public OrdenesTransferDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long idOrdTransfer = rs.getLong("idOrdTransfer");
		Long nroRecepciones = rs.getLong("nroRecepciones");
		return new OrdenesTransferDTO(idOrdTransfer, nroRecepciones);
	}
	
}
