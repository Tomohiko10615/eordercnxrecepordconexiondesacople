package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.ProductoSrvVentaDTO;

public class ProductoSrvVentaMapper implements RowMapper<ProductoSrvVentaDTO>{

	public static final String SQLORACLE_SELECT_FOR_PRODUCTOSRVVENTA = "SELECT"
			+ " PROD.ID_PRODUCTO as idProducto, PROD.TIP_PRODUCTO as tipProducto"
			+ "	FROM ORD_ORDEN ORD, ORD_CONEX OCNX, VTA_SRV_VENTA SVEN, VTA_SOL_SRV_ELE SELE, PRD_PRODUCTO PROD"
			+ "	WHERE ORD.ID_ORDEN = OCNX.ID_ORD_VENTA"
			+ "	AND ORD.ID_ORDEN = SVEN.ID_ORD_VENTA"
			+ "	AND OCNX.ID_ORD_CONEX = SVEN.ID_ORD_CONEX"
			+ "	AND SVEN.ID_SOL_SRV_ELE = SELE.ID_SOL_SRV_ELE"
			+ "	AND SVEN.ID_PRODUCTO = PROD.ID_PRODUCTO"
			+ "	AND OCNX.NRO_ORDEN = ?"
			+ "	AND ORD.NRO_ORDEN = ?";

	@Override
	public ProductoSrvVentaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long idProducto = rs.getLong("idProducto");
		String tipProducto = rs.getString("tipProducto");
		return new ProductoSrvVentaDTO(idProducto, tipProducto);
	}
	
	
}
