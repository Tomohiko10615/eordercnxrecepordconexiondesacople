package com.enel.scom.api.eordercnxrecepordconexion.autoactiva;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import com.enel.scom.api.eordercnxrecepordconexion.dto.ParametrosDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.RegistroOrdenDTO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GeneradorPlano {
	
	private GeneradorPlano() {
		super();
	}
	private static String archivoPlano;
	private static final String FILE_NAME = "EOR_CNX_ORD_CONEXION_ACTIVAR_";
	public static final String SEPARATOR = "\t";

	public static String generar(List<RegistroOrdenDTO> listaRegistroOrden, ParametrosDTO parametros) {
		String nombreArchivo = crearArchivo(parametros.getPathCnx(), parametros.getNroOrden());
		imprimirLinea(listaRegistroOrden);
		return nombreArchivo;
	}

	private static String crearArchivo(String pathCnx, Long nroOrden) {
		String nombreArchivo = FILE_NAME + nroOrden + ".txt";
		archivoPlano = pathCnx + nombreArchivo;
		Path rutaPlano = Paths.get(archivoPlano);
		try {
		    Files.createFile(rutaPlano);
		} catch (IOException e) {
			log.error("Error: No se encuentra configurado Ruta de Entrada. No se puede abrir archivo para proceso de Autoactivación {}", archivoPlano);
			e.printStackTrace();
			System.exit(1);
		}
		return nombreArchivo;
	}
	
	private static void imprimirLinea(List<RegistroOrdenDTO> listaRegistroOrden) {
		try (PrintWriter writer =
					 new PrintWriter(
							 new FileWriter(archivoPlano, true))) {
			for (RegistroOrdenDTO registro : listaRegistroOrden) {
				// REQ MAXIMETROS INICIO
				//String linea = registro.obtenerLinea();
				String linea = generarLinea(registro);
				// REQ MAXIMETROS FIN
				writer.write(linea);
				writer.println();
				log.info(linea);
			}
		} catch (IOException e) {
			log.error("Error: No se pudo escribir en el archivo: {}", archivoPlano);
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	private static String generarLinea(RegistroOrdenDTO registro) {
		List<Field> fields = Arrays.asList(registro.getClass().getDeclaredFields());
		StringBuilder lineaBuilder = new StringBuilder();
		for (Field field: fields) {
			try {
				field.setAccessible(true);
				if (field.get(registro) != null) {
					lineaBuilder.append(field.get(registro).toString() + SEPARATOR);
				} else {
					lineaBuilder.append("" + SEPARATOR);
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				log.error("No se pudo obtener el campo");
				System.exit(1);
			}
		}
		String linea = lineaBuilder.toString();
		return linea.substring(0, linea.length() - 1);
	}

}
