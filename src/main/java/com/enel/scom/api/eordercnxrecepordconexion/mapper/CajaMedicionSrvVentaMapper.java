package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class CajaMedicionSrvVentaMapper implements RowMapper<String> {

	public static final String SQLORACLE_SELECT_FOR_CAJAMEDSRVVENTA = "SELECT"
			+ " CASE"
			+ "	WHEN FA.COD_FASE ='T' THEN 'LTR'"
			+ "	ELSE 'LR'"
			+ "	END as cajaMedicion"
			+ " FROM ORD_ORDEN ORD, ORD_CONEX OCNX, VTA_SRV_VENTA SVEN, VTA_SOL_SRV_ELE SELE, PRD_CONEXION CNX, COM_FASE FA"
			+ " WHERE ORD.ID_ORDEN = OCNX.ID_ORD_VENTA"
			+ " AND ORD.ID_ORDEN = SVEN.ID_ORD_VENTA"
			+ " AND OCNX.ID_ORD_CONEX = SVEN.ID_ORD_CONEX"
			+ " AND SVEN.ID_SOL_SRV_ELE = SELE.ID_SOL_SRV_ELE"
			+ " AND SELE.ID_VTA_CONEXION = CNX.ID_VTA_CONEXION"
			+ " AND CNX.ID_FASE = FA.ID_FASE"
			+ " AND OCNX.NRO_ORDEN = ?"
			+ " AND ord.nro_orden = ?";

	@Override
	public String mapRow(ResultSet rs, int rowNum) throws SQLException {
		String cajaMedicion = rs.getString("cajaMedicion");
		return cajaMedicion;
	}
	
}
