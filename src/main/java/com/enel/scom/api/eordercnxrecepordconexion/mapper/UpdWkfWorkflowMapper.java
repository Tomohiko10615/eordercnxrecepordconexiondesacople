package com.enel.scom.api.eordercnxrecepordconexion.mapper;

public class UpdWkfWorkflowMapper {

	public static final String SQLORACLE_UPDATE_FOR_WKFWORKFLOW = "UPDATE"
			+ " wkf_workflow"
			+ " SET id_old_state=id_state, id_state = ?"
			+ " WHERE id_workflow = ?";
	
}
