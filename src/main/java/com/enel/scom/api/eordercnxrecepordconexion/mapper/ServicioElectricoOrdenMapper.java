package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.ServicioElectricoOrdenDTO;

public class ServicioElectricoOrdenMapper implements RowMapper<ServicioElectricoOrdenDTO> {

	public static final String SQLORACLE_SELECT_FOR_SRVELECTRICOORDEN = "SELECT"
			+ " c.id_srv_venta as idDato, b.id_servicio as idSrvElectrico, nvl(d.id_alimentador,0) as idAlim,"
			+ " nvl(d.id_sed,0) as idSed, nvl(d.id_llave,0) as idLlave,  d.id_dir_servicio as idDir,"
			+ " nvl(d.id_ruta_lectura,0) as idRutaSumin, nvl(d.id_pcr,0) as idPcr,"
			+ " c.id_sol_srv_ele as idSolSrvEle, e.id_estado as idEstSrvElectrico, c.id_conf_pres_vta as idConfPres,"
			+ "	c.id_ord_conex as idOrdConex, nvl(f.nro_orden,'') as nroOrdConex, g.cod_oficina as codSucEjecutora"
			+ " FROM nuc_cuenta a, nuc_servicio b, vta_srv_venta c,"
			+ " vta_sol_srv_ele d , srv_electrico e,"
			+ "	ord_conex f, seg_oficina g"
			+ " WHERE a.id_cuenta=b.id_cuenta"
			+ " AND b.id_servicio=d.id_srv_electrico"
			+ " AND d.id_sol_srv_ele=c.id_sol_srv_ele"
			+ " AND b.id_servicio=e.id_servicio"
			+ " AND b.tipo='ELECTRICO'"
			// + " AND c.estado in('Generado','Inspeccionado', 'Ejecutado','NoEjecutado','Habilitado')"
			+ " AND c.estado in('Generado','Inspeccionado', 'Ejecutado','NoEjecutado')"
			+ " AND c.id_ord_venta = ?"
			+ " AND a.nro_cuenta = ?"
			+ " AND c.id_ord_conex = f.id_ord_conex(+)"
			+ " AND c.id_suc_ejecutora = g.id_oficina"
			+ " AND e.id_estado in(0,3,5)";

	@Override
	public ServicioElectricoOrdenDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long idDato = rs.getLong("idDato");
		Long idSrvElectrico = rs.getLong("idSrvElectrico");
		Long idAlim = rs.getLong("idAlim");
		Long idSed = rs.getLong("idSed");
		Long idLlave = rs.getLong("idLlave");
		Long idDir = rs.getLong("idDir");
		Long idRutaSumin = rs.getLong("idRutaSumin");
		Long idPcr = rs.getLong("idPcr");
		Long idSolSrvEle = rs.getLong("idSolSrvEle");
		Long idEstSrvElectrico = rs.getLong("idEstSrvElectrico");
		Long idConfPres = rs.getLong("idConfPres");
		Long idOrdConex = rs.getLong("idOrdConex");
		String nroOrdConex = rs.getString("nroOrdConex");
		String codSucEjecutora = rs.getString("codSucEjecutora");
		return new ServicioElectricoOrdenDTO(idDato, idSrvElectrico, idAlim, idSed, idLlave, idDir, idRutaSumin, idPcr, idSolSrvEle, idEstSrvElectrico, idConfPres,
				idOrdConex, nroOrdConex, codSucEjecutora);
	}

}
