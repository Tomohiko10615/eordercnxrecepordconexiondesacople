package com.enel.scom.api.eordercnxrecepordconexion.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.enel.scom.api.eordercnxrecepordconexion.dto.FechaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.FechaHoraMapper;

@Component
public class FechaHoraDAO {
		
	@Autowired
	@Qualifier("jdbcTemplate2") 
	private JdbcTemplate jdbcTemplatePostgres;
	
	public FechaDTO getFechaHoy(String formato)
	{
		FechaDTO fecha= new FechaDTO();
		try 
		{
        	String sqlString = FechaHoraMapper.SQLPOSTGRESQL_SELECT_FOR_FECHA_HORA_HOY;
        	FechaHoraMapper fechaHoraMapper= new FechaHoraMapper();
        	FechaDTO oFecha = new FechaDTO();
        	
        	oFecha=jdbcTemplatePostgres.queryForObject(sqlString, fechaHoraMapper,formato);
        	fecha.setFechaHora(oFecha.getFechaHora());
        	
        	return fecha;
		}
		catch (Exception e) {
			System.out.println("Exception: "+e.getMessage());
			System.out.println("formato: "+formato);
			return null;
		}
	}
}
