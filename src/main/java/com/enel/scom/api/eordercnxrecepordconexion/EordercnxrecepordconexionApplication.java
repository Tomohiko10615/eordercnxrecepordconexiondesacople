package com.enel.scom.api.eordercnxrecepordconexion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.enel.scom.api.eordercnxrecepordconexion.util.ArchivoUtil;
import com.enel.scom.api.eordercnxrecepordconexion.util.Constante;
import com.enel.scom.api.eordercnxrecepordconexion.util.Log;
import com.enel.scom.api.eordercnxrecepordconexion.service.OrdRecepConexionService;

@SpringBootApplication
public class EordercnxrecepordconexionApplication implements CommandLineRunner {

	@Autowired
	OrdRecepConexionService service;
	
	public static void main(String[] args) {
		SpringApplication.run(EordercnxrecepordconexionApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		// borrar/comentar cuando se haga el pase a los servidores linux
		//args = new String[3];
        //args[0] = "EDEL";
        //args[1] = "pe02850937";
        //args[2] = "78954133";
		
		Constante constante = new Constante();
		Log.bfnOpenLogFile(Constante.PROCESO,constante.getRUTA_LOG());
		String rutaLog=constante.getRUTA_LOG();
        
		String codEmpresa = args[0];
		//String codEmpresa = "EDEL";
		String usuario = args[1];
		//String usuario = "pe02850937";
		String nroOrdenString = args[2];
		Long nroOrden = Long.parseLong(nroOrdenString);
		//Long nroOrden = 21357978L;
		
		Log.mbfnOpenFileError(String.format("%sEOR_CNX_RECEP_ORD_CONEXION_ERRORES_%d.xls",rutaLog,nroOrden));
		
		service.procesar(codEmpresa, usuario, nroOrden,rutaLog);
		
		exit(0);
	}
	private static void exit(int estado)
	{
		try
		{
			Log.bfnCloseLogFile();
			Log.bfnCloseLogFileError();
			Log.bfnCloseLogFileError2();
			ArchivoUtil.file2Writer.close();
			System.exit(estado);
		}catch(Exception ex)
		{
			System.exit(1);
		}
	}
}
