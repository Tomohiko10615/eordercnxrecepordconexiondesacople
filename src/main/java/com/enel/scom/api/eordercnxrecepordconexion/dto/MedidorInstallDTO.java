package com.enel.scom.api.eordercnxrecepordconexion.dto;

import com.enel.scom.api.eordercnxrecepordconexion.autoactiva.GeneradorPlano;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MedidorInstallDTO {

	private String numeroMedidor;
	private String codMarca;
	private String codModelo;
	private String fecLectTerreno;
	private String novedad;
	private String mACFP;
	private String mACHP;
	private String mACTI;
	private String mDMFP;
	private String mDMHP;
	private String mREAC;
	private String primeraLectura;
	private String factor;
	/* ANL_20230912 */
	private String mACFPB;
	private String mACFPM;
	private String mDMFPB;
	private String mDMFPM;
	/* ANL_20230912 fin */

	public String obtenerLinea() {
		StringBuilder lineaBuilder = new StringBuilder();
		appendField(lineaBuilder, numeroMedidor);
		appendField(lineaBuilder, codMarca);
		appendField(lineaBuilder, codModelo);
		appendField(lineaBuilder, fecLectTerreno);
		appendField(lineaBuilder, novedad);
		appendField(lineaBuilder, primeraLectura);
		return lineaBuilder.toString();
	}

	private void appendField(StringBuilder builder, String value) {
		if (value != null) {
			builder.append(value);
		}
		builder.append(GeneradorPlano.SEPARATOR);
	}
}
