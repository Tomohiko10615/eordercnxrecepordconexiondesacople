package com.enel.scom.api.eordercnxrecepordconexion.mapper;

public class InsAuditoriaVentaMapper {

	public static final String SQLORACLE_INSERT_FOR_AUDITORIAVENTA = "INSERT INTO"
			+ " vta_aud_ord(id_aud_ord, id_empresa, id_usuario, observacion,"
			+ "	fecha_ejecucion, tipo_obra, id_segmento, discriminator, estado_origen,"
			+ "	estado_destino, nombre_actividad, id_ord_venta, id_motivo)"
			+ "	VALUES (?, ?, ?, ?,"
			+ "	sysdate, ?, ?, 'OPERACION_ORD', ?, ?,"
			+ "	'PARALIZAR', ?, ?)";
	
	public static final String SQLORACLE_INSERT_FOR_AUDITORIAVENTA2 = "INSERT INTO"
			+ " VTA_AUD_ORD"
			+ " (ID_AUD_ORD, ID_EMPRESA, ID_ORD_VENTA, ESTADO_ORIGEN, ESTADO_DESTINO, NOMBRE_ACTIVIDAD, OBSERVACION,"
			+ " ID_USUARIO, DISCRIMINATOR,  TIPO_OBRA, ID_SEGMENTO, FECHA_EJECUCION)"
			+ " SELECT (SELECT SQAUDITORIAVENTA.NEXTVAL FROM DUAL), ID_EMPRESA, ID_ORD_VENTA,ESTADO_ORIGEN,ESTADO_DESTINO, 'ACTIVAR', 'SE DESAPARALIZA POR PROCESO DE AUTOACTIVACION',"
			+ "	9998, 'ORDEN',  TIPO_OBRA, ID_SEGMENTO, SYSDATE"
			+ " FROM VTA_AUD_ORD"
			+ " WHERE ID_ORD_VENTA = ?"
			+ "	AND ID_AUD_ORD IN (SELECT MAX(ID_AUD_ORD) FROM VTA_AUD_ORD WHERE ID_ORD_VENTA = ?)";
	
}
